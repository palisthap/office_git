<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataflowStatus extends Model
{
	protected $fillable=['dha_id','passport_no','dha_ref_no','barcode_no','remarks','status'];
    public function dhafees(){
    	return $this->belongsTo('App\DhaFees','dha_id','id');
    }
}
