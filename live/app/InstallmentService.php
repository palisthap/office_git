<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstallmentService extends Model
{
     protected $fillable=['id','date','nepali_date','service_charge','payment_mode','received_by','bank_deposited','servicecharge_id','status','deposited_date','nepali_deposited_date'];

        public function servicecharge()
   {
   	return $this->belongsTo('App\ServiceCharge');
   }
}
