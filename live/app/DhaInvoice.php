<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DhaInvoice extends Model
{
 protected $fillable=['dhainvoice_id','dha_id','date','nepali_date'];
    public function dhafees(){
    	return $this->belongsTo('App\DhaFees','dha_id');
    }   
}
