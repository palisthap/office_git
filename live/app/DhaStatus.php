<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DhaStatus extends Model
{
    protected $fillable=['dha_id','email','username','password','status'];
    public function dhafees(){
    	return $this->belongsTo('App\DhaFees','dha_id');
    }

   }
