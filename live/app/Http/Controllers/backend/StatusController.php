<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DhaFees;
use App\DhaStatus;
use App\DataflowStatus;
use DB;
use App\User;
class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $status;
    public function __construct(DhaStatus $status){
        $this->status = $status;
    }
    public function index()
    {
        $status = DhaStatus::all();
        $dha_fees=DhaFees::pluck('name','id');
        return view('officio.status.index',compact('status','dha_fees'))->with('title','User Status');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
        // $dhaFees_detail=DhaFees::all();
        $dhafees=DhaFees::pluck('name','id');
        return view('officio.status.create',compact('dhafees'))->with('title','Create User Status');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     if(!$request->dha_id){
        return false;
    }
    
    $this->validate($request, [
        'dha_id' => 'required',


        ]);
    $input = $request->all();
    if ($input) {
       DhaStatus::create($input);
       session()->flash('message', 'User Status  Created.');
       return redirect('admin/dhastatus');
            // return view('')
   }
}
public function createdhastatus(Request $request)
{

    $type=DhaStatus::with(array('dhafees'=>function($query){
        $query->select('id','name','applied_for');
    }))->get();

   // return($type[0]->dhafees->applied_for);
  
  if($request->dha_id){
      $dataflow=DataflowStatus::all();
      
      $dha_id=$request->dha_id;
      return view('officio.status.createdhastatus',compact('dataflow','dha_id','type'))->with('title','Create User Status');

      
  }
  
}

public function postdhastatus(Request $request){

   $this->validate($request, [
    'email' => 'required',
    'username' => 'required',
    'password' => 'required',


    ]);

   $input = $request->all();
// $input['password']=bcrypt($request->password);
   if ($input) {
       DhaStatus::create($input);
       session()->flash('message', 'User Status  Created.');
       return redirect('admin/status');
   }   
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $status = DhaStatus::with('dhafees')->get();
//          return $status;
       
       return view('officio.status.show',compact('status'))->with('title','User Status'); 
   }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dhastatus = DhaStatus::find($id);
       // $dhastatus=DhaStatus::with('dhafees')->where('id',$id)->get();
        $dha_id=DhaStatus::with('dhafees')->where('id',$id)->get();
        return view('officio.status.editdha',compact('dhastatus','dha_id'))->with('title','Edit User Status');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            
            ]);
        $dhastatus = DhaStatus::find($id);
        $input = $request->all();
        $dhastatus->update($input);
        session()->flash('message', 'Dha Status Updated.');
        return redirect('admin/status');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        DhaStatus::find($id)->delete();


        session()->flash('message', 'DHA Profile Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

