<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EnquiryByThem;
use DB;

class EnquiryByThemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enquirybythems = EnquiryByThem::all();
       
        return view('officio.enquirybythem.index',compact('enquirybythems','data'))->with('title','enquiry by thems');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
        return view('officio.enquirybythem.create')->with('title','Create enquiry by thems');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required',
            'name' => 'required',
            'email' => 'required',
            'qualification' => 'required',
            'phone' => 'required',
            'remarks' => 'required',
           
        ]);
        $input = $request->all();
        if ($input) {
           EnquiryByThem::create($input);
            session()->flash('message', 'Enquiry By Them Created.');
            return redirect('admin/enquirybythem'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        $enquiry=DB::table('enquiry_by_thems')->select(DB::raw('*'))->
                  where('date', '>=', \Carbon::today()->toDateString())->get();
            return view('officio.enquiry.show',compact('enquiry'))->with('title','Show Griffith Enquiry');        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enquirybythem = EnquiryByThem::find($id);
        return view('officio.enquirybythem.edit',compact('enquirybythem'))->with('title','Edit Enquiry By Them');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'date' => 'required',
            'name' => 'required',
            'email' => 'required',
            'qualification' => 'required',
            'phone' => 'required',
            'remarks' => 'required',
        ]);
        $enquirybythem = EnquiryByThem::find($id);
        $input = $request->all();
        $enquirybythem->update($input);
        session()->flash('message', 'Enquiry By Them Updated.');
        return redirect('admin/enquirybythem');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

       EnquiryByThem::find($id)->delete();


        session()->flash('message', 'Enquiry By Them Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

