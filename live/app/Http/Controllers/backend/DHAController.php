<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DhaFees;
use DB;
use App\InstallmentDha;

class DHAController extends Controller
{

      public function __construct(DhaFees $dha){
        $this->dha = $dha;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dhaFees = DhaFees::all();
        $data=DB::table("dha_fees")->sum('first_payment');
//        dd($data);
        return view('officio.dhafees.index',compact('dhaFees','data'))->with('title','DHA/Fees');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
        return view('officio.dhafees.create')->with('title','Create DHA/Fees');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
           'name' => 'required',
            'qualification' => 'required',
            'applied_for' => 'required',
            'first_payment' => 'required',
           
        ]);
        $input = $request->all();
        if ($input) {
            DhaFees::create($input);
            session()->flash('message', 'DHA/Fees Created.');
            return redirect('admin/dha');
        }
    }

public function createdhaInstallment(Request $request)
{


  
          if($request->dha_id){
  $installment=InstallmentDha::all();
 
    $dha_id=$request->dha_id;
    return view('officio.installment.create',compact('installment','dha_id'))->with('title','Create Dha Installment');

       
    }
  
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    
    {
        $dhafees=DhaFees::with('installmentDha')->where('id',$id)->get();
        $dhaFeeTotal=DhaFees::with('installmentDhaTotal')->where('id',$id)->get();
      return view('officio.dhafees.show',compact('dhafees','dhaFeeTotal'))->with('title','Dha User Profile');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dhaFee = DhaFees::find($id);
        return view('officio.dhafees.edit',compact('dhaFee'))->with('title','Edit DHA/Fee');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'qualification' => 'required',
            'applied_for' => 'required',
            'first_payment' => 'required',
        ]);
        $dhaFee = DhaFees::find($id);
        $input = $request->all();
        $dhaFee->update($input);
        session()->flash('message', 'DHA/Fees Updated.');
        return redirect('admin/dha');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        DhaFees::find($id)->delete();


        session()->flash('message', 'DHA/Fees Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

