<?php

namespace App\Http\Controllers\backend;

// use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use Mail;
use Validator;
use App\Enquiry;
use App\EnquiryByThem;
use App\EnquiryInOffice;
use Request;
class NewsLetterController extends Controller
{
    public function contact(){
        $uss=Enquiry::all();

        $forms=EnquiryByThem::all();
        $ins=EnquiryInOffice::all();
        return view('officio.contact.create',compact('uss','forms','ins'))->with('title','News Letter');
    }
    public function postcontact(){
        $data = Request::all();
       // var_dump( $data); die;
        $checkboxes1 = Request::input('usemail');
        $checkboxes2 = Request::get('formemail');
        $checkboxes3 = Request::get('inemail');
 // dd($checkboxes1);
        $check = Validator::make($data, array(

            'subject' => 'required',
            'message' => 'required',
            ));

        $emails = [

        'palistha01@gmail.com'
        ];

        // if the validator fails, redirect back to the form
        if ($check->fails()) {
            return Redirect::back()
                            ->withErrors($check) // send back all errors to the login form
                            ->withInput();
                        } else {
                    if($checkboxes1!=null) {
                        foreach ($checkboxes1 as $checkEmail1) {
                            $this->sendEmail($checkEmail1, $data);
                        }
                    }
                       if($checkboxes2!=null){
                           foreach ($checkboxes2 as $checkEmail2) {
                               $this->sendEmail($checkEmail2,$data);
                           }
                       }

                    if($checkboxes3!=null) {
                        foreach ($checkboxes3 as $checkEmail3) {
                            $this->sendEmail($checkEmail3, $data);
                        }
                    }


                        session()->flash('message','Mail Sent Successfully!!');
                         return redirect('admin/news');
                     }


                 }
                 private function sendEmail($email_address,$data){
                    Mail::send('emails1.contact', ['data' => $data], function ($message) use ($data,$email_address){
                       // $message->to( $request->input('usemail') );
                        $message->to($email_address)->subject('Contact query');
                    });
                }

            }
