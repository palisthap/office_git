<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InstallmentService;
use App\ServiceCharge;
use App\Http\Controllers\backend\DateConverterController;
use DB;
use Carbon;
use App\ServiceInvoice;

class ServiceInstallmentController extends Controller
{
	public function __construct( DateConverterController $nep_date){
		$this->nep_date = $nep_date;
	}
	private function convert_to_eng($nepali_date){
		$nepali=$nepali_date;
		$nepali_temp=explode('-', $nepali);
		$nepali_year=$nepali_temp[0];
		$nepali_month=$nepali_temp[1];
		$nepali_day=$nepali_temp[2];
		$eng_temp=$this->nep_date->nep_to_eng($nepali_year,$nepali_month,$nepali_day);
		$eng_year=$eng_temp['year'];
		$eng_month=$eng_temp['month'];
		$eng_day=$eng_temp['date'];
		$eng_date=$eng_year.'-'.$eng_month.'-'.$eng_day;
		return $eng_date;
	}
	public function index()
	{
		$installment = InstallmentService::all();

		return view('officio.installmentservice.index',compact('installment'))->with('title','Griffith installment');
	}
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()

    {

    	$installment=InstallmentService::all();


    	return view('officio.installmentservice.create',compact('installment','servicecharge_id'))->with('title','Create Service Installment');



    }

    public function viewinvoice($id){

  // echo $this->nep_date->ToOrdinal(1); die;


     $servicecharges=ServiceCharge::with('installmentService')->where('id',$id)->get();
        $servicechargeTotal=ServiceCharge::with('installmentServiceTotal')->where('id',$id)->get();

    	
    	$serviceinvoice_id=strtotime(Carbon::now());
    	$date=Carbon::today();
    	$dha= ServiceInvoice::create([
    		'servicecharge_id'=>$id,
    		'serviceinvoice_id'=>$serviceinvoice_id,
    		'date'=>$date,

    		]);


    	return view('officio.installmentservice.invoice',compact('date','servicecharges','servicechargeTotal','serviceinvoice_id'))->with('title','Grifith Plus')->with('convert',new DateConverterController);
    }
    /**
    }
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    	$this->validate($request, [

    		// 'payment_mode' => 'required',
    		// 'received_by' => 'required',
    		// 'deposited_date' => 'required',
    		'service_charge' => 'required',



    		]);
   
     $input = $request->all();

        $nepali_date=$request->nepali_date;
        $nepali_deposited_date=$request->nepali_deposited_date;
        $deposited_date=$this->convert_to_eng($nepali_deposited_date);
        $eng_date=$this->convert_to_eng($nepali_date);
        // if ($input) {
    // dd($input);
    	InstallmentService::create([
    		'nepali_date' => $nepali_date,
    		'date' => $eng_date,
    		'status' => $request->status,     
    		'received_by' => $request->received_by?$request->received_by:Null,
    		'bank_deposited' => $request->bank_deposited?$request->bank_deposited:Null,
    		'payment_mode' => $request->payment_mode?$request->payment_mode:Null,
    		'service_charge' => $request->service_charge?$request->service_charge:Null,
    		'deposited_date' => $deposited_date,
    		'nepali_deposited_date' =>$nepali_deposited_date,
    		

    		'servicecharge_id' => $request->servicecharge_id?$request->servicecharge_id:Null,
    		]);
    	session()->flash('message', 'Griffith installment Created.');
    return redirect('admin/service_charge/'.$request->servicecharge_id);

        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$installment = InstallmentService::find($id);
    	return view('officio.installmentservice.edit',compact('installment'))->with('title','Edit Griffith installment');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$this->validate($request, [
    		'payment_mode' => 'required',
    		'received_by' => 'required',
    		'deposited_date' => 'required',
    		'service_charge' => 'required',


    		]);
    	$input = $request->all();
          $nepali_date=$request->nepali_date;
        $nepali_deposited_date=$request->nepali_deposited_date;
        $deposited_date=$this->convert_to_eng($nepali_deposited_date);
        $eng_date=$this->convert_to_eng($nepali_date);
    	$installment = InstallmentService::find($id);
    	$input = $request->all();
    	$installment->update([
    		'nepali_date' => $nepali_date,
    		'date' => $eng_date,
    		'status' => $request->status,     
    		'received_by' => $request->received_by?$request->received_by:Null,
    		'bank_deposited' => $request->bank_deposited?$request->bank_deposited:Null,
    		'payment_mode' => $request->payment_mode?$request->payment_mode:Null,
    		'service_charge' => $request->service_charge?$request->service_charge:Null,
    		'deposited_date' => $deposited_date,
    		'nepali_deposited_date' =>$nepali_deposited_date,
    		

    		'servicecharge_id' => $request->servicecharge_id?$request->servicecharge_id:Null,
    		]);

    	session()->flash('message', 'Griffith installment Updated.');
    	return redirect('admin/installmentservice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	if(!request()->ajax()){
    		return false;
    	}

    	InstallmentService::find($id)->delete();


    	session()->flash('message', 'Griffith installment Deleted.');

    	return response()->json(array(
    		'status' => 'success',
    		));
    }
}

