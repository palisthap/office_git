<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Enquiry;
use DB;


class EnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enquirys = Enquiry::all();
     
        return view('officio.enquiry.index',compact('enquirys'))->with('title','Griffith enquiry');
    }
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()
    {
        return view('officio.enquiry.create')->with('title','Create Griffith enquiry');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'date' => 'required',
            'name' => 'required',
            'email' => 'required',
            'qualification' => 'required',
            'phone' => 'required',
            'remarks' => 'required',
           
        ]);
        $input = $request->all();
        if ($input) {
            Enquiry::create($input);
            session()->flash('message', 'Griffith Enquiry Created.');
            return redirect('admin/enquiry');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $enquiry=DB::table('enquiries')->select(DB::raw('*'))->
                  where('date', '>=', \Carbon::today()->toDateString())->get();
            return view('officio.enquiry.show',compact('enquiry'))->with('title','Show Griffith Enquiry');        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enquiry = Enquiry::find($id);
        return view('officio.enquiry.edit',compact('enquiry'))->with('title','Edit Griffith Enquiry');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'date' => 'required',
            'name' => 'required',
            'email' => 'required',
            'qualification' => 'required',
            'phone' => 'required',
            'remarks' => 'required',
        ]);
        $enquiry = Enquiry::find($id);
        $input = $request->all();
        $enquiry->update($input);
        session()->flash('message', 'Griffith Enquiry Updated.');
        return redirect('admin/enquiry');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        Enquiry::find($id)->delete();


        session()->flash('message', 'Griffith Enquiry Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

