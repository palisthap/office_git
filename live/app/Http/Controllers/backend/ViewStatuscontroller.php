<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DhaFees;
use App\DhaStatus;
use App\DataflowStatus;
use App\ExamStatus;
class ViewStatuscontroller extends Controller
{
    public function show()
    {
        $status = DhaStatus::with('dhafees')->get();
//          return $status;
        $dataflow = DataflowStatus::with('dhafees')->get();
        $examstatus=ExamStatus::with('dhafees')->get();
        return view('officio.status.show',compact('status','dataflow','examstatus'))->with('title','User Status');
    }
}
