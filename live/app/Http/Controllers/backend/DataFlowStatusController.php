<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DhaFees;
use App\DataflowStatus;
use DB;
use App\User;

class DataFlowStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataflowstatus = DataflowStatus::with('dhafees')->get();
      
       
        $dha_fees=DhaFees::pluck('name','id');
        return view('officio.status.index',compact('dataflowstatus','dha_fees'))->with('title','Data Flow  Status');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create(Request $request)
    {
        // if($request->dha_id){
      $dataflowstatus = DataflowStatus::all();

      $dhaFees_detail=DhaFees::all();
      $dhafees=DhaFees::pluck('name','id');
      return view('officio.status.createdhastatus',compact('dhafees','dhaFees_detail','dataflowstatus'))->with('title','Create Data Flow Status');
  // }
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request);

        $this->validate($request, [
         'passport_no' => 'required',
         'dha_ref_no' => 'required',
         'barcode_no' => 'required',
         'remarks' => 'required',


         ]);
        $input = $request->all();
        $dha_id=$request->dha_id; 
        if ($input) {
           DataflowStatus::create($input);
           session()->flash('message', 'Data Flow Status  Created.');
           return redirect('admin/statuss?dha_id='.$dha_id);
       }
   }

    public function show($id)
    {
        $statuss = DataflowStatus::with('dhafees')->get();


        return view('officio.status.show',compact('statuss'))->with('title','User Status');
    }
    public function edit($id)
    {
      
        $dataflow = DataflowStatus::find($id);
        // dd($cashin)dataflow;
        return view('officio.status.editdataflow',compact('dataflow'))->with('title','Edit DataFlow Status');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
       'passport_no' => 'required',
        



           ]);
        // $cashIn = Cash_in::find($id);
        $input = $request->all();
      

        $cash_in=DataflowStatus::find($id);
    
        $cash_in->update($input);
        session()->flash('message', 'DataFlow Status Updated.');
        return redirect('admin/viewstatus');
    }

    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        DataflowStatus::find($id)->delete();


        session()->flash('message', 'Data Flow Status Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

