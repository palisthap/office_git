<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InstallmentDha;
use App\DhaFees;
use App\Http\Controllers\backend\DateConverterController;
use DB;
use Carbon;
use App\DhaInvoice;

class DhaInstallmentController extends Controller
{
 public function __construct( DateConverterController $nep_date){
    $this->nep_date = $nep_date;
}
private function convert_to_eng($nepali_date){
    $nepali=$nepali_date;
    $nepali_temp=explode('-', $nepali);
    $nepali_year=$nepali_temp[0];
    $nepali_month=$nepali_temp[1];
    $nepali_day=$nepali_temp[2];
    $eng_temp=$this->nep_date->nep_to_eng($nepali_year,$nepali_month,$nepali_day);
    $eng_year=$eng_temp['year'];
    $eng_month=$eng_temp['month'];
    $eng_day=$eng_temp['date'];
    $eng_date=$eng_year.'-'.$eng_month.'-'.$eng_day;
    return $eng_date;
}
public function index()
{
    $installment = InstallmentDha::all();

    return view('officio.installment.index',compact('installment'))->with('title','Griffith installment');
}
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()

    {

      $installment=InstallmentDha::all();


      return view('officio.installment.create',compact('installment','dha_id'))->with('title','Create Dha Installment');



  }

  public function viewinvoice($id){

  // echo $this->nep_date->ToOrdinal(1); die;


      $dhafees=DhaFees::with('installmentDha')->where('id',$id)->get();
      $dhaFeeTotal=DhaFees::with('installmentDhaTotal')->where('id',$id)->get();
     
      $dhainvoice_id=strtotime(Carbon::now());
      $date=Carbon::today();
      $dha= DhaInvoice::create([
        'dha_id'=>$id,
        'dhainvoice_id'=>$dhainvoice_id,
        'date'=>$date,
        
        ]);
      

      return view('officio.installment.invoice',compact('date','dhainvoice_id','dhafees','installment','dhaFeeTotal'))->with('title','Grifith Plus')->with('convert',new DateConverterController);
  }
    /**
    }
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [

        // 'nepali_date' => 'required',
        // 'payment_mode' => 'required',
        // 'received_by' => 'required',
        // 'deposited_date' => 'required',
        // 'verified_by' => 'required',
            'amount' => 'required',

            

            ]);
        $input = $request->all();
        $nepali_date=$request->nepali_date;
        $nepali_deposited_date=$request->nepali_deposited_date;
        $deposited_date=$this->convert_to_eng($nepali_deposited_date);
        $eng_date=$this->convert_to_eng($nepali_date);
        // if ($input) {
    // dd($input);
        InstallmentDha::create([
           'nepali_date' => $nepali_date,
           'date' => $eng_date,
           'status' => $request->status,     
           'received_by' => $request->received_by?$request->received_by:Null,
           'bank_deposited' => $request->bank_deposited?$request->bank_deposited:Null,
           'payment_mode' => $request->payment_mode?$request->payment_mode:Null,
           'deposited_date' => $deposited_date,
           'nepali_deposited_date' =>$nepali_deposited_date,
           'verified_by' => $request->verified_by?$request->verified_by:Null,
           'amount' => $request->amount?$request->amount:Null,
           'dha_id' => $request->dha_id?$request->dha_id:Null,
           ]);
        session()->flash('message', 'Griffith installment Created.');
        return redirect('admin/dha/'.$request->dha_id);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $installment = InstallmentDha::find($id);
        return view('officio.installment.edit',compact('installment'))->with('title','Edit Griffith installment');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'payment_mode' => 'required',
           'received_by' => 'required',
           'deposited_date' => 'required',
           'verified_by' => 'required',
           'amount' => 'required',

           ]);
        $nepali_date=$request->nepali_date;
        $nepali_deposited_date=$request->nepali_deposited_date;
        $deposited_date=$this->convert_to_eng($nepali_deposited_date);
        $eng_date=$this->convert_to_eng($nepali_date);
        $installment = InstallmentDha::find($id);
        $input = $request->all();
        $installment->update([
           'nepali_date' => $nepali_date,
           'date' => $eng_date,
           'status' => $request->status,     
           'received_by' => $request->received_by?$request->received_by:Null,
           'bank_deposited' => $request->bank_deposited?$request->bank_deposited:Null,
           'payment_mode' => $request->payment_mode?$request->payment_mode:Null,
           'deposited_date' => $deposited_date,
           'nepali_deposited_date' =>$nepali_deposited_date,
           'verified_by' => $request->verified_by?$request->verified_by:Null,
           'amount' => $request->amount?$request->amount:Null,
           'dha_id' => $request->dha_id?$request->dha_id:Null,
           ]);
        
        session()->flash('message', 'Griffith installment Updated.');
        return redirect('admin/installment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if(!request()->ajax()){
        //     return false;
        // }

        InstallmentDha::find($id)->delete();


        session()->flash('message', 'Griffith installment Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

