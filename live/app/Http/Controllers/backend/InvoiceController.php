<?php

namespace App\Http\Controllers\backend;
use App\DhaInvoice;
use App\ServiceInvoice;
use App\DhaFees;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon;
// use App\Invoice;

class InvoiceController extends Controller
{

	public function dhainvoice(){
		// $invoices=Invoice::all();
		$dhainvoice = DhaInvoice::with('dhafees')->get();




		return view('officio.invoice.dhainvoice',compact('dhainvoice '))->with('title','Invoice');
	}
	public function serviceinvoice(){

		$serviceinvoice = ServiceInvoice::with('servicecharge')->get();
		


		return view('officio.invoice.serviceinvoice',compact('serviceinvoice  '))->with('title','Invoice');
	}
}