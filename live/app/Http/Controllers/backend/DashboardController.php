<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;
// use redirect;
use App\User;
use Validator;
use Hash;
use Carbon\Carbon;
use App\Cash_in;
use App\CashOut;
use App\DhaFees;
use DB;
use App\Report;
use DateTime;
use App\ServiceCharge;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\backend\DateConverterController;

class DashboardController extends Controller
{
  public function __construct( DateConverterController $nep_date){
    $this->nep_date = $nep_date;
    $this->middleware('auth',['only'=>'index']);
        // $this->middleware('guest',['only'=>'getLogin']);
  }

  public function index(){


    $cashins=Cash_in::pluck('service_charge','id');
    $data_cashin = DB::table("cash_ins")->sum('service_charge');

    $cashouts=CashOut::pluck('amount','id');
    $data_cashout=DB::table("cash_outs")->sum('amount');

    $dhaFees = DhaFees::pluck('first_payment','id');
    $data_dhafees=DB::table("dha_fees")->sum('first_payment');
    $serviceCharges = ServiceCharge::pluck('service_charge','id');
    $data_servicecharge=DB::table("service_charges")->sum('service_charge');

    $timestemp = \Carbon\Carbon::now()->toDateTimeString();
    $month = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timestemp)->month;



    return view('officio.dashboard.index',compact('cashins','cashouts','dhafees','serviceCharges','reports','current_time',' data_cashin','data_cashout','data_dhafees','data_servicecharge','month'))->with('title','Green Computing Nepal | Dashboard');
  }
  public function getReport(){
    $month=$_POST['month'];
   

     $cashout_expenses=DB::table('cash_outs')->select(DB::raw('expenses_type,SUM(amount) as amount'))->groupBy('expenses_type')->whereMonth('date', '=', $month)->get();


   $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->whereMonth('date', '=', $month)->get();
   
   $data_cashouts = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->whereMonth('date', '=', $month)->get();
     // $cashin=Cash_in::all();

   $Basic_Life_Support= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','Basic Life Support')->whereMonth('date', '=', $month)->get();
   $DHA_MCQ= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','DHA MCQ/HAAD MCQ')->whereMonth('date', '=', $month)->get();

       // date2
   
   return view('officio.dashboard.report',compact('month','date2','data_cashin','cashout_expenses','data_cashouts','Basic_Life_Support','DHA_MCQ'))->with('title','Show record');
 
  }

  public function nepalidate(){
    $month=$_POST['month'];

  

       $cashout_expenses=DB::table('cash_outs')->select(DB::raw('expenses_type,SUM(amount) as amount'))->groupBy('expenses_type')->whereMonth('nepali_date', '=', $month)->get();


   $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->whereMonth('nepali_date', '=', $month)->get();
   
   $data_cashouts = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->whereMonth('nepali_date', '=', $month)->get();
     // $cashin=Cash_in::all();

   $Basic_Life_Support= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','Basic Life Support')->whereMonth('nepali_date', '=', $month)->get();
   $DHA_MCQ= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','DHA MCQ/HAAD MCQ')->whereMonth('nepali_date', '=', $month)->get();

       // date2
   
   return view('officio.dashboard.report',compact('month','data_cashin','cashout_expenses','data_cashouts','Basic_Life_Support','DHA_MCQ'))->with('title','Show record');

  }
  public function nepali(){
   $date=$_POST['date'];
  
   $cashout_expenses=DB::table('cash_outs')->select(DB::raw('expenses_type,SUM(amount) as amount'))->groupBy('expenses_type')->whereDate('nepali_date', '=', $date)->get();


   $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->whereDate('nepali_date', '=', $date)->get();
   
   $data_cashouts = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->whereDate('nepali_date', '=', $date)->get();
     // $cashin=Cash_in::all();

   $Basic_Life_Support= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','Basic Life Support')->whereDate('nepali_date', '=', $date)->get();
   $DHA_MCQ= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','DHA MCQ/HAAD MCQ')->whereDate('nepali_date', '=', $date)->get();

       // date2
   
   return view('officio.dashboard.report',compact('date1','date2','data_cashin','cashout_expenses','data_cashouts','Basic_Life_Support','DHA_MCQ'))->with('title','Show record');
 }
 public function englishDate(){
   $date=$_POST['engdate'];
 

   $cashout_expenses=DB::table('cash_outs')->select(DB::raw('expenses_type,SUM(amount) as amount'))->groupBy('expenses_type')->whereDate('date', '=', $date)->get();


   $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->whereDate('date', '=', $date)->get();
   
   $data_cashouts = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->whereDate('date', '=', $date)->get();
     // $cashin=Cash_in::all();

   $Basic_Life_Support= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','Basic Life Support')->whereDate('date', '=', $date)->get();
   $DHA_MCQ= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','DHA MCQ/HAAD MCQ')->whereDate('date', '=', $date)->get();

       // date2
   
   return view('officio.dashboard.report',compact('date1','date2','data_cashin','cashout_expenses','data_cashouts','Basic_Life_Support','DHA_MCQ'))->with('title','Show record');
 }

 public function nepaliDate2(){

   $date1=$_POST['date1'];

   $date2=$_POST['date2'];

   $cashout_expenses=DB::table('cash_outs')->select(DB::raw('expenses_type,SUM(amount) as amount'))->groupBy('expenses_type')->whereBetween('nepali_date', array($date1, $date2))->get();


   $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->whereBetween('nepali_date', array($date1, $date2))->get();
   
   $data_cashouts = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->whereBetween('nepali_date', array($date1, $date2))->get();
     // $cashin=Cash_in::all();

   $Basic_Life_Support= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','Basic Life Support')->whereBetween('nepali_date', array($date1, $date2))->get();
   $DHA_MCQ= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','DHA MCQ/HAAD MCQ')->whereBetween('nepali_date', array($date1, $date2))->get();

       // date2
   
   return view('officio.dashboard.report',compact('date1','date2','data_cashin','cashout_expenses','data_cashouts','Basic_Life_Support','DHA_MCQ'))->with('title','Show record');
 }

 public function englishDate2(){
   $date1=$_POST['date1'];
   $date2=$_POST['date2'];
   $cashout_expenses=DB::table('cash_outs')->select(DB::raw('expenses_type,SUM(amount) as amount'))->groupBy('expenses_type')->whereBetween('date', array($date1, $date2))->get();


   $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->whereBetween('date', array($date1, $date2))->get();
   
   $data_cashouts = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->whereBetween('date', array($date1, $date2))->get();
     // $cashin=Cash_in::all();

   $Basic_Life_Support= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','Basic Life Support')->whereBetween('date', array($date1, $date2))->get();
   $DHA_MCQ= DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->where('service_type','=','DHA MCQ/HAAD MCQ')->whereBetween('date', array($date1, $date2))->get();

       // date2
   
   return view('officio.dashboard.report',compact('date1','date2','data_cashin','cashout_expenses','data_cashouts','Basic_Life_Support','DHA_MCQ'))->with('title','Show record');
  
 }
 public function getLogin(){
  return view('admin.login')->with('title','Green Commputing Nepal | Admin Login');
}

public function postLogin(Request $request){
        // dd($request);
  if(Auth::attempt([
    'username' => $request->username,
    'password'=>$request->password
    ])){

    $user_id = auth()->user()->id;
  return redirect('admin/dashboard');
}

// return redirect()->back();
Auth::logout();
return redirect('admin')
->withInput($request->only('username'))
->withErrors([
  'username' => 'Invalid username/password'
  ]);
}

public function getLogout(){
  Auth::logout();
  return redirect('admin')->with('title','Aladdin | Admin Login');
}

public function profile(){
  $users = User::find(Auth::user()->id);

  return view('officio.profile', array('users'=>$users))->with('title','Aladdin | Profile');
}

public function editInfo(Request $request){
  $this->validate($request, [
    'name'     => 'required',

    'username'     => 'required',
    ]);

  $id = Auth::user()->id;
  $user = User::find($id);


  $user->name = $request->input('name');
  $user->username = $request->input('username');



  $user->save();
  session()->flash('message', 'Profile Updated.');
  return redirect('admin/profile');

}


public function updatePassword(Request $request)
{
  $user = Auth::user();

  $this->validate($request, [
    'old_password'          => 'required',
    'new_password'                 => 'required|min:6',
    'confirm_password'    => 'required|same:new_password'
    ]);

  $user->password = Hash::make(Input::get('new_password'));
  $user->save();
  session()->flash('message', 'Profile Updated.');

  return redirect()->back();
}




}

