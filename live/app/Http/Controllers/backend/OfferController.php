<?php
namespace App\Http\Controllers\backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offer;
use App\User;
use Validator;
use Redirect;
use Auth;
use Mail;
use Input;
// use Request;
use session;

class OfferController extends Controller
{
 private $offers;
 public function __construct(Offer $offers){
    $this->offers = $offers;
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offer::all();
        
        return view('officio.offer.index',compact('offers'))->with('title','Griffith offer');
    }
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()
    {
        return view('officio.offer.create')->with('title','Create Griffith offer');
    }

    
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'title' => 'required',
            'description' => 'required',
          
            'percentage' => 'required',
            // 'image' => 'required',
            'email' => 'required',
            ]);
        $input = $request->all();
        if ($input) {
            $destinationPath = 'uploads/';
            if (Input::file('image')) {
                $imagePath = str_random(6) . '_' . time() . "-" . $request->file('image')->getClientOriginalName();
                $input['image'] = $request->file('image')->move($destinationPath, $imagePath);

            }

            Offer::create($input);
            session()->flash('message', 'Griffith offer Created.');
            return redirect('admin/offer');
        }
    }

    
    public function show($id)
    {
        // $offers=Offer::find($id);
      $offer = Offer::where("id",$id)->first();
      
      return view('officio.offer.show',compact('offer','users'))->with('title','Griffith offer');
  }


  public function postOffer(Request $request){


     $data = $request->all();

     $data1 = $request->input('emailto');
     

     $check = Validator::make($data, array(

         'first_name' => 'required',
         'last_name' => 'required',
         'emailto' => 'required',
         ));

     if ($check->fails()) {
        return Redirect::back()
                            ->withErrors($check) // send back all errors to the login form
                            ->withInput();
                        } else {
                            Mail::send('emails.contact', ['data' => $data], function ($message) use ($data,$data1){
                                
                                $message->to($data1)->subject('Contact query');
                                
                            });
                            session()->flash('message','Mail Sent Successfully!!');
                            return redirect('admin/offer');
                        }


                        
                    }

                    
                    public function edit($id)
                    {
                        $offer = Offer::find($id);
                        return view('officio.offer.edit',compact('offer'))->with('title','Edit Griffith offer');
                    }

                    public function update(Request $request, $id)
                    {
                        $this->validate($request, [
                         'name' => 'required',
                         'title' => 'required',
                         'description' => 'required',
                         'address' => 'required',
                         'percentage' => 'required',

                         'email' => 'required'
                         ]);
                        $oldfile=Offer::find($id);
                        $offers = $this->offers->find($id);
                        $input = $request->all();
                        if ($offers) {
                            $removeimage = $oldfile->image;
                            if (empty($removeimage)) {
                                if (Input::file('image')) {
                                    $destinationPath = 'uploads/';
                                    $imagePath = str_random(6) . '_' . time() . "-" . $request->file('image')->getClientOriginalName();
                                    $input['image'] = $request->file('image')->move($destinationPath, $imagePath);
                                    $offers->update($input);
                                    session()->flash('message', 'Griffith offer Updated.');
                                    return redirect('admin/offer');
                                } else {
                                    $offers->update($input);
                                    session()->flash('message', 'offers Updated.');
                                    return redirect('admin/offer');
                                }
                            }
                            else{
                                if(Input::file('image')){
                                    $destinationPath = 'uploads/';
                                    $imagePath = str_random(6) . '_' . time() . "-" . $request->file('image')->getClientOriginalName();
                                    $input['image'] = $request->file('image')->move($destinationPath, $imagePath);
                                    $users->update($input);
                                    session()->flash('message', 'News Updated.');
                                    return redirect('admin/offer');
                                }else {
                                    $offers->fill($request->all())->save();
                                    session()->flash('message', 'Offers Updated.');
                                    return redirect('admin/offer');
                                }
                            }
                        }
                    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        Offer::find($id)->delete();


        session()->flash('message', 'Griffith offer Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

