<?php
namespace App\Http\Controllers\backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
     
        return view('officio.student.index',compact('students'))->with('title','Griffith Student');
    }
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()
    {
        return view('officio.student.create')->with('title','Create Griffith student');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
           
        //     'name' => 'required',
        //     'ph_no' => 'required',
        //     'mb_no' => 'required',
        //     'address' => 'required',
        //     'email' => 'required',
        //     'qualification' => 'required',
        //     'subject' => 'required',
        //     'title' => 'required',
        //     'experience' => 'required',
        //     'work_sector' => 'required',
        //     'language_test' => 'required',
        //     'test_score' => 'required',
        //     'service' => 'required',
           
           
        // ]);
        $input = $request->all();
        if ($input) {
            Student::create($input);
            session()->flash('message', 'Griffith student Created.');
            return redirect('admin/student');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $students = Student::all();
     
        return view('officio.student.show',compact('students'))->with('title','Griffith Student');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        return view('officio.student.edit',compact('student'))->with('title','Edit Griffith student');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required',
            'ph_no' => 'required',
            'mb_no' => 'required',
            'address' => 'required',
            'email' => 'required',
            'qualification' => 'required',
            'subject' => 'required',
            'title' => 'required',
            'experience' => 'required',
            'work_sector' => 'required',
            'language_test' => 'required',
            'test_score' => 'required',
            'service' => 'required',
          
        ]);
        $student = Student::find($id);
        $input = $request->all();
        $student->update($input);
        session()->flash('message', 'Griffith student Updated.');
        return redirect('admin/student');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        Student::find($id)->delete();


        session()->flash('message', 'Griffith student Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

