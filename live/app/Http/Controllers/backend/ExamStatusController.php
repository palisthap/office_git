<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DhaFees;
use App\ExamStatus;
use DB;
use App\User;
class ExamStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = ExamStatus::all();
      
     
        // $dha_fees=DhaFees::pluck('name','id');
        return view('officio.status.index',compact('status'))->with('title','Exam  Status');
    }

    /**
     * Show the form for creating a new resource.e
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create(Request $request)
    {

      $examstatus = ExamStatus::all();

      $dhaFees_detail=DhaFees::all();
      $dhafees=DhaFees::pluck('name','id');
      return view('officio.status.createdhastatus',compact('dhafees','dhaFees_detail','examstatus'))->with('title','Create Exam Status');
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

        $this->validate($request, [
         'preferred_date_from' => 'required',
         'preferred_date_to' => 'required',
         // 'barcode_no' => 'required',
     


         ]);
        $input = $request->all();
      
 $dha_id=$request->dha_id; 

        if ($input) {     
      
          $status= ExamStatus::create($input);
        
           session()->flash('message', 'Exam Status  Created.');
           // return redirect('admin/examstatus');
            return redirect('admin/statuss?dha_id='.$dha_id);


       }
   }
 
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $ExamStatus = CashOut::find($id);
     $examstatus=ExamStatus::find($id);
  
     $dha_id=ExamStatus::with('dhafees')->where('id',$id)->get();  
     return view('officio.status.editexamstatus',compact('examstatus','dha_id'))->with('title','Edit Exam Status Status');
 }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $this->validate($request, [
           'preferred_date_from' => 'required',
         'preferred_date_to' => 'required',
         // 'dhamcq' => 'required',
         // 'dha_status' => 'required',
            ]);
        $examstatus = ExamStatus::find($id);
        $input = $request->all();
        $examstatus->update($input);
        session()->flash('message', 'Exam Status Updated.');
        return redirect('admin/examstatus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        ExamStatus::find($id)->delete();


        session()->flash('message', 'Exam Status Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

