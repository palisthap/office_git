<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reminder;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\backend\DateConverterController;
class ReminderController extends Controller
{
	public function __construct( DateConverterController $nep_date){
		$this->nep_date = $nep_date;
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function convert_to_eng($nepali_date){
    	$nepali=$nepali_date;
    	$nepali_temp=explode('-', $nepali);
    	$nepali_year=$nepali_temp[0];
    	$nepali_month=$nepali_temp[1];
    	$nepali_day=$nepali_temp[2];
    	$eng_temp=$this->nep_date->nep_to_eng($nepali_year,$nepali_month,$nepali_day);
    	$eng_year=$eng_temp['year'];
    	$eng_month=$eng_temp['month'];
    	$eng_day=$eng_temp['date'];
    	$eng_date=$eng_year.'-'.$eng_month.'-'.$eng_day;
    	return $eng_date;
    }
    public function index()
    {
    	$reminders = Reminder::all();

    	return view('officio.reminder.index',compact('reminders'))->with('title','Reminder');
    }
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()
    {
    	return view('officio.reminder.create')->with('title','Create Griffith reminder');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	$nepali_dates = $request->date;
       // $nepali_date=strtotime('$nepali_dates');
       // dd($nepali_date);
    	$this->validate($request, [
    		'subject' => 'required',
    		'message' => 'required',

    		]);
    	$input = $request->all();
    	$nepali_date=$request->nepali_date;
    	$eng_date=$this->convert_to_eng($nepali_date);
    	$reminder=new Reminder;
        $reminder->user_id=Auth::user()->id;
    	$reminder->subject=$request->subject;
    	$reminder->date=$eng_date;
    	$reminder->nepali_date=$request->nepali_date;
    	$reminder->message=$request->message;     
    	$reminder->status=$request->status;
    	$result=$reminder->save();



    	session()->flash('message', 'Reminder Created.');
    	return redirect('admin/reminder');
        // }

    }



    public function show($id)
    {
        $reminder = Reminder::find($id);
        $reminder->status='0';
        $reminder->update();
        return view('officio.reminder.show',compact('reminder'))->with('title','Show Griffith Reminder');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$reminder = Reminder::find($id);
    	return view('officio.reminder.edit',compact('reminder'))->with('title','Edit Griffith Reminder');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$this->validate($request, [

    		'subject' => 'required',
    		'message' => 'required',

    		]);

    	$input = $request->all();

    	$nepali_date=$request->nepali_date;
    	$eng_date=$this->convert_to_eng($nepali_date);

    	$reminder = Reminder::find($id);
    	$reminder->subject=$request->subject;
    	$reminder->date=$eng_date;
    	$reminder->nepali_date=$request->nepali_date;
    	$reminder->message=$request->message;
    	
    	$reminder->status=$request->status;
        // $result=$reminder->save();
    	$reminder->update();
 
    	session()->flash('message', 'Griffith Reminder Updated.');
    	return redirect('admin/reminder');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	if(!request()->ajax()){
    		return false;
    	}

    	Reminder::find($id)->delete();


    	session()->flash('message', 'Griffith Reminder Deleted.');

    	return response()->json(array(
    		'status' => 'success',
    		));
    }
}
