<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CashOut;
use DB;
use App\Http\Controllers\backend\DateConverterController;
use Validator;
class CashOutController extends Controller
{
   public function __construct( DateConverterController $nep_date){
    $this->nep_date = $nep_date;
}
private function convert_to_eng($nepali_date){
   $nepali=$nepali_date;
   $nepali_temp=explode('-', $nepali);
   $nepali_year=$nepali_temp[0];
   $nepali_month=$nepali_temp[1];
   $nepali_day=$nepali_temp[2];
   $eng_temp=$this->nep_date->nep_to_eng($nepali_year,$nepali_month,$nepali_day);
   $eng_year=$eng_temp['year'];
   $eng_month=$eng_temp['month'];
   $eng_day=$eng_temp['date'];
   $eng_date=$eng_year.'-'.$eng_month.'-'.$eng_day;
   return $eng_date;
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cashOuts = CashOut::all();
        $data=DB::table("cash_outs")->sum('amount');
        return view('officio.cashout.index',compact('cashOuts','data'))->with('title','Cash Out');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
        return view('officio.cashout.create')->with('title','Create Cash Out');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'expenses_type' => 'required',
            'amount' => 'required',
            'paid_by' => 'required',
            'received_by' => 'required',
            'payment_mode' => 'required',
            
            ]);
        $input = $request->all();
        $nepali_date=$request->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);

        CashOut::create([
         'expenses_type' => $request->expenses_type?$request->expenses_type:Null,
         'amount' => $request->amount?$request->amount:Null,
         'date' => $request->eng_date?$request->eng_date:Null,
         'paid_by' => $request->paid_by?$request->paid_by:Null,
         
         'received_by' => $request->received_by?$request->received_by:Null,
         'payment_mode' => $request->payment_mode?$request->payment_mode:Null,
         'nepali_date' => $request->nepali_date?$request->nepali_date:Null,
         'status' => $request->status?$request->status:Null,
         'date' => $eng_date,
         


         ]);
        session()->flash('message', 'Cash Out Created.');
        return redirect('admin/cash_out');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cashOut = CashOut::find($id);
        return view('officio.cashout.edit',compact('cashOut'))->with('title','Edit Cash Out');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'expenses_type' => 'required',
            'amount' => 'required',
            'paid_by' => 'required',
            'received_by' => 'required',
            'payment_mode' => 'required',
            ]);
        $nepali_date=$request->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);
        $cashout = CashOut::find($id);

        $cashout->expenses_type=$request->expenses_type;
        $cashout->date=$eng_date;
        $cashout->nepali_date=$request->nepali_date;
        $cashout->amount=$request->amount;
        $cashout->paid_by=$request->paid_by;
        $cashout->received_by=$request->received_by;
        $cashout->payment_mode=$request->payment_mode;
        $cashout->status=$request->status;
        // $result=$cashout->save();
        $cashout->update();
        
        session()->flash('message', 'Cash Out Updated.');
        return redirect('admin/cash_out');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        CashOut::find($id)->delete();


        session()->flash('message', 'Cash Out Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

