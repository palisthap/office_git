<?php
namespace App\Http\Controllers\backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Cash_in;
use DB;
use App\Report;

use App\Http\Controllers\backend\DateConverterController;
use Validator;
class cash_inController extends Controller
{
   public function __construct( DateConverterController $nep_date){
    $this->nep_date = $nep_date;
}
   
    private function convert_to_eng($nepali_date){
        $nepali=$nepali_date;
        $nepali_temp=explode('-', $nepali);
        $nepali_year=$nepali_temp[0];
        $nepali_month=$nepali_temp[1];
        $nepali_day=$nepali_temp[2];
        $eng_temp=$this->nep_date->nep_to_eng($nepali_year,$nepali_month,$nepali_day);
        $eng_year=$eng_temp['year'];
        $eng_month=$eng_temp['month'];
        $eng_day=$eng_temp['date'];
        $eng_date=$eng_year.'-'.$eng_month.'-'.$eng_day;
        return $eng_date;
    }
    public function index()
    {
        $cashIn = Cash_in::all();
        $data = DB::table("cash_ins")->sum('service_charge');


        return view('officio.cashin.index',compact('cashIn','data'))->with('title','Cash in');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('officio.cashin.create')->with('title','Create Cash in');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $nepali_dates = $request->date;
       // $nepali_date=strtotime('$nepali_dates');
       // dd($nepali_date);
        $this->validate($request, [
            'service_type' => 'required',
            'service_charge' => 'required',


            // 'date' => 'required',

            'paid_by' => 'required',
            'received_by' => 'required',



            ]);
        $input = $request->all();
        $nepali_date=$request->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);

        $cash_in=new Cash_in;
        $cash_in->service_type=$request->service_type;
        $cash_in->date=$eng_date;
        $cash_in->nepali_date=$request->nepali_date;
        $cash_in->service_charge=$request->service_charge;
        $cash_in->paid_by=$request->paid_by;
        $cash_in->received_by=$request->received_by;
        $cash_in->payment_mode=$request->payment_mode;
        $cash_in->status=$request->status;
        $result=$cash_in->save();



        session()->flash('message', 'Cash in Created.');
        return redirect('admin/cash_in');
        // }

    }



    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cashIn = Cash_in::find($id);
        // dd($cashin);
        return view('officio.cashin.edit',compact('cashIn'))->with('title','Edit Visa Type');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
           'service_type' => 'required',
           'nepali_date' => 'required',
           'service_charge' => 'required',
           'paid_by' => 'required',
           'received_by' => 'required',
           'payment_mode' => 'required',



           ]);
        // $cashIn = Cash_in::find($id);
        $input = $request->all();
           $nepali_date=$request->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);

        $cash_in=Cash_in::find($id);
        $cash_in->service_type=$request->service_type;
        $cash_in->date=$eng_date;
        $cash_in->nepali_date=$request->nepali_date;
        $cash_in->service_charge=$request->service_charge;
        $cash_in->paid_by=$request->paid_by;
        $cash_in->received_by=$request->received_by;
        $cash_in->payment_mode=$request->payment_mode;
        $cash_in->status=$request->status;
        // $result=$cash_in->save();
        $cash_in->update();
        session()->flash('message', 'Cash In Updated.');
        return redirect('admin/cash_in');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        Cash_in::find($id)->delete();


        session()->flash('message', 'Cash In Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }

}
