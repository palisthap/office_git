<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CandidatesReport;

class CandidatesReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = CandidatesReport::all();
    
        return view('officio.candidatesreport.index',compact('candidates'))->with('title','Candidates Flow');
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
        return view('officio.candidatesreport.create')->with('title','Create Candidates Report');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'expenses_type' => 'required',
        //     'amount' => 'required',
        //     'paid_by' => 'required',
        //     'received_by' => 'required',
        //     'payment_mode' => 'required',
           
        // ]);
        $input = $request->all();
        if ($input) {
           CandidatesReport::create($input);
            session()->flash('message', 'CandidatesReport Created.');
            return redirect('admin/candidates_report');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidate=CandidatesReport::find($id);
        return view('officio.candidatesreport.show')->with('candidates',$candidate)->with('title','Candidates Report');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate =CandidatesReport::find($id);
        return view('officio.candidatesreport.edit')->with('candidates',$candidate)->with('title','Edit');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'expenses_type' => 'required',
        //     'amount' => 'required',
        //     'paid_by' => 'required',
        //     'received_by' => 'required',
        //     'payment_mode' => 'required',
        // ]);
        $candidates = CandidatesReport::find($id);
        $input = $request->all();
        $candidates->update($input);
        session()->flash('message', 'Candidates Report Updated.');
        return redirect('admin/candidates_report');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        CandidatesReport::find($id)->delete();


        session()->flash('message', 'CandidatesReport Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

