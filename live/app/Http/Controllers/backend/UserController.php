<?php
namespace App\Http\Controllers\backend;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use File;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(User $user){
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->user->all();
        return view('officio.user.index',compact('users'))->with('title','All Users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = UserRole::orderBy('role_type','ASC')->pluck('role_type','id');
        return view('officio.user.create',compact('roles'))->with('title','Create User');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // if(!$request->ajax()){
        //     return false;
        // }
//        var_dump($_FILES); die;
        $validator = Validator::make($request->all(),[
           'username'=>'required|unique:users',
           'password'=>'required|min:6|alpha_dash|confirmed',
           'password_confirmation'=>'required|min:6|alpha_dash',
           'name'=>'required',   
           'phone'=>'required',
           'images'=>'required',
           'user_role_id'=>'required',
       

           ]);

        if($validator->fails()){

            return response()->json(array(
                'status'    => 'fails',
                'errors'    => $validator->getMessageBag()->toArray()
                ));
        }
        if($validator->passes()){
            $inputs = $request->all();

            $inputs['password']=bcrypt($request->password);

            if ($request->file('images'!='')) {
                $destination_path = 'uploads/users';
                $name= time() . "-" . $request->file('images')->getClientOriginalName();
                $image = $destination_path . '/' .$name;

                $request->file('images')->move($destination_path, $image);
                $inputs['images']=$name;
            }


            User::create($inputs);

          session()->flash('message','User Created');
             return redirect('admin/user');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!request()->ajax()){
            return false;
        }


        $user = User::find($id);

        $roles = UserRole::orderBy('role_type','ASC')->pluck('role_type','id');


        return view('officio.user.edit',
            compact(
                'user',
                'roles'
                )
            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//var_dump($request->all()); die;
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'phone'=>'required',
//            'images'=>'required',
            'user_role_id'=>'required',
            ]);

        $user = User::find($id);

        if($validator->passes()){
            $inputs = $request->all();

            if ($request->file('images')!='') {
//                die('here');
                $destination_path = 'uploads/users';
              $name= time() . "-" . $request->file('images')->getClientOriginalName();
                $image = $destination_path . '/' .$name;

                $request->file('images')->move($destination_path, $image);
                $inputs['images']=$name;
            }
            else{
                $inputs['images']=$user->images;
            }
            $user->update($inputs);

            session()->flash('message','User Updated');

            return redirect('admin/user');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        $user = User::find($id);

        $user->delete();

        session()->flash('message', 'User Deleted.');

        return response()->json(array(
            'status' => 'success',
            'url'    => url('admin/user')
            ));
    }
}
