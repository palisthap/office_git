<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ServiceCharge;
use installmentService;
use DB;
class ServiceChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serviceCharges = ServiceCharge::all();
      
        return view('officio.servicecharge.index',compact('serviceCharges'))->with('title','Service Charge');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
        return view('officio.servicecharge.create')->with('title','Create Service Charge');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'qualification' => 'required',
            'applied_for' => 'required',
            'service_charge' => 'required',
           
        ]);
        $input = $request->all();
        if ($input) {
           ServiceCharge::create($input);
            session()->flash('message', 'Service  Created.');
            return redirect('admin/service_charge');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show($id)
    {
        
        $servicecharges=ServiceCharge::with('installmentService')->where('id',$id)->get();
        $servicechargeTotal=ServiceCharge::with('installmentServiceTotal')->where('id',$id)->get();
      return view('officio.servicecharge.show',compact('servicecharges','servicechargeTotal'))->with('title','Service User Profile');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serviceCharge= ServiceCharge::find($id);
        return view('officio.servicecharge.edit',compact('serviceCharge'))->with('title','Edit Service Charge');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required',
            'qualification' => 'required',
            'applied_for' => 'required',
            'service_charge' => 'required',
        ]);
        $serviceCharge = ServiceCharge::find($id);
        $input = $request->all();
        $serviceCharge->update($input);
        session()->flash('message', 'Service Charge Updated.');
        return redirect('admin/service_charge');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        ServiceCharge::find($id)->delete();


        session()->flash('message', 'Service Charge Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

