<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Todo;
use App\User;
use Auth;
use Carbon\Carbon;
use Request;
use App\Cash_in;
use Session;
use App\CashOut;
use App\DhaFees;
use App\InstallmentDha;
use App\ServiceCharge;
use App\Offer;
use App\EnquiryByThem;
use App\EnquiryInOffice;
use App\Enquiry;
use App\Service;
use App\Student;
use App\Transaction;
use Illuminate\Support\Facades\Input;
use Symfony\Component\DomCrawler\Image;
use App\Http\Controllers\backend\DateConverterController;
use DB;

//use Symfony\Component\HttpFoundation\Response;
class TodoController extends Controller {
    public function __construct(DateConverterController $nep_date ){
        date_default_timezone_set("Asia/Kathmandu");
        $this->nep_date = $nep_date;
    }
    private function convert_to_eng($nepali_date){
        $nepali=$nepali_date;
        $nepali_temp=explode('-', $nepali);
        $nepali_year=$nepali_temp[0];
        $nepali_month=$nepali_temp[1];
        $nepali_day=$nepali_temp[2];
        $eng_temp=$this->nep_date->nep_to_eng($nepali_year,$nepali_month,$nepali_day);
        $eng_year=$eng_temp['year'];
        $eng_month=$eng_temp['month'];
        $eng_day=$eng_temp['date'];
        $eng_date=$eng_year.'-'.$eng_month.'-'.$eng_day;
        return $eng_date;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $cash_in=Cash_in::get();
        return json_encode($cash_in);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
//        echo 'harry'; die;
        $input = Request::all();
        $input=json_decode($input['data']);
        $cashin=new Cash_in();
        $cashin->date=$input->date;
        $cashin->service_type=$input->servicetype;
        $cashin->service_charge=$input->servicecharge;
        $cashin->paid_by=$input->paidby;
        $cashin->received_by=$input->receivedby;
        $cashin->payment_mode=$input->paymentmode;
        $cash_entered=$cashin->save();
        if ($cash_entered) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function get_cashIn(){
        $cash_in=Cash_in::limit(5)->orderBy('id','DESC')->get();
        return json_encode($cash_in);
    }
    public function post_cashIn(){
        $input = Request::all();
        $input=json_decode($input['data']);
        $nepali_date=$input->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);
        $cashin=new Cash_in();
        $cashin->date=$eng_date;
        $cashin->nepali_date=$input->nepali_date;
        $cashin->service_type=$input->servicetype;
        $cashin->service_charge=$input->servicecharge;
        $cashin->paid_by=$input->paidby;
        $cashin->received_by=$input->receivedby;
        $cashin->payment_mode=$input->paymentmode;
        $cashin->created_at= Carbon::now()->toDateTimeString();
        $cash_entered=$cashin->save();
        if ($cash_entered) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function edit_cashIn($id){
        $input = Request::all();
        $input=json_decode($input['data']);
        $cashin=Cash_in::findOrFail($id);
        $nepali_date=$input->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);
        $cashin->nepali_date=$input->nepali_date;
        $cashin->date=$eng_date;
        $cashin->service_type=$input->servicetype;
        $cashin->service_charge=$input->servicecharge;
        $cashin->paid_by=$input->paidby;
        $cashin->received_by=$input->receivedby;
        $cashin->payment_mode=$input->paymentmode;
        $cashin->updated_at= Carbon::now()->toDateTimeString();
        $cash_entered=$cashin->update();
        if ($cash_entered) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function delete_cashIn($id){
        $query=Cash_in::find($id)->delete();
        if ($query) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function get_cashOut(){
        $cash_in=CashOut::get();
        return json_encode($cash_in);
    }
    public function post_cashOut(){
        $input = Request::all();
        $input=json_decode($input['data']);
        $nepali_date=$input->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);
        $cashout=new CashOut();
        $cashout->date=$eng_date;
        $cashout->nepali_date=$nepali_date;
        $cashout->expenses_type=$input->expendituretype;
        $cashout->amount=$input->totalamount;
        $cashout->paid_by=$input->paidby;
        $cashout->received_by=$input->receivedby;
        $cashout->payment_mode=$input->paymentmode;
        $cashout->status=$input->status;
        $cashout->created_at= Carbon::now()->toDateTimeString();
        $cash_entered=$cashout->save();
        if ($cash_entered) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function edit_cashOut($id){
//        echo $id; die;
        $input = Request::all();

        $input=json_decode($input['data']);
        $nepali_date=$input->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);
        $cashout=CashOut::findOrFail($id);
        $cashout->date=$eng_date;
        $cashout->nepali_date=$nepali_date;
        $cashout->expenses_type=$input->expendituretype;
        $cashout->amount=$input->totalamount;
        $cashout->paid_by=$input->paidby;
        $cashout->received_by=$input->receivedby;
        $cashout->payment_mode=$input->paymentmode;
        $cashout->status=$input->status;
        $cashout->updated_at= Carbon::now()->toDateTimeString();
        $cash_entered=$cashout->update();
        if ($cash_entered) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function delete_cashOut($id){
        $query=CashOut::find($id)->delete();
        if ($query) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function get_dhaFees(){
        $dhaFees=DhaFees::with('installmentDhaTotal')->get();
//        return $dhaFees[8]['installmentDhaTotal'];
        return json_encode($dhaFees);
    }
    public function get_userInstallment($id){
        $dhaFees=InstallmentDha::where('dha_id',$id)->get();
//        return $dhaFees;
        return json_encode($dhaFees);

    }
    public function delete_userInstallment($id,$ins_id){
        $query=InstallmentDha::where('dha_id',$id)->where('id',$ins_id)->delete();
        if ($query) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function post_dhaFees(){
        $input = Request::all();
        $input=json_decode($input['data']);
        $dhaFees=new DhaFees();
        $dhaFees->name=$input->name;
        $dhaFees->qualification=$input->qualification;
        $dhaFees->applied_for=$input->appliedfor;
        $dhaFees->first_payment=$input->totalamount;
        $dhaFees->status=$input->status;
        $dhaFees->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$dhaFees->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }

    }
    public function edit_dhaFees($id){
//        echo $id; die;
        $input=Request::all();
//        var_dump($input); die;
        $input=json_decode($input['data']);
        $dhaFees=DhaFees::findOrFail($id);
        $dhaFees->name=$input->name;
        $dhaFees->qualification=$input->qualification;
        $dhaFees->applied_for=$input->appliedfor;
        $dhaFees->first_payment=$input->totalamount;
        $dhaFees->status=$input->status;
        $dhaFees->updated_at= Carbon::now()->toDateTimeString();
        $saved_data=$dhaFees->update();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function delete_dhaFees($id){
        $query=DhaFees::findOrFail($id)->delete();
        if ($query) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function post_userInstallment(){
        $input=Request::all();
        $input=json_decode($input['data']);
        $nepali_date=$input->nepali_date;
        $eng_date=$this->convert_to_eng($nepali_date);
        $nepali_deposited_date=$input->deposited_date;
        $eng_deposited_date=$this->convert_to_eng($nepali_deposited_date);
        $installment=new InstallmentDha();
        $installment->dha_id=$input->dha_id;
        $installment->date=$eng_date;
        $installment->nepali_date=$nepali_date;
        $installment->payment_mode=$input->paymentmode;
        $installment->received_by=$input->receivedby;
        $installment->bank_deposited=$input->depositedby;
        $installment->deposited_date=$eng_deposited_date;
        $installment->nepali_deposited_date=$nepali_deposited_date;
        $installment->verified_by=$input->verifiedby;
        $installment->amount=$input->amount;
        $installment->status='1';
        $installment->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$installment->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function get_ServiceCharge(){
        $service_charge=ServiceCharge::get();
        return json_encode($service_charge);

    }
    public function post_ServiceCharge(){
        $input = Request::all();
        $input=json_decode($input['data']);
        $service_charge=new ServiceCharge();
        $service_charge->name=$input->name;
        $service_charge->qualification=$input->qualification;
        $service_charge->applied_for=$input->appliedfor;
        $service_charge->service_charge=$input->servicecharge;
        $service_charge->status=$input->status;
        $service_charge->updated_at= Carbon::now()->toDateTimeString();
        $saved_data=$service_charge->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }

    }
    public function edit_ServiceCharge($id){
        $input=Request::all();
//        var_dump($input); die;
        $input=json_decode($input['data']);
        $service_charge=ServiceCharge::findOrFail($id);
        $service_charge->name=$input->name;
        $service_charge->qualification=$input->qualification;
        $service_charge->applied_for=$input->appliedfor;
        $service_charge->service_charge=$input->servicecharge;
        $service_charge->status=$input->status;
        $service_charge->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$service_charge->update();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function delete_ServiceCharge($id){
        $query=ServiceCharge::findOrFail($id)->delete();
        if ($query) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function get_dhaMcqOffer(){
        echo public_path(); die;
//        echo date(' Y-m-d h:i:s');
//        echo('</br>');
//        echo strtotime(date(' Y-m-d h:i:s'));
//        die;
        $dhaFees=Offer::get();
        return json_encode($dhaFees);

    }
    public function post_dhaMcqOffer(){
//        echo 'harry'; die;
        $this->uploadDhaOffer();
        echo 'Image Uploaded Successfully!!';
         die;
        $input = Request::all();
        $input=json_decode($input['data']);
        $mcqOffer=new Offer();
        $mcqOffer->name=$input->offername;
        $mcqOffer->title=$input->title;
        $mcqOffer->description=$input->description;
        $mcqOffer->percentage=$input->percentage;
        $mcqOffer->image=$input->image;//image upload is remaining to do
        $mcqOffer->email=$input->email;
        $mcqOffer->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$mcqOffer->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }

    public function get_EnquiryByThem(){
        $enquiry=EnquiryByThem::get();
        return json_encode($enquiry);
    }
    public function today_EnquiryByThem(){
        $today=Carbon::today();
        $enquiry=EnquiryByThem::where('date',$today)->get();
        return json_encode($enquiry);
    }
    public function post_EnquiryByThem(){
        $input = Request::all();
        $input=json_decode($input['data']);
        $enquiry=new EnquiryByThem();
        $enquiry->date=$input->date;
        $enquiry->name=$input->name;
        $enquiry->email=$input->email;
        $enquiry->qualification=$input->qualification;
        $enquiry->phone=$input->phone;//image upload is remaining to do
        $enquiry->remarks=$input->remarks;
        $enquiry->status=$input->status;
        $enquiry->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$enquiry->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function edit_EnquiryByThem($id){
        $input=Request::all();
//        var_dump($input); die;
        $input=json_decode($input['data']);
        $enquiry=EnquiryByThem::findOrFail($id);
        $enquiry->date=$input->date;
        $enquiry->name=$input->name;
        $enquiry->email=$input->email;
        $enquiry->qualification=$input->qualification;
        $enquiry->phone=$input->phone;//image upload is remaining to do
        $enquiry->remarks=$input->remarks;
        $enquiry->status=$input->status;
        $enquiry->updated_at= Carbon::now();
        $saved_data=$enquiry->update();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function delete_EnquiryByThem($id){
        $query=EnquiryByThem::findOrFail($id)->delete();
        if ($query) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }

    public function get_EnquiryInOffice(){
        $enquiry=EnquiryInOffice::all();
        return json_encode($enquiry);
    }
    public function today_EnquiryInOffice(){
        $today=Carbon::today();
        $enquiry=EnquiryInOffice::where('date',$today)->get();
        return json_encode($enquiry);
    }
    public function post_EnquiryInOffice(){
        $input = Request::all();
        $input=json_decode($input['data']);
        $enquiry=new EnquiryInOffice();
        $enquiry->date=$input->date;
        $enquiry->name=$input->name;
        $enquiry->email=$input->email;
        $enquiry->qualification=$input->qualification;
        $enquiry->phone=$input->phone;
        $enquiry->remarks=$input->remarks;
        $enquiry->status=$input->status;
        $enquiry->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$enquiry->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function edit_EnquiryInOffice($id){
        $input=Request::all();
//        var_dump($input); die;
        $input=json_decode($input['data']);
        $enquiry=EnquiryInOffice::findOrFail($id);
        $enquiry->date=$input->date;
        $enquiry->name=$input->name;
        $enquiry->email=$input->email;
        $enquiry->qualification=$input->qualification;
        $enquiry->phone=$input->phone;//image upload is remaining to do
        $enquiry->remarks=$input->remarks;
        $enquiry->status=$input->status;
        $enquiry->updated_at= Carbon::now();
        $saved_data=$enquiry->update();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function delete_EnquiryInOffice($id){
        $query=EnquiryInOffice::findOrFail($id)->delete();
        if ($query) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }

    public function get_EnquiryByUs(){
        $enquiry=Enquiry::get();
        return json_encode($enquiry);
    }
    public function today_EnquiryByUs(){
        $today=Carbon::today();
        $enquiry=Enquiry::where('date',$today)->get();
        return json_encode($enquiry);
    }
    public function post_EnquiryByUs(){
//        echo "harry"; die;
        $input = Request::all();
        $input=json_decode($input['data']);
        $enquiry=new Enquiry();
        $enquiry->date=$input->date;
        $enquiry->name=$input->name;
        $enquiry->email=$input->email;
        $enquiry->qualification=$input->qualification;
        $enquiry->phone=$input->phone;
        $enquiry->remarks=$input->remarks;
        $enquiry->status=$input->status;
        $enquiry->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$enquiry->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function edit_EnquiryByUs($id){
        $input=Request::all();
//        var_dump($input); die;
        $input=json_decode($input['data']);
        $enquiry=Enquiry::findOrFail($id);
        $enquiry->date=$input->date;
        $enquiry->name=$input->name;
        $enquiry->email=$input->email;
        $enquiry->qualification=$input->qualification;
        $enquiry->phone=$input->phone;//image upload is remaining to do
        $enquiry->remarks=$input->remarks;
        $enquiry->status=$input->status;
        $enquiry->updated_at= Carbon::now();
        $saved_data=$enquiry->update();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function delete_EnquiryByUs($id){
        $query=Enquiry::findOrFail($id)->delete();
        if ($query) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }

    public function get_service(){
        $service=Service::get();
        return json_encode($service);
    }
    public function post_service(){
        $input = Request::all();
        $input=json_decode($input['data']);
        $service=new Service();
        $service->name=$input->name;
        $service->description=$input->description;
        $service->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$service->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function  get_student(){
//       echo $date=Carbon::now()->toDateTimeString(); die;
        $student=Student::get();
        return json_encode($student);
    }
    public function post_student(){
        $input = Request::all();
        $input=json_decode($input['data']);
        $student=new Student();
        $student->name=$input->name;
        $student->ph_no=$input->phome;
        $student->mb_no=$input->mobile;
        $student->address=$input->address;
        $student->email=$input->email;
        $student->qualification=$input->qualificatioin;
        $student->subject=$input->subject;
        $student->title=$input->titleofqualification;
        $student->experience=$input->yearsosexperience;
        $student->work_sector=$input->worksector;
        $student->language_test=$input->languagetest;
        $student->test_score=$input->languagetestscore;
        $student->service=$input->serviceifintrested;
        $student->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$student->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }
    }
    public function get_transaction(){
        $transaction=Transaction::get();
        return json_encode($transaction);
    }
    public function post_transaction(){
        $input=Request::all();
        $input=json_decode($input['data']);
        $transaction=new Student();
        $transaction->name=$input->name;
        $transaction->date=$input->date;
        $transaction->amount=$input->totalamount;
        $transaction->purpose=$input->purpose;
        $transaction->particular=$input->particular;
        $transaction->created_at= Carbon::now()->toDateTimeString();
        $saved_data=$transaction->save();
        if ($saved_data) {
            return ('success');
        }
        else{
            return 'failure';

        }

    }
    public function uploadImage(){
//        $file = Request::file('image');
        $file = Request::file('image');
        $destinationPath = 'uploads';
        $file->move($destinationPath,time().$file->getClientOriginalName());
        echo 'done';

    }
    public function uploadDhaOffer(){
        echo 'Indra'; die;
        $base=$_REQUEST['image'];
        // Get file name posted from Android App
        $filename = $_REQUEST['filename'];
        // Decode Image
        $binary=base64_decode($base);
        header('Content-Type: bitmap; charset=utf-8');

        // Images will be saved under 'www/imgupload/uplodedimages' folder
        $file = fopen(public_path().'/images'.$filename, 'wb');
//        $file=$binary;
//        $destinationPath = public_path().'\images';
//        $file->move($destinationPath,$file->getClientOriginalName());
        // Create File
        fwrite($file, $binary);
        fclose($file);

    }

    public function cmsInformation(){
        $users=User::with('role')->where('user_role_id','3')->get();

        return json_encode($users);
    }

}
