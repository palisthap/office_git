<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidatesReport extends Model
{
    protected $fillable=['date','name','profession','email','contact_no','DOB','passport_no','service_charge','books','bls','bls_date','GSC_issued_date','equivalent_certificate','DHAMCQ_charge','DHAMCQ_provided','DHAMCQ_performance','preffered_exam_date','email_account','DHA_account','DHA_username','DHA_ref_no','installment1','installment2','dha_fee_paid_date','document_upload','payment1','approval_for_psv_verification','payment2','dataflow_initiated','barcode','exam_eligibility_id','activited','DHAMCQ_performance_revision','selection_of_DHA_exam_date','send_confirmation_to_candidate_email','sms','exam_result','dataflow_report','DHA_eligibility','updated_cv','remarks','status'];
}
