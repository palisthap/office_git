<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnquiryInOffice extends Model
{
       protected $fillable=['date','name','qualification','email','phone','remarks','status']; //
}
