<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reminder extends Model
{
 protected $fillable=['subject','date','message','nepali_date','status','user_id'];

    public function count_remainder(){
        $user_id=Auth::user()->id;
        return $this->where('user_id',$user_id)->where('status','1')->count();
    }
    public function all_remainder(){
        $user_id=Auth::user()->id;
        return $this->where('user_id',$user_id)->where('status','1')->get();
    }
}
