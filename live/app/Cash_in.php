<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash_in extends Model
{
    protected $fillable=['service_type','service_charge','paid_by','received_by','payment_mode','date','nepali_date','is_edit','total','status'];
      

   public function Report()
   {
   	return $this->hasMany('App\Report');
   }

}
