<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceInvoice extends Model
{
   protected $fillable=['serviceinvoice_id','servicecharge_id','date'];
    public function servicecharge(){
    	return $this->belongsTo('App\ServiceCharge','servicecharge_id');
    } 
     public function dhafees(){
    	return $this->belongsTo('App\DhaFees','dha_id');
    }  
}
