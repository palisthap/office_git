<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profille extends Model
{
    protected $fillable=['service_type','pick_up_date','service_charge','paid_by','receive_by','payment_mode','status']
}
