<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $fillable=['date','name','email','qualification','phone','remarks','status'];
}
