<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DhaFees extends Model
{
    protected $fillable=['name','qualification','applied_for','first_payment','is_edit','total','status'];
      public function Report()
   {
   	return $this->hasMany('App\Report');
   }
   public function dhastatus(){
   	return $this->hasMany('App\DhaStatus','dha_id','id');
   }
     public function dataflowstatus(){
    return $this->hasMany('App\DataflowStatus','dha_id');
   }
     public function installmentDha(){
   	return $this->hasMany('App\InstallmentDha','dha_id');
   }
    public function dhainvoice(){
    return $this->hasMany('App\DhaInvoice','dha_id');
   }

    public function serviceinvoice(){
    return $this->hasMany('App\ServiceInvoice','service_charge_id');
   }
    public function installmentDhaTotal(){
//   	return $this->hasMany('App\InstallmentDha','dha_id');
        return $this->hasMany('App\InstallmentDha','dha_id')->selectRaw('installment_dhas.dha_id,sum(installment_dhas.amount) as paid_amount')->groupBy('installment_dhas.dha_id');
   }
}
