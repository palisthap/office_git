<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnquiryByThem extends Model
{
    protected $fillable=['date','name','qualification','email','phone','remarks','status'];
}
