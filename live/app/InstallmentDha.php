<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstallmentDha extends Model
{
    protected $fillable=['id','date','nepali_date','payment_mode','received_by','bank_deposited','verified_by','amount','dha_id','status','deposited_date','nepali_deposited_date'];

        public function Dhafees()
   {
   	return $this->belongsTo('App\DhaFees');
   }
}
