<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
   protected $fillable=['name','ph_no','mb_no','address','email','qualification','subject','title','experience','work_sector','language_test','test_score','service','status'];
}
