<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
  protected $fillable=['name','date','amount','purpose','particular','status'];
}
