<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCharge extends Model
{
  protected $fillable=['id','name','qualification','applied_for','service_charge','total','status'];
    public function Report()
   {
   	return $this->hasMany('App\Report');
   }
   public function invoice(){
   	return $this->hasMany('App\invoice');
   }
      public function installmentService(){
   	return $this->hasMany('App\InstallmentService','servicecharge_id');
   }
    public function serviceinvoice(){
    return $this->hasMany('App\ServiceInvoice','servicecharge_id');
   }
      public function installmentServiceTotal(){
//   	return $this->hasMany('App\InstallmentDha','dha_id');
        return $this->hasMany('App\InstallmentService','servicecharge_id')->selectRaw('installment_services.servicecharge_id,sum(installment_services.service_charge) as paid_amount')->groupBy('installment_services.servicecharge_id');

       
   }
}
