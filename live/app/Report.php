<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
     protected $fillable=['cashin_id','cashout_id','dhafees_id','service_charge_id','status'] ;
   public function cashin()
   {
return $this->belongsTo('App\Cash_in');
   }
   public function cashout()
   {
   	return $this->belongsTo('App\CashOut');
   }

public function dhafees()
{
	return $this->belongsTo('App\DhaFees');

    }
    public function servicecharge()
    {

return $this->belongsTo('App\ServiceCharge');
    }
}
