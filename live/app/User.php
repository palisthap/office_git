<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_role_id',
        'name',      
        'username',
        'password',    
        'email',    
        'lastname',    
        'status',
        'phone'  ,  
        'images',   
        'remember_token',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
//    protected $table='users';

    public function role(){
        return $this->belongsTo('App\UserRole','user_role_id','id');
    }
    public function client()
    {
        return $this->hasMany('App\Client');
    }
  public function dhastatus()
    {
        return $this->hasMany('App\DhaStatus');
    }


}