<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashOut extends Model
{
    protected $fillable=['expenses_type','amount','paid_by','received_by','payment_mode','date','nepali_date','is_edit','total','status'];
      public function Report()
   {
   	return $this->hasMany('App\Report');
   }
}
