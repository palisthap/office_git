<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('api/todo', ['uses' => 'TodoController@index']);
Route::post('api/todo', ['uses' => 'TodoController@store']);
Route::get('api/cashIn', ['uses' => 'TodoController@get_cashIn']);
Route::post('api/cashIn', ['uses' => 'TodoController@post_cashIn']);
Route::post('api/edit_cashIn/{id}', ['uses' => 'TodoController@edit_cashIn']);
Route::post('api/delete_cashIn/{id}', ['uses' => 'TodoController@delete_cashIn']);

Route::get('api/cashOut', ['uses' => 'TodoController@get_cashOut']);
Route::post('api/cashOut', ['uses' => 'TodoController@post_cashOut']);
Route::post('api/edit_cashOut/{id}', ['uses' => 'TodoController@edit_cashOut']);
Route::post('api/delete_cashOut/{id}', ['uses' => 'TodoController@delete_cashOut']);

Route::get('api/dhaFees', ['uses' => 'TodoController@get_dhaFees']);
Route::post('api/dhaFees', ['uses' => 'TodoController@post_dhaFees']);
Route::post('api/edit_dhaFees/{id}', ['uses' => 'TodoController@edit_dhaFees']);
Route::post('api/delete_dhaFees/{id}', ['uses' => 'TodoController@delete_dhaFees']);

/*Installment Api Routes*/
Route::get('api/userInstallment/{id}', ['uses' => 'TodoController@get_userInstallment']);
Route::post('api/userInstallment/{id}/delete/{ins_id}', ['uses' => 'TodoController@delete_userInstallment']);
Route::post('api/userInstallment',['uses'=>'TodoController@post_userInstallment']);


/*service charge routes*/
Route::get('api/addServiceCharge', ['uses' => 'TodoController@get_ServiceCharge']);
Route::post('api/addServiceCharge', ['uses' => 'TodoController@post_ServiceCharge']);
Route::post('api/editServiceCharge/{id}', ['uses' => 'TodoController@edit_ServiceCharge']);
Route::post('api/deleteServiceCharge/{id}', ['uses' => 'TodoController@delete_ServiceCharge']);


Route::get('api/addNewDhaMcqOffer', ['uses' => 'TodoController@get_dhaMcqOffer']);
Route::post('api/addNewDhaMcqOffer', ['uses' => 'TodoController@post_dhaMcqOffer']);

/*Routes for enquiry by Them*/
Route::get('api/addEnquiryByThem', ['uses' => 'TodoController@get_EnquiryByThem']);
Route::get('api/todayEnquiryByThem', ['uses' => 'TodoController@today_EnquiryByThem']);
Route::post('api/addEnquiryByThem', ['uses' => 'TodoController@post_EnquiryByThem']);
Route::post('api/editEnquiryByThem/{id}', ['uses' => 'TodoController@edit_EnquiryByThem']);
Route::post('api/deleteEnquiryByThem/{id}', ['uses' => 'TodoController@delete_EnquiryByThem']);

/*Routes for enquiry In Office*/
Route::get('api/addEnquiryInOffice', ['uses' => 'TodoController@get_EnquiryInOffice']);
Route::get('api/todayEnquiryInOffice', ['uses' => 'TodoController@today_EnquiryInOffice']);
Route::post('api/addEnquiryInOffice', ['uses' => 'TodoController@post_EnquiryInOffice']);
Route::post('api/editEnquiryInOffice/{id}', ['uses' => 'TodoController@edit_EnquiryInOffice']);
Route::post('api/deleteEnquiryInOffice/{id}', ['uses' => 'TodoController@delete_EnquiryInOffice']);

/*Routes for enquiry by us*/
Route::get('api/addEnquiryByUs', ['uses' => 'TodoController@get_EnquiryByUs']);
Route::get('api/todayEnquiryByUs', ['uses' => 'TodoController@today_EnquiryByUs']);
Route::post('api/addEnquiryByUs', ['uses' => 'TodoController@post_EnquiryByUs']);
Route::post('api/editEnquiryByUs/{id}', ['uses' => 'TodoController@edit_EnquiryByUs']);
Route::post('api/deleteEnquiryByUs/{id}', ['uses' => 'TodoController@delete_EnquiryByUs']);

Route::get('api/addServiceManageService', ['uses' => 'TodoController@get_service']);
Route::post('api/addServiceManageService', ['uses' => 'TodoController@post_service']);
Route::get('api/addStudent', ['uses' => 'TodoController@get_student']);
Route::post('api/addStudent', ['uses' => 'TodoController@post_student']);
Route::get('api/addTransaction', ['uses' => 'TodoController@get_transaction']);
Route::post('api/addTransaction', ['uses' => 'TodoController@post_transaction']);
Route::post('api/uploadImage', ['uses' => 'TodoController@uploadImage']);

Route::get('api/cmsInformation', ['uses' => 'TodoController@cmsInformation']);



Route::get('/', 'backend\DashboardController@getLogin');
//Route::get('/login', 'backend\DashboardController@getLogin');
Route::get('admin', 'backend\DashboardController@getLogin');
//by harry
Route::get('/login', array('as' => 'login', 'uses' => 'backend\DashboardController@getLogin'));
Route::get('/logout', array('as' => 'logout', 'uses' => 'backend\DashboardController@getLogout'));
Route::post('/logout', array('as' => 'logout', 'uses' => 'backend\DashboardController@getLogout'));
Route::post('admin', 'backend\DashboardController@postLogin');

Route::get('/{id}','backend\DashboardController@notFound');

Route::group(['middleware'=>['auth']],function(){
    Route::get('admin/expensestype', 'backend\DashboardController@getExpenses');
    Route::post('admin/expensestype', 'backend\DashboardController@postExpenses');
    Route::get('admin/logout', 'backend\DashboardController@getLogout');

    Route::get('admin/{user_id}/dashboard','backend\DashboardController@index');
    Route::get('admin/dashboard','backend\DashboardController@index');
    Route::resource('admin/user','backend\UserController');
    Route::resource('admin/installment','backend\DhaInstallmentController');
    Route::resource('admin/installmentservice','backend\ServiceInstallmentController');
    Route::get('admin/serviceinvoice/{id}','backend\ServiceInstallmentController@viewinvoice');
    Route::post('admin/installment/{id}','backend\DhaInstallmentController@create');
    Route::post('admin/installmentservice/{id}','backend\ServiceInstallmentController@create');
    Route::post('admin/installmentservice/{id}','backend\ServiceInstallmentController@create');
    Route::delete('admin/installmentservice/{id}','backend\ServiceInstallmentController@destroy');
    Route::post('admin/installment','backend\DhaInstallmentController@store');
    Route::delete('admin/installment/{id}','backend\DhaInstallmentController@destroy');
    Route::get('admin/viewinvoice/{id}','backend\DhaInstallmentController@viewinvoice');
    // show report
    Route::post('admin/dhainstallment','backend\DHAController@createdhainstallment');

    Route::get('admin/edit/{id}','backend\DhaController@edit');
    Route::get('admin/editservicecharge/{id}','backend\ServiceChargeController@edit');
    Route::post('admin/json','backend\JsonController@json');
    Route::resource('admin/service_charge','backend\ServiceChargeController');


    Route::resource('admin/cash_in','backend\cash_inController');

    Route::resource('admin/cash_out','backend\CashOutController');

    Route::resource('admin/dha','backend\DHAController');
    Route::get('admin/dhashow/{id}','backend\DHAController@show');
    Route::resource('admin/enquiry','backend\EnquiryController');
    Route::resource('admin/enquirybythem','backend\EnquiryByThemController');
    Route::resource('admin/enquiryinoffice','backend\EnquiryInOFficeController');
    Route::get('admin/dhainvoice','backend\InvoiceController@dhainvoice');
    Route::get('admin/serviceinvoice','backend\InvoiceController@serviceinvoice');
    Route::resource('admin/reminder','backend\ReminderController');
    Route::resource('admin/service','backend\ServiceController');
    Route::resource('admin/student','backend\StudentController');
    Route::resource('admin/transaction','backend\TransactionController');
    Route::resource('admin/offer','backend\OfferController');
    Route::resource('admin/status','backend\StatusController');
    Route::get('admin/viewstatus','backend\ViewStatusController@show');
    Route::get('admin/statuss','backend\StatusController@createdhastatus');
    Route::get('admin/dhastatus','backend\StatusController@createdhastatus');
    Route::post('admin/dhastatuss','backend\StatusController@postdhastatus');
    // Route::resource('admin/dataflow','DataFlowStatusController');
    Route::resource('admin/dataflow','backend\DataFlowStatusController');
    Route::get('admin/dataflowform','backend\DataFlowStatusController@create');
    Route::post('admin/dataflowform','backend\DataFlowStatusController@store');
    Route::resource('admin/examstatus','backend\ExamStatusController');
    Route::get('admin/examstatusform','backend\ExamStatusController@create');
    Route::post('admin/examstatusform','backend\ExamStatusController@store');
    Route::post('admin/contact','backend\OfferController@postOffer');
// CanditatesREport

    Route::resource('admin/candidates_report','backend\CandidatesReportController'); 

   


    // newsletter
    Route::get('admin/news','backend\NewsLetterController@contact');
    Route::post('admin/news','backend\NewsLetterController@postcontact');
    // CMS
    Route::get('admin/cms','backend\cmsController@index');
    Route::get('updatePassword', 'backend\DashboardController@updatePassword');
    Route::get('admin/{username}/profile', 'backend\DashboardController@profile');
    Route::post('/profilepost', 'backend\DashboardController@updatePassword')->name('profilepost');
    Route::post('/profilepost/{id}', 'backend\DashboardController@editInfo');
    Route::post('updateProfile','backend\DashboardController@updateProfile');
    // show report
    Route::post('admin/report','backend\DashboardController@getReport');
    Route::post('admin/nepalicalender','backend\DashboardController@nepalidate');
    Route::post('admin/nepali','backend\DashboardController@nepali');
    Route::post('admin/englishDate','backend\DashboardController@englishDate');
    Route::post('admin/nepalidate2','backend\DashboardController@nepaliDate2');
    Route::post('admin/englishdate2','backend\DashboardController@englishDate2');
    Route::get('admin/{id}','backend\DashboardController@notFound');
    Route::get('admin/{id}/{ids}','backend\DashboardController@notFound');



});

/*Route::get('/', funcbtion () {
    return view('welcome');
});*/

// Route::resource('admin/clienrdetail','clientController');