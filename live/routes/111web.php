<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['namespace'=>'backend'],function(){
  Route::get('/', 'DashboardController@getLogin');

  Route::get('admin', 'DashboardController@getLogin');
  Route::get('admin/expensestype', 'DashboardController@getExpenses');
  Route::post('admin/expensestype', 'DashboardController@postExpenses');
  Route::post('admin', 'DashboardController@postLogin');
  Route::get('admin/logout', 'DashboardController@getLogout');

  // Route::get('admin/{user_id}/dashboard','DashboardController@index');
  Route::get('admin/dashboard','DashboardController@index');
  Route::resource('admin/user','UserController');
  
  Route::resource('admin/user','UserController');
  Route::post('admin/json','JsonController@json');
  Route::resource('admin/service_charge','ServiceChargeController');


  Route::resource('admin/cash_in','cash_inController');

  Route::resource('admin/cash_out','CashOutController');

  Route::resource('admin/DHA','DHAController');
  Route::resource('admin/enquiry','EnquiryController');
  Route::resource('admin/enquirybythem','EnquiryByThemController');
  Route::resource('admin/enquiryinoffice','EnquiryInOFficeController');
  Route::resource('admin/invoice','InvoiceController');
  Route::resource('admin/reminder','ReminderController');
  Route::resource('admin/service','ServiceController');
  Route::resource('admin/student','StudentController');
  Route::resource('admin/transaction','TransactionController');
  Route::resource('admin/offer','OfferController');
  Route::resource('admin/status','StatusController');
  Route::get('admin/dhastatus','StatusController@createdhastatus');
  Route::post('admin/dhastatus','StatusController@postdhastatus');
  Route::resource('admin/dataflow','DataFlowStatusController');
  Route::get('admin/dataflowform','DataFlowStatusController@create');
  Route::post('admin/dataflowform','DataFlowStatusController@store');
  Route::post('admin/contact','OfferController@postOffer');

  Route::resource('admin/examstatus','ExamStatusController');
  Route::get('admin/examstatusform','ExamStatusController@create');
  Route::post('admin/examstatusform','ExamStatusController@store');
    // newsletter
  Route::get('admin/news','NewsLetterController@contact');
  Route::post('admin/news','NewsLetterController@postcontact');
    // CMS
  Route::get('admin/cms','cmsController@index');
  Route::get('updatePassword', 'DashboardController@updatePassword');   
  Route::get('admin/{username}/profile', 'DashboardController@profile');        
  Route::post('/profilepost', 'DashboardController@updatePassword')->name('profilepost');
  Route::post('/profilepost/{id}', 'DashboardController@editInfo');
  Route::post('updateProfile','DashboardController@updateProfile');
  //by harry
  Route::post('admin/report','DashboardController@getReport');
  Route::post('admin/nepalicalender','DashboardController@nepalidate');



});

/*Route::get('/', funcbtion () {
    return view('welcome');
  });*/

     // Route::resource('admin/clienrdetail','clientController');