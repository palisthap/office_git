<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =
            array(
                'user_role_id'=>1,
                'name'=>'Pratap kc',            
                'username'=>'pratapkc',
                'phone'=>'phone',
                'images'=>'dffd.jpg',
                'password'=>Hash::make('Nepal@123'),               
            
            );
        \App\User::insert($users);
    }
}
