<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallmentServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installment_services', function (Blueprint $table) {
            $table->increments('id');
           $table->integer('servicecharge_id')->unsigned();
            $table->foreign('servicecharge_id')
            ->references('id')
            ->on('service_charges')
                ->onDelete('cascade');
            $table->date('date');        
            $table->string('nepali_date');        
            $table->string('service_charge');        
            $table->string('payment_mode');        
            $table->string('received_by');        
            $table->string('bank_deposited');        
                 
       
            $table->date('deposited_date');        
            $table->string('nepali_deposited_date');        
          $table->boolean('status')->default(1);        
                  

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installment_services');
    }
}
