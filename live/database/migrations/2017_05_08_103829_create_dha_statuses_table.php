<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhaStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dha_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dha_id')->unsigned();
            $table->foreign('dha_id')
            ->references('id')
            ->on('dha_fees')
            ->onDelete('cascade');
            // $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')
            // ->references('id')
            // ->on('users')
            // ->onDelete('cascade');
            $table->string('email')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
           $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dha_statuses');
    }
}
