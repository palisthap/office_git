<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('cashin_id')->unsigned();
            $table->foreign('cashin_id')
            ->references('id')
            ->on('cash_ins')
            ->onDelete('cascade');
            $table->integer('cashout_id')->unsigned();
            $table->foreign('cashout_id')
            ->references('id')
            ->on('cash_outs')
            ->onDelete('cascade');
            $table->integer('dhafees_id')->unsigned();
            $table->foreign('dhafees_id')
            ->references('id')
            ->on('dha_fees')
            ->onDelete('cascade');
            $table->integer('service_charge_id')->unsigned();
            $table->foreign('service_charge_id')
            ->references('id')
            ->on('service_charges')

            ->onDelete('cascade');
            $table->boolean('status')->default(1);

           $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
