<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_outs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('expenses_type')->nullable();
            $table->string('amount');
            $table->string('paid_by');
            $table->string('payment_mode')->nullable();
            $table->date('date')->nullable();
            $table->string('nepali_date')->nullable();
            $table->string('is_edit')->nullable();
            $table->float('total')->default(0);
            $table->string('received_by');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_outs');
    }
}
