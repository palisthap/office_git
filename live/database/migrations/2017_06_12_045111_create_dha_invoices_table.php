<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhaInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dha_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dhainvoice_id');
              $table->integer('dha_id')->unsigned();
            $table->foreign('dha_id')
                ->references('id')
                ->on('dha_fees')
                ->onDelete('cascade');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dha_invoices');
    }
}
