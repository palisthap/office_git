<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ph_no');
            $table->string('mb_no');
            $table->string('address');
            $table->string('email');
            $table->string('qualification');
            $table->string('subject');
            $table->string('title');
            $table->string('experience');
            $table->string('work_sector');
            $table->string('language_test')->nullable();
            $table->string('test_score');
            $table->string('service')->nullable();
             $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
