<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallmentDhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installment_dhas', function (Blueprint $table) {
            $table->increments('id');
           $table->integer('dha_id')->unsigned();
            $table->foreign('dha_id')
                ->references('id')
                ->on('dha_fees')
                ->onDelete('cascade');
            $table->date('date');        
            $table->string('nepali_date');        
            $table->string('payment_mode');        
            $table->string('received_by');        
            $table->string('bank_deposited');        
            $table->string('verified_by');        
            $table->string('amount');        
            $table->date('deposited_date');        
            $table->string('nepali_deposited_date');        
          $table->boolean('status')->default(1);        
                  

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installment_dhas');
    }
}
