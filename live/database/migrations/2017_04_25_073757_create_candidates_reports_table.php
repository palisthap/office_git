<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->string('name')->nullable();
            $table->string('profession')->nullable();
            $table->string('email')->nullable();
            $table->integer('contact_no')->nullable();
            $table->date('DOB')->nullable();
            $table->integer('passport_no')->nullable();
            $table->string('service_charge')->nullable();
            $table->string('books')->nullable();
            $table->string('bls')->nullable();
            $table->date('bls_date')->nullable();
            $table->date('GSC_issued_date')->nullable();
            $table->string('equivalent_certificate')->nullable();
            $table->string('DHAMCQ_charge')->nullable();
            $table->string('DHAMCQ_provided')->nullable();
            $table->string('DHAMCQ_performance')->nullable();
            $table->date('preffered_exam_date')->nullable();
            $table->string('email_account')->nullable();
            $table->string('DHA_account')->nullable();
            $table->string('DHA_username')->nullable();
            $table->integer('DHA_ref_no')->nullable();
            $table->string('installment1')->nullable();
            $table->string('installment2')->nullable();
            $table->date('dha_fee_paid_date')->nullable();
            
            $table->string('document_upload')->nullable();
            $table->string('payment1')->nullable();
            $table->string('approval_for_psv_verification')->nullable();
            $table->string('payment2')->nullable();
            $table->string('dataflow_initiated')->nullable();
            $table->string('barcode')->nullable();
            $table->string('exam_eligibility_id')->nullable();
            $table->string('activited')->nullable();
            $table->string('DHAMCQ_performance_revision')->nullable();
            $table->date('selection_of_DHA_exam_date')->nullable();
            $table->string('send_confirmation_to_candidate_email')->nullable();
            $table->string('sms')->nullable();
            $table->string('exam_result')->nullable();
            
            $table->string('dataflow_report')->nullable();
            
            $table->string('DHA_eligibility')->nullable();
            $table->string('updated_cv')->nullable();
            $table->string('remarks')->nullable();
              $table->boolean('status')->default(1);
          


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates_reports');
    }
}
