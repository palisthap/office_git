<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiryInOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiry_in_offices', function (Blueprint $table) {
            $table->increments('id');
                $table->date('date');
            $table->string('name');
            $table->string('qualification');
            $table->string('email');
            $table->string('phone');
            $table->string('remarks');
       $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquiry_in_offices');
    }
}
