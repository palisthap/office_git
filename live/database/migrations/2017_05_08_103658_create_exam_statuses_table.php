<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dha_id')->unsigned();
            $table->foreign('dha_id')
            ->references('id')
            ->on('dha_fees')
            ->onDelete('cascade');
            $table->date('preferred_date_from');
            $table->date('preferred_date_to');

            $table->boolean('dhamcq')->default(1);;
            $table->date('exam_date_booked');
            $table->boolean('dha_status')->default(1);;
            $table->boolean('eligibility')->default(1);;
           $table->boolean('status')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_statuses');
    }
}
