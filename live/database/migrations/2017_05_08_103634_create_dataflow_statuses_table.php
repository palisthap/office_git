<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataflowStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataflow_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dha_id')->unsigned();
            $table->foreign('dha_id')
            ->references('id')
            ->on('dha_fees')
            ->onDelete('cascade');
            $table->string('passport_no');
            $table->string('dha_ref_no');
            $table->string('barcode_no');
            $table->string('remarks');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dataflow_statuses');
    }
}
