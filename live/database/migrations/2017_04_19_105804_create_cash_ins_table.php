<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashInsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_ins', function (Blueprint $table) {
            $table->increments('id');
            // $table->enum('service_type',['D','B'])->nullable();
            $table->string('service_type'); 
            $table->date('date')->nullable();
            $table->string('service_charge');
            $table->string('paid_by');
            $table->string('received_by');
            // $table->enum('payment_mode',['c','ch','b'])->nullable();
            $table->string('payment_mode')->nullable(); 
            $table->string('nepali_date')->nullable();
            $table->string('is_edit')->nullable();
            $table->float('total')->default(0);
             $table->boolean('status')->default(1);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_ins');
    }
}
