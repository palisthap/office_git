@extends('officio.main')

@section('content')

<section class="content-header">
  <h1>
    General Information
  </h1>
</section>
<div class="row">
  <div class="col-lg-3 col-md-2">

    {{--@include('flash.message')--}}
    {{--@include('errors.list')--}}
  </div>
  <div class="col-lg- col-md-5">
    <div class="page-title-box">
      <ol class="breadcrumb pull-right">
        <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
        <li class="active">Profile</li>
      </ol>
      <h4 class="page-title"></h4>
    </div>
  </div>
</div>


<section class="content">
  <div class="row">
    <div class="col-md-12">

      {{--@include('flash.message')--}}
      {{--@include('errors.list')--}}

      <!-- Horizontal Form -->
      <div class="box box-info">
        <!-- form start -->
        <div class="box-body">



          <br></br>

          <div class="row">
           <!--  <div class="col-lg-3 col-md-2"> -->


           <!--   </div> --> <!-- end col -->
           {{--@include('flash.message')--}}
           {{--@include('errors.list')--}}

           <div class="col-md-12 ">
             <div class="col-md-6 ">
               <div class="panel panel-warning">
      <div class="panel-heading">Edit Info</div>
   
    </div>
             <!--  <h2>Edit Info</h2> -->
              <form action="{{ url('/profilepost/'.$users->id) }}" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="col-md-3">
                      <label for="name">Name</label>
                    </div>
                    <div class="col-md-9">
                      <input type="name" name="name"  value="<?php echo $users->name; ?>" class="form-control" id="name">
                      @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                      <span class="error-message"></span>
                    </div>

                  </div>
                  <br> </br>


                  <div class="form-group"> 
                    <div class="col-md-3">
                      <label for="username">username</label>
                    </div>
                    <div class="col-md-9">  <input type="username" name="username" value="<?php echo $users->username; ?>" class="form-control" id="username">
                      @if ($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                      <span class="error-message"></span>
                    </div>
                  </div>

                  <br> </br>
                  <div class="text-right border-top">
                   <center>   <button type="submit" class="btn btn-warning">Save</button></center>
                 </div>
                 <input type="hidden" value="{{ Session::token() }}" name="_token">
               </form>

             </div>
           </div>               
           <div class="row">

            {{--@include('flash.message')--}}
            {{--@include('errors.list')--}}

            <div class="col-md-6 ">
            <div class="panel panel-info">
      <div class="panel-heading"><i class="fa fa-cog" aria-hidden="true"></i>Update Password</div>
    
    </div>
            <!--   <h2>Update Password</h2> -->
              <form action="{{ url('/profilepost') }}" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="col-md-3">
                      <label for="old_password">Current Password</label>
                    </div>
                    <div class="col-md-9">
                      <input type="password" name="old_password" class="form-control" id="old_password">
                      @if ($errors->has('old_password')) <p class="help-block">{{ $errors->first('old_password') }}</p> @endif
                      <span class="error-message"></span>
                    </div>
                  </div>
                  <br></br>
                  <div class="form-group">
                    <div class="col-md-3">
                      <label for="name"> New Password</label>
                    </div>
                    <div class="col-md-9">
                      <input type="password" name="new_password" class="form-control" id="new_password">
                      @if ($errors->has('new_password')) <p class="help-block">{{ $errors->first('new_password') }}</p> @endif
                      <span class="error-message"></span>
                    </div></div>
                    <br></br>
                    <div class="form-group">
                     <div class="col-md-3">
                      <label for="name">Confirm Password</label>
                    </div>
                    <div class="col-md-9">
                      <input type="password" name="confirm_password" class="form-control" id="confirm_password">
                      @if ($errors->has('confirm_password')) <p class="help-block">{{ $errors->first('confirm_password') }}</p> @endif
                    </div></div>
                    <br></br>
                    <div class="text-right border-top"><center>
                      <button type="submit" class="btn btn-warning">Save</button></center>
                    </div>
                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                  </form>
                </div></div>
              </div></div></div>
            </table>
          </div>
          </div>
        </div>
      </div><!-- /.box -->


    </div><!-- /.col -->
  </div>
</section>
@stop