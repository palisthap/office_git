<div class="box-body">
     <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">Name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
            {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' Name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

       <div class="form-group" {{ $errors->has('qualification') ? ' has-error' : '' }}>
                        <label for="qualification" class="col-sm-2 control-label">Qualification:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">
                                {!! Form::text('qualification', null , ['class'=> 'form-control', 'placeholder' => ' Qualification', 'id'=>"qualification"]) !!}

                                @if ($errors->has('qualification'))
                                <span class="help-block">
                                    <strong> {{ $errors->first('qualification') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
    <div class="form-group" {{ $errors->has('applied_for') ? ' has-error' : '' }}>
        <label for="applied_for" class="col-sm-2 control-label">Applied For:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
            {!! Form::text('applied_for', null , ['class'=> 'form-control', 'placeholder' => ' Applied For', 'id'=>"applied_for"]) !!}

            @if ($errors->has('applied_for'))
                <span class="help-block">
                    <strong> {{ $errors->first('applied_for') }}</strong>
                </span>
            @endif

        </div>
    </div>
   
    <div class="form-group" {{ $errors->has('service_charge') ? ' has-error' : '' }}>
        <label for="service_charge" class="col-sm-2 control-label">Service Charge:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
            {!! Form::text('service_charge', null , ['class'=> 'form-control', 'placeholder' => ' Service Charge', 'id'=>"service_charge"]) !!}

            @if ($errors->has('service_charge'))
                <span class="help-block">
                    <strong> {{ $errors->first('service_charge') }}</strong>
                </span>
            @endif

        </div>
    </div>
  


  

  <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
</div>