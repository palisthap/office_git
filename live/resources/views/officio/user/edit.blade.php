{!! Form::model($user, [
        'route' => ['user.update', $user->id],
        'class' => 'form-horizontal ',
        'method'=> 'PUT',
        'files'=>'true'
    ])
!!}

<div class="row">
    <div class="col-md-6">
        @include('officio.user.form')
    </div>
</div>

<div class="text-right border-top">
    <button type="submit" class="btn btn-warning">Update</button>
</div>
</div>


{!! Form::close() !!}

