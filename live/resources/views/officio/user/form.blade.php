
<div class="box-body">

    <div class="form-group">
        <label for="first_name" class="col-sm-2 control-label"> Name<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
        <div class="col-sm-10">
            {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => 'First Name', 'id'=>"name"]) !!}
            <span class="error-message"></span>
        </div>
    </div>
    <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Username<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
        <div class="col-sm-10">
            {!! Form::text('username', null , ['class'=> 'form-control', 'placeholder' => 'Username', 'id'=>"username"]) !!}
            <span class="error-message"></span>
        </div>
    </div>
      <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Phone<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
        <div class="col-sm-10">
            {!! Form::text('phone', null , ['class'=> 'form-control', 'placeholder' => 'Phone', 'id'=>"phone"]) !!}
            <span class="error-message"></span>
        </div>
    </div>
      

    @if(Request::segment(4) != 'edit')
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Password<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
            <div class="col-sm-5">
                {!! Form::input('password', 'password', null , ['class'=> 'form-control', 'placeholder' => 'Password', 'id'=>"password"]) !!}
                <span class="error-message"></span>
            </div></div>
             <div class="form-group">
            <label for="password" class="col-sm-2 control-label"> Confirm Password<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
            <div class="col-sm-5">
                {!! Form::input('password', 'password_confirmation', null , ['class'=> 'form-control', 'placeholder' => 'Confirm Password', 'id'=>"password_confirmation"]) !!}
                <span class="error-message"></span>
            </div>
        </div>
    @endif


  

    <div class="form-group">
        <label for="country" class="col-sm-2 control-label">User Role<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-5">
            {{--@if($request::segment(4) != 'edit')--}}
            {{ Form::select('user_role_id', $roles, null, ['placeholder'=>'--Select User Role--', 'class'=>'form-control']) }}
            <span class="error-message"></span>
            {{--@else--}}
            {{--<select name="user_role_id" class="form-control">--}}
            {{--@foreach($roles as $role)--}}
            {{--<option value="{{ $role->id }}" @if($role->id == $role->role->id) selected @endif >--}}
            {{--{{ $role->role_type }}--}}
            {{--</option>--}}
            {{--@endforeach--}}
            {{--</select>--}}
            {{--@endif--}}

        </div>

    </div>
    <div class="form-group">
        {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
            <div class="col-sm-8">
                        {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                        {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


            </div>
        </div>

</div>







