 <div class="form-group" {{ $errors->has('emailto') ? ' has-error' : '' }}>
        <label for="emailto" class="col-sm-2 control-label"><span class=help-block"
                                                                    style="color: #b30000">&nbsp; </span></label>

        <div class="col-sm-8">
          {!! Form::hidden('emailto', null , ['class'=> 'form-control', 'placeholder' => ' emailto', 'id'=>"emailto"]) !!}

            @if ($errors->has('emailto'))
                <span class="help-block">
                    <strong> {{ $errors->first('emailto') }}</strong>
                </span>
            @endif

        </div>
    </div>
<div class="box-body">

    
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">Name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('title') ? ' has-error' : '' }}>
            <label for="title" class="col-sm-2 control-label">Title:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('title', null , ['class'=> 'form-control', 'placeholder' => ' title', 'id'=>"title"]) !!}

                    @if ($errors->has('title'))
                    <span class="help-block">
                        <strong> {{ $errors->first('title') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
    <div class="form-group" {{ $errors->has('description') ? ' has-error' : '' }}>
        <label for="description" class="col-sm-2 control-label">Description:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
            {{ Form::textarea('description',null,['class'=>'form-control ckeditor','placeholder'=>'Enter content ']) }}

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong> {{ $errors->first('description') }}</strong>
                </span>
            @endif

        </div>
    </div>
 
    <div class="form-group" {{ $errors->has('percentage') ? ' has-error' : '' }}>
        <label for="percentage" class="col-sm-2 control-label">Percentage:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('percentage', null , ['class'=> 'form-control', 'placeholder' => ' percentage', 'id'=>"percentage"]) !!}

            @if ($errors->has('percentage'))
                <span class="help-block">
                    <strong> {{ $errors->first('percentage') }}</strong>
                </span>
            @endif

        </div>
    </div>

   
    <div class="form-group">

        <label for="image" class="col-sm-3 control-label">
            Featured Image:
        </label>

        <div class="col-sm-3"><input onchange="document.getElementById('thumb').src = window.URL.createObjectURL(this.files[0])"
                                     name="image" type="file" placeholder="">
            Upload Image
        </div>

        <div class="col-md-12 col-md-offset-3">
            <img  width="160px" height="120px"  id="thumb" />
        </div>


    </div>
     <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
        <label for="email" class="col-sm-2 control-label">Email:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('email', null , ['class'=> 'form-control', 'placeholder' => ' email', 'id'=>"email"]) !!}

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong> {{ $errors->first('email') }}</strong>
                </span>
            @endif

        </div>
    </div>
      
  <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
</div>