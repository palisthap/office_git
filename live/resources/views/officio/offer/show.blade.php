@extends('officio.main')


@section('content')
<!DOCTYPE html>
<html>
<head>
	<title>EID OFFER</title>
	
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style type="text/css">
        .background-color-main 
        {
            background: linear-gradient(270deg, #15632f, #a71c1c, #E7E7E7, #504c4c, #2798d8);
            background-size: 1000% 1000%;
            -webkit-animation: gifback 30s ease infinite;
            -moz-animation: gifback 30s ease infinite;
            -o-animation: gifback 30s ease infinite;
            animation: gifback 30s ease infinite;
        }
        @-webkit-keyframes gifback {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-moz-keyframes gifback {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-o-keyframes gifback {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @keyframes gifback { 
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }

        .eid-special-offer
        {
            text-align: center;
            font-size: 20px;
            color: white;
            font-weight: bold;
        }

        .eid-mubarak
        {
            text-align: center;
            font-size: 24px;
            color: white;
            font-weight: bold;
        }

        .additional-off
        {
            text-align: center;
            font-size: 21px;
            color: white;
            font-weight: bold;
        }

        .details
        {
            font-size: 16px;
            color: white;
            text-align: center;

        }

        .width-img
        {
            width: 570px !important;
        }

        .name
        {
            width: 250px;
            height: 70px;
            padding-left: 5px;
            font-size: 20px;
        }

        .email
        {
            width: 504px;
            margin: 10px 0 10px 0;
            height: 75px;
            padding-left: 5px;
            font-size: 20px;
        }

        .btn-submit
        {
            width: 504px;
            height: 85px;
            text-align: center;
            font-size: 20px;
            font-weight: bold;
            background-color: #3ba160;
            color: white;
        }

        
        .img-form-main
        {
            margin-top: 10px;
        }


    </style>
</head>
<body>
<?php

  ?>


    <div class="container background-color-main">
     <div class="row">
      <div class="col-md-12">
       <div class="eid-special-offer">
        <br><?php echo $offer->title; ?>	
    </div>

    <div class="eid-mubarak">
        <br><?php echo $offer->name; ?>	
    </div>

    <div class="additional-off">
        <p><?php echo $offer->description; ?></p>
    </div>

    <div class="details">
        <p>Use the coupon code below</p><p> when you checkout to get an additional <?php echo $offer->percentage; ?>	 off your purchase and be eligible for future discounts when you join our mailing list.</p>
    </div>
</div>

<div class="col-md-12 img-form-main">
   <div class="col-md-6 width-img">
       <!-- 	<img src="<?php echo $offer->image; ?>" alt="eid-images" width="500px">	 -->			<img src="{{asset($offer->image)}} " alt="eid-images" width="500px"> 
   </div>
   <div class="col-md-6">
       
    {{ Form::open(['url'=>'admin/contact', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
    <input type="text" name="first_name" placeholder="First Name" class="name" required/>
    <input type="text" name="last_name" placeholder="Last Name" class="name" required/>
   <!--  <input type="hidden" name="toemail" placeholder="Email" value="<?php echo $offer->email;?>" class="email" required/> -->
    <input type="text" name="emailto" placeholder="Email"  class="email" required/>
    <p>
        <input type="submit" name="btnSubmit" value="GET MY <?php echo $offer->percentage; ?> OFF!" class="btn-submit"></p>
        
        {{ form::close() }}
    </div>
</div>
</div>
</div>
</body>
</html>
@stop