<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>{{ $title }}</title>
  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <script src="{{URL::to('//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js')}}"></script>
<!--   <script type="text/javascript" src="{{URL::to('js/nepalidatepicker/nepali.datepicker.v2.2.min.js')}}"></script>
  <script src="{{URL::to('//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{URL::to('js/nepalidatepicker/nepali.datepicker.v2.2.min.css')}}" />
-->  <!-- Bootstrap 3.3.5 -->

{{ Html::style('bootstrap/css/bootstrap.min.css') }}

<!-- Font Awesome -->
{{ Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}

<!-- {{ Html::style('font-awesome/font-awesome.min.css') }}-->

<!-- Ionicons -->
{!! Html::script('plugins/ckeditor/ckeditor.js') !!}
{{ Html::style('font-awesome/ionicons.min.css') }}


{{ Html::style('plugins/datepicker/datepicker3.css') }}

{{ Html::style('plugins/daterangepicker/daterangepicker-bs3.css') }}



{{ Html::style('fonts/font/flaticon.css') }}

<!-- Select2 -->


{{ Html::style('choosen/css/chosen.min.css') }}

<!-- Theme style -->

{{ Html::style('dist/css/AdminLTE.min.css') }}

            <!-- AdminLTE Skins. We have chosen the skin-blue for this starter

          page. However, you can choose any other skin. Make sure you

          apply the skin class to the body tag so the changes take effect.

        -->
        <link rel="stylesheet" href="{{URL::to('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}">
<!--         <script src="{{URL::to('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script>
-->        <script src="{{URL::to('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>
{{ Html::style('dist/css/skins/skin-blue.min.css') }}


{{ Html::style('plugins/iCheck/all.css') }}
{{ Html::style('/dist/css/sweetalert.css') }}

<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{URL::to('css/screen.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{URL::to('css/bootstrap.min.css')}}"> 
<script type="text/javascript" href="{{URL::to('js/jQuery.min.js')}}"></script>
<!--  <script type="text/javascript" href="{{URL::to('js/nepali.datepicker.v2.min.js')}}"></script> -->
<script type="text/javascript" href="{{URL::to('js/bootstrap.min.js')}}"></script>
<script src="{{URL::to('js/jQuery.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::to('js/jquery.validate.min.js')}}"></script>
{{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}


{{  Html::style('css/officio.css') }}
{{ Html::style('css/radiobutton.css') }}

{{ Html::style('css/gallery.css') }}


{{ Html::style('plugins/timepicker/bootstrap-timepicker.css') }}



{{--{!! Html::script("http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js") !!}--}}



<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->


<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--[if lt IE 9] -->

{{ Html::script('js/html5shiv.min.js') }}

{{ Html::script('js/respond.min.js') }}

{{ Html::script('plugins/jQuery/jQuery-2.1.4.min.js') }}

{{ Html::script('chart/highcharts.js') }}
{{ Html::script('chart/exporting.js') }}


</head>


<body class="hold-transition skin-blue sidebar-mini">
  @inject('remainders', 'App\Reminder')


  <div class="wrapper">

    {{----}}

    {{----}}


    <!-- Main Header -->

    <header class="main-header">


      <!-- Logo -->

      <a href="{{ url('admin/dashboard') }}" class="logo">

        <!-- mini logo for sidebar mini 50x50 pixels -->

        <span class="logo-mini"></span>

        <!-- logo for regular state and mobile devices -->

        <span class="logo-lg">

          {{ Html::image('images/green.png', '') }}

        </span>

      </a>


      <!-- Header Navbar -->

      <nav class="navbar navbar-static-top" role="navigation">

        <!-- Sidebar toggle button-->

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

          <span class="sr-only">Toggle navigation</span>

        </a>

        <!-- Navbar Right Menu -->

        <div class="navbar-custom-menu">

          <ul class="nav navbar-nav">



            @if(Auth::user())
            <!-- Remainder Notification -->
            <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">{{$remainders->count_remainder()}}</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have {{$remainders->count_remainder()}} notifications</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    @foreach($remainders->all_remainder() as $remainder)
                    <li>
                      <a href="{{URL::to('/').'/admin/reminder/'.$remainder->id}}">
                        <i class="fa fa-bell"></i> {{$remainder->subject}}
                      </a>
                    </li>
                    @endforeach

                  </ul>
                </li>
                <li class="footer"><a href="{{URL::to('/admin/reminder')}}">View all</a></li>
              </ul>
            </li>
            <!-- User Account Menu -->

            <li class="dropdown user user-menu">

              <!-- Menu Toggle Button -->

              <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                <!-- The user image in the navbar-->



                {{ Auth::user()->name }} 





                <!-- hidden-xs hides the username on small devices so only the image appears. -->

                <!-- {{--<span class="hidden-xs"> {{ Auth::user()->first_name }}</span>--}} -->

              </a>

              <ul class="dropdown-menu">

                <!-- The user image in the menu -->

                <li class="user-header">


                  {{ Html::image('images/pratap.jpg', 'User Image', ['class'=>'img-circle']) }}

                  <p>
                    {{ Auth::user()->name }} 


                  </p>



                </li>


                <!-- Menu Body -->



                <!-- Menu Footer-->

                <li class="user-footer">
                 @if(Auth::user())


                 @if(Auth::user()->user_role_id == 1)

                 <div class="pull-left">
                  <?php 
                  $user_name=Auth::User()->username;

                  ?>

                  <a href="{{URL('admin/'.$user_name.'/profile')}}" class="btn btn-default">Profile</a>

                </div>

                @endif


                @endif
                <div class="pull-right">

                  <a href="{{ url('admin/logout') }}" class="btn btn-warning">Sign out</a>

                </div>

              </li>

            </ul>

          </li>

          <!-- Control Sidebar Toggle Button -->

          {{--<li>--}}

          {{--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>--}}

          {{--</li>--}}

        </ul>
        @endif

      </div>

    </nav>

  </header>

  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">




    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">


      <!-- Sidebar Menu -->

      <ul class="sidebar-menu nav" id="side-menu">


        <!-- Optionally, you can add icons to the links -->


        @if(Auth::user())


        @if(Auth::user()->user_role_id == 1)

        <li class="treeview">

          <a href="#">

            <i class="fa fa-user" aria-hidden="true" style="font-size:18px"></i>

            <span>User Management </span>

            <i class="fa fa-angle-left pull-right"></i>

          </a>

          <ul class="treeview-menu">

            <li><a href="{{ url('admin/user/create') }}">Add New</a></li>

            <li><a href="{{ url('admin/user') }}">All Client</a></li>



            
          </ul>
          <li class="treeview">

            <a href="#">

              <i class="fa fa-pie-chart"></i>

              <span>Daybook</span>

              <i class="fa fa-angle-left pull-right"></i>

            </a>

            <ul class="treeview-menu">



             <li><a href="#"> Cash In</a></li>
             <li><a href="{{ url('admin/cash_in/create') }}"> <i class="fa fa-plus">Add Cash In</i></a></li>

             <li><a href="{{ url('admin/cash_in') }}"><i class="fa fa-eye">View Cash In</i></a></li>


           </ul>

           <ul class="treeview-menu">



             <li><a href="#"> Cash Out</a></li>
             <li><a href="{{ url('admin/cash_out/create') }}"> <i class="fa fa-plus">Add Cash Out</i></a></li>

             <li><a href="{{ url('admin/cash_out') }}"><i class="fa fa-eye">View Cash Out</i></a></li>


           </ul>   

           <ul class="treeview-menu">



             <li><a href="#"> DHA/Fees</a></li>
             <li><a href="{{ url('admin/dha/create') }}"> <i class="fa fa-plus">Add DHA/Fees</i></a></li>

             <li><a href="{{ url('admin/dha') }}"><i class="fa fa-eye">View DHA/Fees</i></a></li>


           </ul>   
           <ul class="treeview-menu">



             <li><a href="#"> Service Charge</a></li>
             <li><a href="{{ url('admin/service_charge/create') }}"> <i class="fa fa-plus">Add Service Charge</i></a></li>

             <li><a href="{{ url('admin/service_charge') }}"><i class="fa fa-eye">View Service Charge</i></a></li>
             <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-file-powerpoint-o">View Records</i></a></li>



           </ul>  
               <!-- <li class="treeview">

                <a href="#">

                  <i class="fa fa-id-badge" aria-hidden="true" style="font-size:18px"></i>

                  <span>Griffith Enquiry </span>

                  <i class="fa fa-angle-left pull-right"></i>

                </a>

                <ul class="treeview-menu">

                 <li><a href="#"> Enquiry By Us</a></li>

                 <li><a href="{{ url('admin/enquiry/create') }}"> <i class="fa fa-plus">Add New</i></a></li>

                 <li><a href="{{ url('admin/enquiry') }}"><i class="fa fa-eye-slash">View All Enquiries</i></a></li>



               </li>

               <li><a href="#"> Enquiry By Them</a></li>

               <li><a href="{{ url('admin/enquirybythem/create') }}"> <i class="fa fa-plus">Add New</i></a></li>

               <li><a href="{{ url('admin/enquirybythem') }}"><i class="fa fa-eye-slash">View All Enquiries</i></a></li>



             </li>


           </li>
           </ul>
           <li class="treeview">

            <a href="#">

              <i class="fa fa-id-badge" aria-hidden="true" style="font-size:18px"></i>

              <span> Candiddates Flow </span>

              <i class="fa fa-angle-left pull-right"></i>

            </a>

            <ul class="treeview-menu">

             <li><a href="#"> Candiddates Flow</a></li>

           
             <li><a href="{{ url('admin/candidates_report') }}"><i class="fa fa-eye-slash">Candidates Flow</i></a></li>



           </li>

         



         </li> -->
         <li class="treeview">

          <a href="#">

            <i class="fa fa-pie-chart" aria-hidden="true" style="font-size:18px"></i>

            <span>Griffith Enquiry </span>

            <i class="fa fa-angle-left pull-right"></i>

          </a>

          <ul class="treeview-menu">

           <li><a href="#"> Enquiry By Us</a></li>

           <li><a href="{{ url('admin/enquiry/create') }}"> <i class="fa fa-plus">Add New</i></a></li>
           <li><a href="{{ url('admin/enquiry/show') }}"> <i class="fa fa-plus">View Today's Enquiries</i></a></li>

           <li><a href="{{ url('admin/enquiry') }}"><i class="fa fa-eye-slash">View All Enquiries</i></a></li>



           <!--  </li> -->

           <li><a href="#"> Enquiry By Them</a></li>

           <li><a href="{{ url('admin/enquirybythem/create') }}"> <i class="fa fa-plus">Add New</i></a></li>
           <li><a href="{{ url('admin/enquirybythem/show') }}"> <i class="fa fa-plus">View Today's Enquiries</i></a></li>
           <li><a href="{{ url('admin/enquirybythem') }}"><i class="fa fa-eye-slash">View All Enquiries</i></a></li>




           <li><a href="#"> Enquiry In Office</a></li>

           <li><a href="{{ url('admin/enquiryinoffice/create') }}"> <i class="fa fa-plus">Add New</i></a></li>
           <li><a href="{{ url('admin/enquiryinoffice/show') }}"> <i class="fa fa-plus">View Today's Enquiries</i></a></li>
           <li><a href="{{ url('admin/enquiryinoffice') }}"><i class="fa fa-eye-slash">View All Enquiries</i></a></li>



<!-- 
</li> -->
</ul>

</li>

<li class="treeview">

  <a href="#">

    <i class="fa fa-print" aria-hidden="true" style="font-size:18px"></i>

    <span>Printed Invoice </span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/dhainvoice') }}"><i class="fa fa-newspaper-o">DHA</i></a></li>

    <li><a href="{{ url('admin/serviceinvoice') }}"><i class="fa fa-file-powerpoint-o">Service Charge</i></a></li>


    <!--    </li> -->
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-bell" aria-hidden="true" style="font-size:18px"></i>

    <span>Reminder </span> <small style="margin-right:20px !important;" id="notic" class="label pull-right bg-blue ">{{ $remainders->count_remainder() }}</small>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/reminder') }}"><i class="fa fa-eye">Add Reminder</i></a></li>



    <!--    </li> -->
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-reddit" aria-hidden="true" style="font-size:18px"></i>

    <span>Manage Services </span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/service') }}"><i class="fa fa-eye">All Services</i></a></li>



    <!--    </li> -->
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-reddit" aria-hidden="true" style="font-size:18px"></i>

    <span>Manage Student </span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/student') }}"><i class="fa fa-eye">All Students</i></a></li>



    <!--    </li> -->
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-reddit" aria-hidden="true" style="font-size:18px"></i>

    <span>Manage Transaction </span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/transaction') }}"><i class="fa fa-eye">All transactions</i></a></li>



    <!--    </li> -->
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-reddit" aria-hidden="true" style="font-size:18px"></i>

    <span>DHAMCQ Offer </span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/offer') }}"><i class="fa fa-gears">All Offers</i></a></li>
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-unlock-alt" aria-hidden="true" style="font-size:18px"></i>

    <span>Status </span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/status') }}"><i class="fa fa-gears">Add Status</i></a></li>
    <li><a href="{{ url('admin/viewstatus') }}"><i class="fa fa-gears">Manage Status</i></a></li>
    
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-envelope" aria-hidden="true" style="font-size:18px"></i>

    <span>News Letter </span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/news') }}"><i class="fa fa-mail-forward">Send Emails</i></a></li>



    <!--    </li> -->
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-gears" aria-hidden="true" style="font-size:18px"></i>

    <span>CMS </span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/cms') }}"><i class="fa fa-eye">CMS</i></a></li>



    <!--    </li> -->
  </ul>
</li>
<li class="treeview">

  <a href="#">

    <i class="fa fa-gears" aria-hidden="true" style="font-size:18px"></i>

    <span>Candidates Report</span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/candidates_report') }}"><i class="fa fa-eye">Candidates Report</i></a></li>



    
  </ul>
</li>
<!-- <li class="treeview">

  <a href="#">

    <i class="fa fa-gears" aria-hidden="true" style="font-size:18px"></i>

    <span>Installment</span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/installment') }}"><i class="fa fa-eye">Dhainstallmetn</i></a></li>



  
  </ul>
</li> -->
<!-- <li class="treeview">

  <a href="#">

    <i class="fa fa-gears" aria-hidden="true" style="font-size:18px"></i>

    <span>Service Installment</span>

    <i class="fa fa-angle-left pull-right"></i>

  </a>

  <ul class="treeview-menu">



    <li><a href="{{ url('admin/installmentservice') }}"><i class="fa fa-eye">Dhainstallmetn</i></a></li>




  </ul>
</li> -->
@endif


@endif



<!-- /.sidebar-menu -->

</section>

<!-- /.sidebar -->

<!--  -->

</aside>


<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">


  @yield('content')


</div>
<!-- /.content-wrapper -->

@include('officio.partials.popUpModal')


<div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->



<!-- REQUIRED JS SCRIPTS -->


<script type="text/javascript">

  var APP_URL = {!! json_encode(url('/')) !!};

</script>


<!-- jQuery 2.1.4 -->



<![endif]-->


{{--<!-- Bootstrap 3.3.5 -->--}}
{{--{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.5/vue.js') }}--}}



{{--<script>--}}
{{--new Vue({--}}
  {{--el: '.box-body',--}}
  {{--data: {--}}
  {{--input: ''--}}
  {{--}--}}
  {{--})--}}
  {{--</script>--}}
  {{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}


  {{ Html::script('bootstrap/js/bootstrap.min.js') }}

  {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>--}}

  {{ Html::script('plugins/datepicker/bootstrap-datepicker.js') }}

  {{ Html::script('plugins/daterangepicker/daterangepicker.js') }}

  {{ Html::script('dist/js/sweetalert.min.js') }}


  {!! Html::script('plugins/iCheck/icheck.min.js') !!}


  {{--{!! Html::script('chart/data.js') !!}--}}




  <!-- Select2 -->

  {{ Html::script('plugins/select2/select2.full.min.js') }}

  {{ Html::script('choosen/js/chosen.jquery.min.js') }}
  {{----}}
  <!-- DataTables -->


  {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
  {{--{!! Html::script('chart/data.js') !!}--}}
  {{--{{ Html::script('chart/highcharts.js') }}--}}
  {{--{{ Html::script('chart/exporting.js') }}--}}
  {{--{{ Html::script('chart/drilldown.js') }}--}}

  <!-- AdminLTE App -->

  {{ Html::script('dist/js/app.min.js') }}


  {{ Html::script('js/location.js') }}

  {{ Html::script('js/officio.js') }}



  {{ Html::script('/plugins/timepicker/bootstrap-timepicker.min.js') }}




  <!-- page script -->


  <script>
    $(function () {
      $(".select2").select2();
    });
  </script>

  <script>

    $(document).ready(function () {

      $('#date-range').daterangepicker();

    });


  </script>

  <script>

    $(function () {

      var url = window.location;

      var element = $('ul.sidebar-menu a').filter(function () {

        return this.href == url || url.href.indexOf(this.href) == 0;

      }).addClass('active').parent().parent().addClass('in').parent();

      if (element.is('li')) {

        element.addClass('active');

      }
    });

  </script>
  <script type="text/javascript">
   $(document).ready(function(){
    $('#nepaliDate').nepaliDatePicker({
      ndpEnglishInput: 'englishDate'
    });
  });
</script>

<!-- <script>
jQuery('.nav navbar-nav li').hover(function () {
    jQuery(this).addClass('hover-enabled');
}, function () {
    jQuery(this).removeClass('hover-enabled');
});
</script> -->



</body>

</html>












