<div class="box-body">
 <div class="form-group" {{ $errors->has('subject') ? ' has-error' : '' }}>
    <label for="subject" class="col-sm-2 control-label">Subject:<span class=help-block"
        style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-10">
          {!! Form::text('subject', null , ['class'=> 'form-control', 'placeholder' => ' Subject', 'id'=>"subject"]) !!}

          @if ($errors->has('subject'))
          <span class="help-block">
            <strong> {{ $errors->first('subject') }}</strong>
        </span>
        @endif

    </div>
</div>

<div class="form-group" {{ $errors->has('nepali_date') ? ' has-error' : '' }}>
    <label for="nepali_date" class="col-sm-2 control-label">Date:<span class=help-block"
        style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-10">
           {!! Form::text('nepali_date', null , ['class'=> 'form-control', 'placeholder' => ' Date', 'id'=>'nepaliDate6']) !!}

            @if ($errors->has('nepali_date'))
            <span class="help-block">
                <strong> {{ $errors->first('nepali_date') }}</strong>
            </span>
            @endif

        </div>
    </div>


    <div class="form-group" {{ $errors->has('message') ? ' has-error' : '' }}>
    <label for="message" class="col-sm-2 control-label">Message:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-10">
           

  {!! Form::textarea('message', null , ['class'=> 'form-control ckeditor', 'placeholder' => ' Message', 'id'=>'message']) !!}
               @if ($errors->has('message'))
               <span class="help-block">
               <strong> {{ $errors->first('message') }}</strong>
            </span>
            @endif

        </div>
    </div>

   


  <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
</div>