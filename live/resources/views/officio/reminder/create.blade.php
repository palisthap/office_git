@extends('officio.main')
@section('title','add Griffith Reminder')
@section('content')
{{----}}
<script type="text/javascript" src="{{URL::to('js/nepalidatepicker/nepali.datepicker.v2.2.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::to('js/nepalidatepicker/nepali.datepicker.v2.2.min.css')}}" />

<section class="content-header">
    <h1>
        Contact information
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
   <div class="row">
    <div class="col-md-12">

        {{--@include('flash.message')--}}
        {{--@include('errors.list')--}}

        
        <div class="box box-info">
            
            <div class="box-body">
                <div class="row">
                  
                 <div class="col-md-8">
                    {{ Form::open(['url'=>'admin/reminder', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                    @include('officio.reminder.form')
                    <div class="text-right border-top">
                        <button type="submit" class="btn btn-warning">Send</button>
                    </div>
                    {{ form::close() }}
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    $('#nepaliDate6').nepaliDatePicker({
      npdMonth: true,
      npdYear: true,
      npdYearCount: 10 
  });

    
</script>
<script>
    $(document).ready(function(){
        $("p").mouseover(function(){
            $("p").css("background-color", "yellow");
        });
        
        $("#btn").click(function(){
            $("p").mouseover();
        });  
        
    });
</script>
</section>
@stop
