@extends('officio.main')


@section('content')
{{----}}
<link rel="stylesheet" href="{{URL::to('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}">
<script src="{{URL::to('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script>
<script src="{{URL::to('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>
<!-- Content Header (Pag header) -->
<section class="content-header">
  <h1>
    Reminder<a href="{{URL::to('/admin/reminder')}}"  class="btn btn-success" id="viewbtn">
    View All
  </a>
</h1>
</section>

<section class="content">
<!-- <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> {{$reminder->date}}</span>

                <h3 class="timeline-header">{{$reminder->subject}}</h3>

                <div class="timeline-body">
                 {!!$reminder->message!!}
                </div>

              </div>

    <div class="row"> {!!$reminder->message!!}
        <div class="col-md-10">

        </div>
      </div> -->
  <div class="panel panel-info">
      <div class="panel-heading"> <span class="time"><i class="fa fa-clock-o"></i> {{$reminder->date}}</span></div>
      <div class="panel-body">{{$reminder->subject}}</div>
      <div class="panel-body">{!!$reminder->message!!}</div>
  
  </div>
    </section>
   
   
 
@stop


