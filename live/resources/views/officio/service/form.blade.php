<div class="box-body">

    
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">Name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' Name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('description') ? ' has-error' : '' }}>
            <label for="description" class="col-sm-2 control-label">Description:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('description', null , ['class'=> 'form-control', 'placeholder' => ' Description', 'id'=>"description"]) !!}

                    @if ($errors->has('description'))
                    <span class="help-block">
                        <strong> {{ $errors->first('description') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

      
  <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
</div>