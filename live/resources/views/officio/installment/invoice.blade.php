
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>Editable Invoice</title> 

</head>
<style type="text/css">
	
/*
	 CSS-Tricks Example
	 by Chris Coyier
	 http://css-tricks.com
	 */

	 * { margin: 0; padding: 0; }
	 body { font: 14px/1.4 Georgia, serif; }
	 #page-wrap { width: 800px; margin: 0 auto; }

	 textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
	 table { border-collapse: collapse; }
	 table td, table th { border: 1px solid black; padding: 5px; }

	 #header { height: 15px; width: 100%; margin: 10px 0; background: #b11e24; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

	 #address { width: 250px; height: 150px; float: left;    background: transparent; }
	 #customer { overflow: hidden; }
	 #myimage{
	 	background: url('print.png');
	 }
	 #logo { 
	 	text-align: right;
	 	float: right;
	 	position: relative;
	 	left: -50px;
	 	margin-top: -20px;
	 	border: 1px solid #fff;
	 	max-width: 150px;
	 	padding: 4px;
	 	top: -90px;
	 	max-height: 183px;
	 	overflow: hidden;
	 }

	 #logoctr { display: none; }
	 #logo:hover #logoctr, #logo.edit #logoctr { display: block; text-align: right; line-height: 25px; background: #eee; padding: 0 5px; }
	 #logohelp { text-align: left; display: none; font-style: italic; padding: 10px 5px;}
	 #logohelp input { margin-bottom: 5px; }
	 .edit #logohelp { display: block; }
	 .edit #save-logo, .edit #cancel-logo { display: inline; }
	 .edit #image, #save-logo, #cancel-logo, .edit #change-logo, .edit #delete-logo { display: none; }
	 #customer-title { font-size: 20px; font-weight: bold; float: left; }

	 #meta { margin-top: 1px; width: 300px; float: right; }
	 #meta td { text-align: right;  }
	 #meta td.meta-head { text-align: left; background: #eee; }
	 #meta td textarea { width: 100%; height: 20px; text-align: right; }

	 #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
	 #items th { background: #eee; }
	 #items textarea { width: 80px; height: 50px; }
	 #items tr.item-row td { border: 0; vertical-align: top; }
	 #items td.description { width: 300px; }
	 #items td.item-name { width: 175px; }
	 #items td.description textarea, #items td.item-name textarea { width: 100%; }
	 #items td.total-line { border-right: 0; text-align: right; }
	 #items td.total-value { border-left: 0; padding: 10px; }
	 #items td.total-value textarea { height: 20px; background: none; }
	 #items td.balance { background: #eee; }
	 #items td.blank { border: 0; }

	 #terms { text-align: center; margin: 10px 0 0 0; background: #b11e24; color: white;}
	 #terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
	 #terms textarea { width: 100%; text-align: center; background: #b11e24; color: white;}

	 textarea:hover, textarea:focus, #items td.total-value textarea:hover, #items td.total-value textarea:focus, .delete:hover { background-color:#EEFF88; }

	 .delete-wpr { position: relative; }
	 .delete { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }

	 .description {
	 	float: right;
	 }
	 .price {
	 	float: right;
	 }

	</style>
	<script type="text/javascript">
		function printDiv(printme) {
			var printContents = document.getElementById('printme').innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
		}
	</script>
	<body >
		<form>

			<input type="image" class="pull-right" style="height:70px; margin-left:944px;"onclick="printDiv('printableArea')" id="myimage" style="height:200px;width:200px;" src="http://office.greencomputinguae.com/images/print.png" />

		</form>
		<div id="printme" >
			<div id="page-wrap" >


				<div id="header" ><textarea id="header"  readonly="readonly">RECEIPT </textarea></div><br>
				<div style=" float:left; width:800px; height:0px "><br>
					<h3>
						Griffith Education and Immigration Consultancy Pvt. Ltd. 
					</h3>
					<h5>G.P.O. Box No. 23792, Adwait Marg-Bagbazar<br>
						(Behind Mahendra Polic Club) Kathmandu, Nepal<br>
						Phone: +977-1-4246594, Cell: +977-9843612284<br>
						Skype: griffithmanagementconsultancy<br>
						Viber: +977-9808530033</h5>
						<div id="logo">
							<img id="image" src="http://office.greencomputinguae.com/images/logo.png" width="150px" alt="logo" />
						</div>
					</div>
					<div id="identity">

						<span id="address" readonly="readonly"> 

						</span>


					</div>


					<div style="clear:both"></div>

					<div id="customer">

						<table id="meta"><br>

							<tr>
								<td class="meta-head" readonly="readonly">Invoice #</td>
								<td><textarea readonly="readonly"> {{$dhainvoice_id}}</textarea></td>
							</tr>
							<tr>

								<td class="meta-head" >Date</td>
								<td><textarea id="date"readonly="readonly">{{$date}}</textarea></td>
							</tr>


						</table>
						<table id="meta" style="float:right;">
							@foreach($dhafees as $user)
							<tr>
								<b>{{$user->name}}</b> <br> 
								<b>Kathmandu,Nepal </b>
							</tr>
							@endforeach
						</table>
					</div>

					<table id="items">

						<tr>
							<th>Applied For</th>
							<th colspan="2">Qualification</th>
						</tr>
						@foreach($dhafees as $user)
						<tr>
							<td style="text-align:center"> {{$user->applied_for}} </td>
							<td colspan="2" style="text-align:center">{{$user->qualification}} </td>
						</tr>
						@endforeach
						<tr>
							<th>Particular</th>
							<th>Paid On</th>
							<th>Fees</th>
						</tr>


						<?php
						$sn_installment=0;
						?>
						@if(!empty($user->installmentDha ))
						<?php
						$sn=0;

						?>
						@foreach($user->installmentDha  as $installment_info)
						<tr id="hiderow">
							<td>
									{{ucfirst($convert->ToOrdinal(++$sn)).' Installment'}}
								

							</td>
							<td>{{$installment_info->nepali_date}} </td>
							<td>
							{{$installment_info->amount}}	
							</td>					</tr>
							@endforeach
							@endif
							<?php ++$sn_installment; 

							?>

							<!-- <td>{{$user->date}} </td>
							<td colspan="3"><span class="price" readonly="readonly" >{{$user->amount}} </span></td> -->
							<tr>

								<?php
								if(count($dhaFeeTotal[0]->installmentDhaTotal)>0){
									$total_installment= $dhaFeeTotal[0]->installmentDhaTotal[0]['paid_amount'];

								}
								else{
									$total_installment=0;
								}
								?>

								<td colspan="2" style="text-align:right">Total</td>
								<td class="total-value"><div id="total">{{$total_installment}}</div></td>
							</tr>
							<tr>
								<td colspan="2" style="text-align:right">Amount Due</td>
								<td class="total-value"><textarea id="paid" readonly="readonly">{{$user->first_payment-$total_installment }}</textarea></td>
							</tr>
							<tr>

								<td colspan="3" style="text-align:center">Signature & Stamp</td>
							</tr>

						</table>

						<div id="terms">
							<h3>Thank you for working with us.</h3>
							<textarea readonly="readonly">www.griffitheducation.edu.np</textarea>
						</div>

					</div></div>

				</body>

				</html>

