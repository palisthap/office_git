@extends('officio.main')


@section('content')
{{----}}
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Installment
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($installment, [
        'route' => ['installment.update', $installment->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.installment.form')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


