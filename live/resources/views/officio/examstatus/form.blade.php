<div class="box-body">

    
    <div class="form-group" {{ $errors->has('preferred_date_from') ? ' has-error' : '' }}>
        <label for="preferred_date_from" class="col-sm-2 control-label">preferred_date_from:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::date('preferred_date_from', null , ['class'=> 'form-control', 'placeholder' => ' preferred_date_from', 'id'=>"preferred_date_from"]) !!}

            @if ($errors->has('preferred_date_from'))
                <span class="help-block">
                    <strong> {{ $errors->first('preferred_date_from') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('preferred_date_to') ? ' has-error' : '' }}>
            <label for="preferred_date_to" class="col-sm-2 control-label">preferred_date_to:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('preferred_date_to', null , ['class'=> 'form-control', 'placeholder' => ' preferred_date_to', 'id'=>"preferred_date_to"]) !!}

                    @if ($errors->has('preferred_date_to'))
                    <span class="help-block">
                        <strong> {{ $errors->first('preferred_date_to') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

        <div class="form-group" {{ $errors->has('dha/mcq') ? ' has-error' : '' }}>
            <label for="dha/mcq" class="col-sm-2 control-label">dha/mcq:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('dha/mcq', null , ['class'=> 'form-control', 'placeholder' => ' dha/mcq', 'id'=>"dha/mcq"]) !!}

                    @if ($errors->has('dha/mcq'))
                    <span class="help-block">
                        <strong> {{ $errors->first('dha/mcq') }}</strong>
                    </span>
                    @endif

                </div>
            </div>


        <div class="form-group" {{ $errors->has('exam_date_booked') ? ' has-error' : '' }}>
            <label for="exam_date_booked" class="col-sm-2 control-label">exam_date_booked:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('exam_date_booked', null , ['class'=> 'form-control', 'placeholder' => ' exam_date_booked', 'id'=>"exam_date_booked"]) !!}

                    @if ($errors->has('exam_date_booked'))
                    <span class="help-block">
                        <strong> {{ $errors->first('exam_date_booked') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

        <div class="form-group" {{ $errors->has('dha_status') ? ' has-error' : '' }}>
            <label for="dha_status" class="col-sm-2 control-label">dha_status:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('dha_status', null , ['class'=> 'form-control', 'placeholder' => ' dha_status', 'id'=>"dha_status"]) !!}

                    @if ($errors->has('dha_status'))
                    <span class="help-block">
                        <strong> {{ $errors->first('dha_status') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
      
        <div class="form-group" {{ $errors->has('eligibility') ? ' has-error' : '' }}>
            <label for="eligibility" class="col-sm-2 control-label">eligibility:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('eligibility', null , ['class'=> 'form-control', 'placeholder' => ' eligibility', 'id'=>"eligibility"]) !!}

                    @if ($errors->has('eligibility'))
                    <span class="help-block">
                        <strong> {{ $errors->first('eligibility') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
  <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
</div>