@extends('officio.main')
@section('title','edit Enquiry in office')

@section('content')
{{----}}
        <!-- Content Header (Pag header) -->
<section class="content-header">
    <h1>
        Edit Enquiry in office
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($enquiryinoffice, [
        'route' => ['enquiryinoffice.update', $enquiryinoffice->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.enquiryinoffice.form')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


