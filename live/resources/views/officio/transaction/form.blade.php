<div class="box-body">

    
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">Name:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">
              {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' Name', 'id'=>"name"]) !!}

              @if ($errors->has('name'))
              <span class="help-block">
                <strong> {{ $errors->first('name') }}</strong>
            </span>
            @endif

        </div>
    </div>

    <div class="form-group" {{ $errors->has('date') ? ' has-error' : '' }}>
        <label for="date" class="col-sm-2 control-label">Date:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">
                {!! Form::date('date', null , ['class'=> 'form-control', 'placeholder' => ' Date', 'id'=>"date"]) !!}

                @if ($errors->has('date'))
                <span class="help-block">
                    <strong> {{ $errors->first('date') }}</strong>
                </span>
                @endif

            </div>
        </div>
        <div class="form-group" {{ $errors->has('amount') ? ' has-error' : '' }}>
            <label for="amount" class="col-sm-2 control-label">Amount:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                  {!! Form::text('amount', null , ['class'=> 'form-control', 'placeholder' => ' Amount', 'id'=>"amount"]) !!}

                  @if ($errors->has('amount'))
                  <span class="help-block">
                    <strong> {{ $errors->first('amount') }}</strong>
                </span>
                @endif

            </div>
        </div>
        <div class="form-group" {{ $errors->has('purpose') ? ' has-error' : '' }}>
            <label for="purpose" class="col-sm-2 control-label">Purpose:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                  {!! Form::text('purpose', null , ['class'=> 'form-control', 'placeholder' => ' Purpose', 'id'=>"purpose"]) !!}

                  @if ($errors->has('purpose'))
                  <span class="help-block">
                    <strong> {{ $errors->first('purpose') }}</strong>
                </span>
                @endif

            </div>
        </div>
  
      <div class="form-group" {{ $errors->has('particular') ? ' has-error' : '' }}>
            <label for="particular" class="col-sm-2 control-label">Particular:<span class=help-block"
              style="color: #b30000">&nbsp;* </span></label>

              <div class="col-sm-8">
         
                   {{ Form::select('particular', ['Cheaque In' => 'Cheaque In', 'Cheaque Out' => 'Cheaque Out', 'Deposit Slip In' => 'Deposit Slip In', 'Deposit Slip Out' => 'Deposit Slip Out', 'Griffith UAE In' => 'Griffith UAE In', 'Griffith UAE Out' => 'Griffith UAE Out'], null, ['class' => 'form-control','placeholder'=>'Particular']) }}
                @if ($errors->has('particular'))
                <span class="help-block">
                  <strong> {{ $errors->first('particular') }}</strong>
                </span>
                
                @endif

              </div>
            </div>
  <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
</div>