@extends('officio.main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

       <span>
        <a href="{{ url('admin/transaction/create') }}">
            <button type="button" class="btn btn-success" id="viewbtn">
                Create Griffith transaction
            </button>
        </a>
    </span>
</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                                <tr>
                                    <th>#</th>

                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Purpose</th>
                                    <th>Particular</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=1;
                                ?>
                                @foreach($transactions as $type)
                                <tr>
                                    <td>{{ $i++ }}</td>


                                    <td>{{ $type->name }}</td>
                                    <td>{{ $type->date}}</td>
                                    <td>{{ $type->amount}}</td>
                                    <td>{{ $type->purpose}}</td>
                                    <td>{{ $type->particular}}</td>




                                    <td>
                                        @if($type->status == 1)
                                        Active
                                        @else
                                        InActive
                                        @endif
                                    </td>

                                    <td>

                                        <a type="button" type="button" class="btn btn-primary btn-sm"
                                        href="{{ route('transaction.edit', array($type->id)) }}">
                                        <i class="flaticon-edit"></i>
                                    </a>

                                    <form action="{{ route('transaction.destroy', array($type->id)) }}"
                                      method="DELETE" class="delete-user-form">
                                      {!! csrf_field() !!}

                                      <button type="submit" class="btn btn-sm btn-danger">
                                        <i class="flaticon-delete-button"></i>
                                    </button>

                                </form>
                                </td>
                            </tr>
                        </tbody>

                        @endforeach

                       
                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>

@stop