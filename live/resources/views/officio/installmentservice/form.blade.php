 <input type="hidden" name="servicecharge_id" value="{{ Request::segment(3) }}" >
<div class="box-body">

	<div class="form-group" {{ $errors->has('nepali_date') ? ' has-error' : '' }}>
		<label for="nepali_date" class="col-sm-2 control-label">Date:<span class=help-block"
			style="color: #b30000">&nbsp;* </span></label>

			<div class="col-sm-8">

				{!! Form::text('nepali_date', null, ['class'=> 'form-control', 'placeholder' => ' Date', 'id'=>'nepaliDate5']) !!}

				@if ($errors->has('nepali_date'))
				<span class="help-block">
					<strong> {{ $errors->first('nepali_date') }}</strong>
				</span>
				@endif

			</div>
		</div>

		<div class="form-group" {{ $errors->has('payment_mode') ? ' has-error' : '' }}>
			<label for="payment_mode" class="col-sm-2 control-label">Payment Mode:<span class=help-block"
				style="color: #b30000">&nbsp;* </span></label>

				<div class="col-sm-8">


					{{ Form::select('payment_mode', ['cash' => 'cash', 'cheque' => 'cheque','bank'=>'bank'], null, ['class' => 'form-control']) }}
					@if ($errors->has('payment_mode'))
					<span class="help-block">
						<strong> {{ $errors->first('payment_mode') }}</strong>
					</span>

					@endif

				</div>
			</div>





			<div class="form-group" {{ $errors->has('received_by') ? ' has-error' : '' }}>
				<label for="received_by" class="col-sm-2 control-label">Received By:<span class=help-block"
					style="color: #b30000">&nbsp;* </span></label>

					<div class="col-sm-8">
						{!! Form::text('received_by', null , ['class'=> 'form-control', 'placeholder' => ' Received by', 'id'=>"received_by"]) !!}

						@if ($errors->has('received_by'))
						<span class="help-block">
							<strong> {{ $errors->first('received_by') }}</strong>
						</span>
						@endif

					</div>
				</div>

					<div class="form-group" {{ $errors->has('nepali_deposited_date') ? ' has-error' : '' }}>
						<label for="nepali_deposited_date" class="col-sm-2 control-label">Bank Deposited Date:<span class=help-block"
							style="color: #b30000">&nbsp;* </span></label>

							<div class="col-sm-8">
								{!! Form::text('nepali_deposited_date', null , ['class'=> 'form-control', 'placeholder' => ' Bank Deposited Date', 'id'=>"nepaliDate6"]) !!}

								@if ($errors->has('nepali_deposited_date'))
								<span class="help-block">
									<strong> {{ $errors->first('nepali_deposited_date') }}</strong>
								</span>
								@endif

							</div>
						</div>

							<div class="form-group" {{ $errors->has('bank_deposited') ? ' has-error' : '' }}>
							<label for="bank_deposited" class="col-sm-2 control-label">Bank Deposited By:<span class=help-block"
								style="color: #b30000">&nbsp;* </span></label>

								<div class="col-sm-8">
									{!! Form::text('bank_deposited', null , ['class'=> 'form-control', 'placeholder' => ' Bank Deposited By', 'id'=>"bank_deposited"]) !!}

									@if ($errors->has('bank_deposited'))
									<span class="help-block">
										<strong> {{ $errors->first('bank_deposited') }}</strong>
									</span>
									@endif

								</div>
							</div>
				
							<div class="form-group" {{ $errors->has('service_charge') ? ' has-error' : '' }}>
								<label for="service_charge" class="col-sm-2 control-label">Service Charge:<span class=help-block"
									style="color: #b30000">&nbsp;* </span></label>

									<div class="col-sm-8">
										{!! Form::text('service_charge', null , ['class'=> 'form-control', 'placeholder' => ' service charge', 'id'=>"service_charge"]) !!}

										@if ($errors->has('service_charge'))
										<span class="help-block">
											<strong> {{ $errors->first('service_charge') }}</strong>
										</span>
										@endif

									</div>
								</div>

								<div class="form-group">
									{{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
									<div class="col-sm-8">
										{{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
										{{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


									</div>
								</div>
							</div>
