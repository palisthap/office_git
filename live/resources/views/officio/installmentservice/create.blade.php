@extends('officio.main')


@section('content')
{{----}}
 
  <script type="text/javascript" src="{{URL::to('js/nepalidatepicker/nepali.datepicker.v2.2.min.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{URL::to('js/nepalidatepicker/nepali.datepicker.v2.2.min.css')}}" />
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Create Service Charge Installment
    </h1>
</section>

@include('officio.flash.message')

<section class="content">
    <div class="row">
        <div class="col-md-12">

            {{--@include('flash.message')--}}
            {{--@include('errors.list')--}}

            <!-- Horizontal Form -->
            <div class="box box-info">
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                      <div class="col-md-8">
                        {{ Form::open(['url'=>'admin/installmentservice', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                        @include('officio.installmentservice.form')
                        <div class="text-right border-top">
                            <button type="submit" class="btn btn-warning">Create</button>
                        </div>
                        {{ form::close() }}
                    </div>
                </div>
            </div>
        </div><!-- /.box -->


    </div><!-- /.col -->




<script>
$('#nepaliDate5').nepaliDatePicker({
  npdMonth: true,
  npdYear: true,
  npdYearCount: 10 // Options | Number of years to show
});
$('#nepaliDate6').nepaliDatePicker({
  npdMonth: true,
  npdYear: true,
  npdYearCount: 10 // Options | Number of years to show
});
</script>
</section>
@stop
