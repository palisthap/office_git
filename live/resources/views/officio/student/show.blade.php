@extends('officio.main')
@section('title','Griffith student')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

     <span>
        <a href="{{ url('admin/student/create') }}">
            <button type="button" class="btn btn-success" id="viewbtn">
                Create Griffith student
            </button>
        </a>
    </span>
</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                                <tr>
                                    <th>#</th>

                               <!--      <th>Student Name</th>
                                    <th>Phone No.</th>
                                    <th>Mobile No.</th>
                                    <th>Address</th>
                                    <th>Email Address</th>
                                    <th>Qualification</th> -->
                                    <th>Subject</th>
                                    <th>Title of qualification</th>
                                    <th>Years of experience</th>
                                    <th>Work Sector</th>
                                    <th>Language Test(if any)</th>
                                    <th>Language test score</th>
                                    <th>Service if interested</th>



                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=1;
                                ?>
                                @foreach($students as $type)
                                <tr>
                                    <td>{{ $i++ }}</td>
<!-- 

                                    <td>{{ $type->name }}</td>
                                    <td>{{ $type->ph_no}}</td>
                                    <td>{{ $type->mb_no}}</td>
                                    <td>{{ $type->address}}</td>
                                    <td>{{ $type->email}}</td>
                                    <td>{{ $type->qualification}}</td> -->
                                    <td>{{ $type->subject}}</td>
                                    <td>{{ $type->title}}</td>
                                    <td>{{ $type->experience}}</td>
                                    <td>{{ $type->work_sector}}</td>
                                    <td>{{ $type->language_test}}</td>
                                    <td>{{ $type->test_score}}</td>
                                    <td>{{ $type->service}}</td>




                                    


                                </tr>
                            </tbody>

                            @endforeach


                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>

@stop