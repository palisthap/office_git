<div class="box-body">

    
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">Student Name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' Name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('ph_no') ? ' has-error' : '' }}>
            <label for="ph_no" class="col-sm-2 control-label">Phone No:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('ph_no', null , ['class'=> 'form-control', 'placeholder' => ' Phone No', 'id'=>"ph_no"]) !!}

                    @if ($errors->has('ph_no'))
                    <span class="help-block">
                        <strong> {{ $errors->first('ph_no') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
  <div class="form-group" {{ $errors->has('mb_no') ? ' has-error' : '' }}>
            <label for="mb_no" class="col-sm-2 control-label">Mobile No:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('mb_no', null , ['class'=> 'form-control', 'placeholder' => 'Mobile no', 'id'=>"mb_no"]) !!}

                    @if ($errors->has('mb_no'))
                    <span class="help-block">
                        <strong> {{ $errors->first('mb_no') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
       <div class="form-group" {{ $errors->has('address') ? ' has-error' : '' }}>
        <label for="address" class="col-sm-2 control-label">Address:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('address', null , ['class'=> 'form-control', 'placeholder' => ' Address', 'id'=>"address"]) !!}

            @if ($errors->has('address'))
                <span class="help-block">
                    <strong> {{ $errors->first('address') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
        <label for="email" class="col-sm-2 control-label">Email:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('email', null , ['class'=> 'form-control', 'placeholder' => ' Email', 'id'=>"email"]) !!}

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong> {{ $errors->first('email') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('qualification') ? ' has-error' : '' }}>
        <label for="qualification" class="col-sm-2 qualification-label">Qualification:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">

                       {{ Form::select('qualification', ['School Leaving Certificate' => 'School Leaving Certificate', '+2/A-level' => '+2/A-level', 'Diploma' => 'Diploma', 'Bachelors' => 'Bachelors', 'Master' => 'Master'], null, ['class' => 'form-control', 'placeholder'=>'Qualification']) }}                  

            @if ($errors->has('qualification'))
                <span class="help-block">
                    <strong> {{ $errors->first('qualification') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('subject') ? ' has-error' : '' }}>
        <label for="subject" class="col-sm-2 control-label">Subject:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
       

                                         {{ Form::select('subject', ['Science' => 'Science', 'Commerce' => 'Commerce', 'Art' => 'Art', 'Education' => 'Education'], null, ['class' => 'form-control','placeholder'=>'Subject']) }} 

            @if ($errors->has('subject'))
                <span class="help-block">
                    <strong> {{ $errors->first('subject') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('title') ? ' has-error' : '' }}>
        <label for="title" class="col-sm-2 control-label">Title of qualification:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('title', null , ['class'=> 'form-control', 'placeholder' => ' Title', 'id'=>"title"]) !!}

            @if ($errors->has('title'))
                <span class="help-block">
                    <strong> {{ $errors->first('title') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('experience') ? ' has-error' : '' }}>
        <label for="experience" class="col-sm-2 control-label">Years of experience:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">

                                       {{ Form::select('experience', ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10 or more' => '10 or more'], null, ['class' => 'form-control','placeholder'=>'Experience']) }}
            @if ($errors->has('experience'))
                <span class="help-block">
                    <strong> {{ $errors->first('experience') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('work_sector') ? ' has-error' : '' }}>
        <label for="work_sector" class="col-sm-2 control-label">Work sector:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('work_sector', null , ['class'=> 'form-control', 'placeholder' => ' Work Sector', 'id'=>"work_sector"]) !!}

            @if ($errors->has('work_sector'))
                <span class="help-block">
                    <strong> {{ $errors->first('work_sector') }}</strong>
                </span>
            @endif

        </div>
    </div>
     <div class="form-group" {{ $errors->has('language_test') ? ' has-error' : '' }}>
        <label for="language_test" class="col-sm-2 control-label">language test:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
     
                                       {{ Form::select('language_test', ['CAE' => 'CAE', 'IELTS' => 'IELTS', 'TOEFL' => 'TOEFL', 'PTE' => 'PTE', 'SAT' => 'SAT', 'GRE' => 'GRE', 'GMAT' => 'GMAT'], null, ['class' => 'form-control','placeholder'=>'Language Test']) }}

            @if ($errors->has('language_test'))
                <span class="help-block">
                    <strong> {{ $errors->first('language_test') }}</strong>
                </span>
            @endif

        </div>
    </div>
 
    <div class="form-group" {{ $errors->has('test_score') ? ' has-error' : '' }}>
        <label for="test_score" class="col-sm-2 control-label">Language test score:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('test_score', null , ['class'=> 'form-control', 'placeholder' => ' Test Score', 'id'=>"test_score"]) !!}

            @if ($errors->has('test_score'))
                <span class="help-block">
                    <strong> {{ $errors->first('test_score') }}</strong>
                </span>
            @endif

        </div>
    </div>

     <div class="form-group" {{ $errors->has('service') ? ' has-error' : '' }}>
        <label for="service" class="col-sm-2 control-label">Language test score:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
       {{ Form::select('service', ['Study Abroad' => 'Study Abroad', 'Visa' => 'Visa', 'Passport' => 'Passport', 'Permanent Ressidency(PR)-Canada' => 'Permanent Ressidency(PR)-Canada', 'English Translation' => 'English Translation', 'Taxi Service' => 'Taxi Service'], null, ['class' => 'form-control','placeholder'=>'Service']) }}
     

            @if ($errors->has('service'))
                <span class="help-block">
                    <strong> {{ $errors->first('service') }}</strong>
                </span>
            @endif

        </div>
    </div>
  
  
 <!--    <div class="form-group" {{ $errors->has('service') ? ' has-error' : '' }}>
        <label for="service" class="col-sm-2 control-label">Service if any:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
   
   {{ Form::select('service', ['Study Abroad' => 'Study Abroad', 'Visa' => 'Visa', 'Passport' => 'Passport', 'Permanent Ressidency(PR)-Canada' => 'Permanent Ressidency(PR)-Canada', 'English Translation' => 'English Translation', 'Taxi Service' => 'Taxi Service','placeholder'=>'Service']) }}
            @if ($errors->has('service'))
                <span class="help-block">
                    <strong> {{ $errors->first('service') }}</strong>
                </span>
            @endif

        </div>
    </div> -->
    
  <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
</div>
