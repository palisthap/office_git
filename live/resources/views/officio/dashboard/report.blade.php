@extends('officio.main')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Show Records
	</h1>

</section>

<section class="col-lg-12 connectedSortable">

  <script type="text/javascript">
    $(document).ready(function() {
      $('#example').DataTable();
    } );
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#example2').DataTable();
    } );
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#example3').DataTable();
    } );

  </script>

  <script type="text/javascript">

    $('.sidebar-menu li').click(function() {

      $(this).children('ul').stop(true, false, true).slideToggle(800); 

      $(this).children('li').addClass('active'); 

    });
    $('.treeview-menu a').click(function() {

      $(this).children('li').addClass("active"); 

    });



//     $(document).ready(function () {
//      $(".sidebar-menu li ul").hide();
//     $(".sidebar-menu li").click(function () {

//         $(this).children(".treeview-menu").toggle();

//     });
// });
$(document).ready(function(){

  //Check to see if the window is top if not then display button
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
      $('.scrollToTop').fadeIn();
    } else {
      $('.scrollToTop').fadeOut();
    }
  });
  
  //Click event to scroll to top
  $('.scrollToTop').click(function(){
    $('html, body').animate({scrollTop : 0},1000);
    return false;
  });
  
});
</script> 


<script>
 $(document).ready(function(){
  $('#date').englishDatePicker();
});
</script>
<script type="text/javascript" src="http://office.greencomputinguae.com/js/jQuery.min.js"></script>
<script type="text/javascript" src="http://office.greencomputinguae.com/js/nepali.datepicker.v2.min.js"></script>
<script type="text/javascript" src="http://office.greencomputinguae.com/js/jquery.validate.min.js"></script>
<script type="text/javascript">
 $(document).ready(function(){
  $('#nepaliDate').nepaliDatePicker({
    ndpEnglishInput: 'englishDate'
  });
});
</script>
<style type="text/css">
  .agriculture-field{
   padding: 10px;
   color: #DF1F26;
   font-size: 18px;
   display: inline;
   width: 23%;
   background: rgba(51, 122, 183, 0);
   border: 2px solid rgba(10, 10, 10, 0.3);
   border-radius: 10px;
   margin: 0 0 10px 10px;
 }
 .greenp
 {
   color: green !important;
 }

</style>

  <div class="col-md-12 margin30bot">
    <div class="box box-info">
      <!-- form start -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <table class="table ">

              <thead>
                <tr class="page-header "><th>Total</th></tr>
                <tr class="alert ">
                  <th> Total Cash In</th> 
                  <th>

                    {{$data_cashin[0]->service_charge}}</th>

                  </tr>
                  <tr class="alert ">
                    <th> Total Cash out</th> 
                    <th>

                      {{$data_cashouts[0]->amount}}</th>

                    </tr>
                    <tr class="alert ">
                      <th>Total Service Charge</th> 
                      <th>

                   

                      </tr>


                      <tr class="alert">
                        <th> Total DHA Fees</th> 
                        <th>

                       

                        </tr>

                        <tr class="alert ">
                          <th>Total Income</th> 
                          <th>

                            {{$data_cashin[0]->service_charge}}</th>

                          </tr>
                          <tr class="alert alert">
                            <th>Net Profit/Loss</th> 
                            <th>

                              Total Loss= {{abs($data_cashin[0]->service_charge-$data_cashouts[0]->amount)}}</th>

                            </tr>

                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                        <table class="table table-hover">
                          <thead>
                            <tr class="alert alert-success">
                              <th>Cash In Records</th>

                            </tr>
                          </thead>
                          <tbody>

                           <tr>
                            <th>Total Cash In from Basic Life Support<span class="pull-right">{{$Basic_Life_Support[0]->service_charge}}</span></th>
                          </tr>


                          <tr>
                            <th>Total Cash In from DHA/HAAD MCQ <span class="pull-right"> 
                             {{$DHA_MCQ[0]->service_charge}}
                            </span></th>    
                          </tr>
                        </tbody>
                      </table>



                    </div>
                    <div class="col-md-6">
                     <table class="table table-hover ">
                      <thead>
                        <tr class="alert alert-danger">
                          <th>Cash Out Records</th>

                        </tr>
                      </thead>
                      <tbody>
                      @foreach($cashout_expenses as $cashout)
                      <tr>
                         <th>Total Cash Out In {{$cashout->expenses_type}} &amp; Garbage <span class="pull-right">{{$cashout->amount}}</span></th>
                       </tr>
                      @endforeach
                      
                          </tbody>
                        </table>
                      </div>

                    </div>
                  </div>
                </div>


              </div>

          <!-- BOOTSTRAP SCRIPTS  -->
          <script src="http://office.greencomputinguae.com/assets/js/bootstrap.min.js"></script>

          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>  
          <style type="text/css">


            .changed-color
            {
             background-color: white!important;
           }


         </style>
         
  <!--          <div class="col-sm-12 col-md-12 changed-color">
            <div class="alert-message alert-message-success">
              <h4>
                Todays Reminder By You</h4>  <br>
                <div class="update-nag">
                  <div class="update-split update-success"><i class="glyphicon glyphicon-leaf"></i></div>

                </div>  

              </div>
            </div> -->

          </section>
          @stop

