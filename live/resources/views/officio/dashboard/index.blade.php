@extends('officio.main')

@section('content')
<script type="text/javascript" src="{{URL::to('js/nepalidatepicker/nepali.datepicker.v2.2.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::to('js/nepalidatepicker/nepali.datepicker.v2.2.min.css')}}" />
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
     Show Records
 </h1>

</section>
<div>
<div>

    {{ Form::open(['url'=>'admin/nepalicalender', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}          

    <div class="col-sm-6">             
        <select name="month" class="form-control">
            <option value="0
            ">Select Month
        </option>
        <option value="1
        ">Baisakh
    </option>
    <option value="2
    ">Jestha
</option><option value="3
">Ashad
</option><option value="4
">Shrawan
</option><option value="5
">Bhadra
</option><option value="6
">Ashoj
</option><option value="7
">Kartik
</option><option value="8
">Mangsir
</option><option value="9
">Poush
</option><option value="10
">Magh
</option><option value="11
">Falgun
</option><option value="12
">Chaitra
</option>


</select>
<button type="submit" class="btn btn-primary"> Show Report</button>


</div>
{{form::close()}}
</div>
  <div>    
    {{ Form::open(['url'=>'admin/report', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}          

    <div class="col-sm-6">             
        <select name="month" class="form-control">
            <option value="0
            ">Select Month
        </option>
        <option value="1
        ">Jan
    </option>
    <option value="2
    ">Feb
</option><option value="3
">Mar
</option><option value="4
">Apr
</option><option value="5
">May
</option><option value="6
">Jun
</option><option value="7
">Jul
</option><option value="8
">Aug
</option><option value="9
">Sept
</option><option value="10
">Oct
</option><option value="11
">Nov
</option><option value="12
">Dec
</option>


</select>
<button type="submit" class="btn btn-primary"> Show Report</button>


</div>
{{form::close()}}
</div>

</div>
<div class="col-sm-6">  
    <br></br>
    {{ Form::open(['url'=>'admin/nepali', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }} 



    {!! Form::text('date', null , ['class'=> 'form-control', 'placeholder' => ' Pick up nepali date', 'id'=>'nepaliDate5']) !!}

    <button type="submit" class="btn btn-primary"> Show Report</button>

    {{form::close()}}
</div>
<div class="col-sm-6">  
    <br></br>
    {{ Form::open(['url'=>'admin/englishDate', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }} 



    {!! Form::date('engdate', null , ['class'=> 'form-control', 'placeholder' => ' date']) !!}

    <button type="submit" class="btn btn-primary"> Show Report</button>

    {{form::close()}}
</div>
<br>
</br>
<div class="col-sm-6">  
    <br></br>
    {{ Form::open(['url'=>'admin/nepalidate2', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }} 



    {!! Form::text('date1', null , ['class'=> 'form-control', 'placeholder' => 'Pick up nepali date', 'id'=>'nepaliDate6']) !!}
    {!! Form::text('date2', null , ['class'=> 'form-control', 'placeholder' => 'Pick up nepali  date', 'id'=>'nepaliDate7']) !!}
    <button type="submit" class="btn btn-primary"> Show Report</button>

    {{form::close()}}
</div>
<div class="col-sm-6">  
    <br></br>
    {{ Form::open(['url'=>'admin/englishdate2', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }} 



    {!! Form::date('date1', null , ['class'=> 'form-control', 'placeholder' => ' date']) !!}
    {!! Form::date('date2', null , ['class'=> 'form-control', 'placeholder' => ' date']) !!}
    <button type="submit" class="btn btn-primary"> Show Report</button>

    {{form::close()}}
</div>
<script>
    $('#nepaliDate5').nepaliDatePicker({
      npdMonth: true,
      npdYear: true,
  npdYearCount: 10 // Options | Number of years to show
});

    $('#nepaliDate6').nepaliDatePicker({
      npdMonth: true,
      npdYear: true,
  npdYearCount: 10 // Options | Number of years to show
});
    $('#nepaliDate7').nepaliDatePicker({
      npdMonth: true,
      npdYear: true,
  npdYearCount: 10 // Options | Number of years to show
});
</script>
@stop

