@extends('officio.main')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Green Computing Nepal
        <small>Admin Panel</small>
    </h1>
  <!--  -->
    <div class="row">
         <div class="form-group" {{ $errors->has('dha_id') ? ' has-error' : '' }}>
                            <label for="dha_id" class="col-sm-2 control-label">Name:<span class=help-block"
                                style="color: #b30000">&nbsp;* </span></label>

                                <div class="col-sm-8">
                                 <!--  {!! Form::select('dha_id' ,['c'=>'cash','ch'=>'cheque','b'=>'bank'],null , ['class'=> 'form-control', 'placeholder' => 'dha_id', 'id'=>"dha_id"]) !!} -->
     {{ Form::select('dha_id',$dhafees,null,['class'=>'form-control col-md-7 col-xs-12','placeholder'=>'--Select--']) }}
                                  @if ($errors->has('dha_id'))
                                  <span class="help-block">
                                    <strong> {{ $errors->first('dha_id') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
    
   </div>


</section>

<!-- Main content -->
<section class="content">






</section>


@stop