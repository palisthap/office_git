@extends('officio.main')
@section('title','add Cash Out')

@section('content')
{{----}}
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Create New Cash Out
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
   <div class="row">
    <div class="col-md-12">

        {{--@include('flash.message')--}}
        {{--@include('errors.list')--}}

        <!-- Horizontal Form -->
        <div class="box box-info">
            <!-- form start -->
            <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    {{ Form::open(['url'=>'admin/cash_out', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
               
                    <div class="text-right border-top">
                        <button type="submit" class="btn btn-warning">Create</button>
                    </div>
                    {{ form::close() }}
                </div>
            </div>
        </div>
    </div><!-- /.box -->


</div>
</section>
@stop
