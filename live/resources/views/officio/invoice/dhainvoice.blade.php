@extends('officio.main')
@section('title','Cash Out')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

      
               View DHA Invoice
       
</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=1;
                                $dhainvoice = \App\DhaInvoice::with('dhafees')->get();
                              
                                ?>
                               
                                @foreach($dhainvoice  as $type)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $type->dhafees()->first()->name}}</td>
                                  <td>{{ $type->date}}</td>

                              
                        </tr>
                        </tbody>
                        @endforeach



                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>

@stop