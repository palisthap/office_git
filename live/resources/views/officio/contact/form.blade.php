<div class="box-body">
                     <div class="form-group" {{ $errors->has('subject') ? ' has-error' : '' }}>
                        <label for="subject" class="col-sm-2 control-label">Subject:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-10">
                              {!! Form::text('subject', null , ['class'=> 'form-control', 'placeholder' => ' subject', 'id'=>"subject"]) !!}

                              @if ($errors->has('subject'))
                              <span class="help-block">
                                <strong> {{ $errors->first('subject') }}</strong>
                            </span>
                            @endif

                        </div>
                    </div>




                    <div class="form-group" {{ $errors->has('message') ? ' has-error' : '' }}>
                        <label for="message" class="col-sm-2 control-label">message:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-10">
                               <textarea class="form-control ckeditor" name="message"  id="message" >
                               </textarea>

                               @if ($errors->has('message'))
                               <span class="help-block">
                                   <strong> {{ $errors->first('message') }}</strong>
                               </span>
                               @endif

                           </div>
                       </div>
                       </div>

                       



                