@extends('officio.main')

@section('content')
        <!-- Content Header (Page header) -->
        <script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>
<section class="content-header">
    <h1>
        Customers List
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                            <tr>
                                <th> Name</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Phone</th>
                               
                                <th>Status</th>
                              <!--   <th>Actions</th> -->
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->username}}</td>
                                    <td>
                                        <?php
                                          $roleId = $user->user_role_id;
                                            $role = \App\UserRole::where('id',$roleId)->first();
                                        ?>
                                        {{ $role->role_type }}
                                    </td>
                                 <td>{{ $user->phone }}</td>
                                    <td>
                                        @if($user->status == 1)
                                            Active
                                        @else
                                            InActive
                                        @endif
                                    </td>

                                 <!--    <td>

                                        <button type="button" class="btn btn-primary btn-sm edit-user-form"
                                                data-url="{{ route('user.edit', array($user->id)) }}">
                                            <i class="flaticon-edit"></i>
                                        </button>

                                        <form action="{{ route('user.destroy', array($user->id)) }}"
                                              method="DELETE" class="delete-user-form">
                                            {!! csrf_field() !!}

                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i class="flaticon-delete-button"></i>
                                            </button>
                                        </form>
                                    </td> -->
                                </tr>
                            @endforeach


                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
</section><!-- /.content -->



@stop