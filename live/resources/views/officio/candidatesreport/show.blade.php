@extends('officio.main')
@section('title','Candidates Report')
@section('content')
<!-- Content Header (Page header) -->
<div class="row">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h1>Candidates Report</h1>
	</div>
	<div class="panel-body">
<div class="row">
  <div class="form-group">
{{Form::label('name','Name:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('name',$candidates->name,['class'=>'form-control','placeholder'=>'Enter Name','disabled'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('profession','Profession:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('profession',$candidates->profession,['class'=>'form-control','placeholder'=>'Enter Profession','disabled'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('Email','Email:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::email('email',$candidates->email,['class'=>'form-control','placeholder'=>'Enter Email','disabled'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('contact_no','Contact_No:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::number('contact_no',$candidates->contact_no,['class'=>'form-control','placeholder'=>'Enter Contact No','disabled'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('DOB','Date Of Birth:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::date('DOB',$candidates->DOB,['class'=>'form-control','placeholder'=>'Enter Date Of Birth','disabled'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('passport_no','Passport No:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::number('passport_no',$candidates->passport_no,['class'=>'form-control','placeholder'=>'Enter Passport No','disabled'])}}
    </div>
  </div>
  </div>
<div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('service_charge','Service Charge:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('service_charge',$candidates->service_charge,['class'=>'form-control','placeholder'=>'Enter Service Charge','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('books','Books:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-10">
     {{Form::text('books',$candidates->books,['class'=>'form-control','placeholder'=>'Enter Books','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('bls','BLS:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('bls',$candidates->bls,['class'=>'form-control','placeholder'=>'Enter BLS','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('bls_date','BLS Date:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-10">
     {{Form::date('bls_date',$candidates->bls_date,['class'=>'form-control','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
</div>
 <div class="row">
  <div class="form-group">
{{Form::label('GSC_issued_date','Good Standing Certificate Issue Date:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::date('GSC_issued_date',$candidates->GSC_issued_date,['class'=>'form-control','placeholder'=>'Enter Good Standing Certificate Issue Date','style'=>'margin-top:30px;','disabled'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('equivalent_certificate','Equivalent Certificate:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('equivalent_certificate',$candidates->equivalent_certificate,['class'=>'form-control','placeholder'=>'Enter Good Standing Certificate Equivalent Certificate','disabled'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHAMCQ_charge','DHAMCQ Charge:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('DHAMCQ_charge',$candidates->DHAMCQ_charge,['class'=>'form-control','placeholder'=>'Enter DHAMCQ Charge','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHAMCQ_provided','DHAMCQ Provided:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-10">
     {{Form::text('DHAMCQ_provided',$candidates->DHAMCQ_provided,['class'=>'form-control','placeholder'=>'DHAMCQ Provided','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHAMCQ_performance','DHAMCQ Performance:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('DHAMCQ_performance',$candidates->DHAMCQ_performance,['class'=>'form-control','placeholder'=>'Enter DHAMCQ Performance','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('preffered_exam_date','Prefered Exam Date:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-10">
     {{Form::date('preffered_exam_date',$candidates->preffered_exam_date,['class'=>'form-control','placeholder'=>'Enter Prefered Exam Date','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
{{Form::label('email_account','Email Account:',['class'=>'col-sm-1 control-label','style'=>'margin-top:20px;'])}}
    <div class="col-sm-9">
     {{Form::email('email_account',$candidates->email_account,['class'=>'form-control','placeholder'=>'Enter Email Account','style'=>'margin-top:30px;','disabled'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHA_account','DHA Account:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('DHA_account',$candidates->DHA_account,['class'=>'form-control','placeholder'=>'Enter DHA Account','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHA_username','DHA Username:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;margin-top:-5px;'])}}
    <div class="col-sm-10">
     {{Form::text('DHA_username',$candidates->DHA_username,['class'=>'form-control','placeholder'=>'Enter DHA Username','style'=>'margin-left:3px;','disabled'])}}
    </div>
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
{{Form::label('DHA_ref_no','DHA Ref No:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('DHA_ref_no',$candidates->DHA_ref_no,['class'=>'form-control','placeholder'=>'Enter DHA Ref No','disabled'])}}
    </div>
  </div>
  </div>

  <div class="pull-right">
<button onclick="myFunction()">Print this page</button>
  </div>
</div>



@endsection
<script>
function myFunction() {
    window.print();
}
</script>