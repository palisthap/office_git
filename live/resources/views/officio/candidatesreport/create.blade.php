@extends('officio.main')
@section('title','add Candidates Report')
@section('content')
{{----}}
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Create New Candidates Report
    </h1>
</section>
@include('officio.flash.message')
    
 <section class="content">
    <div class="container">
                        @include('officio.candidatesreport.form')
                        </div>
                       
@stop
