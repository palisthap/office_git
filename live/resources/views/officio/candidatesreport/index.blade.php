@extends('officio.main')
@section('title','Candidates Report')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

       <span>
        <a href="{{ url('admin/candidates_report/create') }}">
            <button type="button" class="btn btn-success" id="viewbtn">
                Create Candidates Flow
            </button>
        </a>
    </span>
</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                                <tr>
                                    <th>#</th>
                                 <!--    <th>Date</th> -->
                                    <th>Name</th>
                                  
                                    <th>Email</th>
                                    <th>contact</th>
                                    <th>DOB</th>
                                    <th>passport_no</th>


                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=1;
                                ?>
                                @foreach($candidates as $type)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                 
                                   <!--  <td>{{ $type->date }}</td> -->
                                    <td>{{ $type->name }}</td>
                                    <td>{{ $type->email}}</td>
                                    <td>{{ $type->contact_no }}</td>
                                    <td>{{ $type->DOB}}</td>
                                    <td>{{ $type->passport_no }}</td>



                                    <td>
                                        @if($type->status == 1)
                                        Active
                                        @else
                                        InActive
                                        @endif
                                    </td>

                                    <td>

                                        <a type="button" type="button" class="btn btn-primary btn-sm"
                                        href="{{ route('candidates_report.edit', array($type->id)) }}">
                                        <i class="flaticon-edit"></i>
                                    </a>

                                    <form action="{{ route('candidates_report.destroy', array($type->id)) }}"
                                      method="DELETE" class="delete-user-form">
                                      {!! csrf_field() !!}

                                      <button type="submit" class="btn btn-sm btn-danger">
                                        <i class="flaticon-delete-button"></i>
                                    </button>

                                    <a href="{{route('candidates_report.show',$type->id)}}" class="btn btn-success btn-sm btn-block"><i class="fa fa-eye"></i></a>

                                </form>
                            </td>
                        </tr>
                        @endforeach

</tbody>
                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->
</section><!-- /.content -->



@stop