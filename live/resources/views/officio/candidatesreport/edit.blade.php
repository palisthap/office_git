@extends('officio.main')
@section('title','edit Candidates Report')

@section('content')
{{----}}
        <!-- Content Header (Pag header) -->
<section class="content-header">
    <h1>
        Edit Candidates Report
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    <section class="content">
    <div class="container">
                        @include('officio.candidatesreport.edit_form')
                        </div>
                       
@stop




