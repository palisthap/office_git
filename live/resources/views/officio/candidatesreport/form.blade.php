<style type="text/css">
  #checkbox1{
    margin-top: 20px;
    margin-left: 105px;
  }
  #checkbox2{
    margin-top: 20px;
    margin-left: 105px;
  }
</style>

{!!Form::open(['url'=>'admin/candidates_report','class'=>'form-horizontal'])!!}
<div class="row">
  <div class="form-group">
{{Form::label('name','Name:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Name'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('profession','Profession:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('profession',null,['class'=>'form-control','placeholder'=>'Enter Profession'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('Email','Email:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::email('email',null,['class'=>'form-control','placeholder'=>'Enter Email'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('contact_no','Contact_No:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('contact_no',null,['class'=>'form-control','placeholder'=>'Enter Contact No'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('DOB','Date Of Birth:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::date('DOB',null,['class'=>'form-control','placeholder'=>'Enter Date Of Birth'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('passport_no','Passport No:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('passport_no',null,['class'=>'form-control','placeholder'=>'Enter Passport No'])}}
    </div>
  </div>
  </div>
<div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('service_charge','Service Charge:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('service_charge',null,['class'=>'form-control','placeholder'=>'Enter Service Charge','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('books','Books:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-10">
     {{Form::text('books',null,['class'=>'form-control','placeholder'=>'Enter Books','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('bls','BLS:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::date('bls',null,['class'=>'form-control','placeholder'=>'Enter BLS','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('bls_date','BLS Date:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-10">
     {{Form::date('bls_date',null,['class'=>'form-control','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
</div>
 <div class="row">
  <div class="form-group">
{{Form::label('GSC_issued_date','Good Standing Certificate Issue Date:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::date('GSC_issued_date',null,['class'=>'form-control','placeholder'=>'Enter Good Standing Certificate Issue Date','style'=>'margin-top:30px;'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('equivalent_certificate','Equivalent Certificate:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('equivalent_certificate',null,['class'=>'form-control','placeholder'=>'Enter Good Standing Certificate Equivalent Certificate'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHAMCQ_charge','DHAMCQ Charge:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('DHAMCQ_charge',null,['class'=>'form-control','placeholder'=>'Enter DHAMCQ Charge','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHAMCQ_provided','DHAMCQ Provided:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-10">
     {{Form::text('DHAMCQ_provided',null,['class'=>'form-control','placeholder'=>'DHAMCQ Provided','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHAMCQ_performance','DHAMCQ Performance:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('DHAMCQ_performance',null,['class'=>'form-control','placeholder'=>'Enter DHAMCQ Performance','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('preffered_exam_date','Prefered Exam Date:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-10">
     {{Form::date('preffered_exam_date',null,['class'=>'form-control','placeholder'=>'Enter Prefered Exam Date','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
{{Form::label('email_account','Email Account:',['class'=>'col-sm-1 control-label','style'=>'margin-top:20px;'])}}
    <div class="col-sm-9">
     {{Form::email('email_account',null,['class'=>'form-control','placeholder'=>'Enter Email Account','style'=>'margin-top:30px;'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHA_account','DHA Account:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
    <div class="col-sm-8">
     {{Form::text('DHA_account',null,['class'=>'form-control','placeholder'=>'Enter DHA Account','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('DHA_username','DHA Username:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;margin-top:-5px;'])}}
    <div class="col-sm-10">
     {{Form::text('DHA_username',null,['class'=>'form-control','placeholder'=>'Enter DHA Username','style'=>'margin-left:3px;'])}}
    </div>
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
{{Form::label('DHA_ref_no','DHA Ref No:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('DHA_ref_no',null,['class'=>'form-control','placeholder'=>'Enter DHA Ref No'])}}
    </div>
  </div>
  </div>
<div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('dha_fees_first_installment','DHA Fees: 1st installment:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
<div id="checkbox1">
  {{Form::radio('installment1',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('installment1',"no")}}{{Form::label('no','No')}}
   
    
  </div>
    
 
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('dha_fees_second_installment','DHA Fees: 2nd installment:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
<div id="checkbox2">
  {{Form::radio('installment2',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('installment2',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
{{Form::label('dha_fee_paid_date','DHA Fees:Paid Date:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::date('dha_fee_paid_date',null,['class'=>'form-control','placeholder'=>'Enter DHA Fees:Paid Date','style'=>'margin-top:20px;'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('document_upload','Document Upload:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
<div id="checkbox1">
  {{Form::radio('document_upload',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('document_upload',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('payment','Payment1:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;margin-top:15px;'])}}
<div id="checkbox2">
  {{Form::radio('payment1',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('payment1',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
</div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('approval_for_psc_verification','Approval For PSV Verification:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
<div id="checkbox1">
  {{Form::radio('approval_for_psv_verification',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('approval_for_psv_verification',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('payment','Payment:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;margin-top:15px;'])}}
<div id="checkbox2">
  {{Form::radio('payment2',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('payment2',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('dataflow_initiated','Dataflow Initiated:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
<div id="checkbox1" >
  {{Form::radio('dataflow_initiated',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('dataflow_initiated',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('barcode','Barcode:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;margin-top:15px;'])}}
<div class="col-sm-10">
{{Form::text('barcode',null,['class'=>'form-control','placeholder'=>'Enter Barcode','style'=>'margin-left:3px; margin-top:15px;'])}}
 </div>
  </div>
  </div>
</div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('exam_eligibility_id_generated','Exam Eligibility ID:Generated:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
<div id="checkbox1">
  {{Form::radio('exam_eligibility_id',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('exam_eligibility_id',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('activated','Activated:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;margin-top:15px;'])}}
<div id="checkbox2">
  {{Form::radio('activited',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('activited',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
{{Form::label('DHAMCQ_performance_revision','DHAMCQ Performance Revision:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::text('DHAMCQ_performance_revision',null,['class'=>'form-control','placeholder'=>' DHAMCQ Performance Revision','style'=>'margin-top:20px;'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group">
{{Form::label('selection_of_DHA_exam_date','Selection Of DHA Exam Date:',['class'=>'col-sm-1 control-label'])}}
    <div class="col-sm-9">
     {{Form::date('selection_of_DHA_exam_date',null,['class'=>'form-control','style'=>'margin-top:20px;'])}}
    </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('send_confirmation_to_candidate:email','Send Confirmation To Candidate:Email:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
<div id="checkbox1">
  {{Form::radio('send_confirmation_to_candidate_email',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('send_confirmation_to_candidate_email',"no")}}{{Form::label('no','No')}}

    
  </div>
    
 
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('sms','SMS:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;margin-top:15px;'])}}
<div id="checkbox2">
  {{Form::radio('sms',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('sms',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
{{Form::label('exam_result','Exam Result:',['class'=>'col-sm-1 control-label'])}}
<div id="radio1">
    <div class="col-sm-9">
   <div class="col-sm-4">{{Form::radio('exam_result',"passed","passed")}}{{Form::label('passed','Passed')}}</div>
  <div class="col-sm-4">{{Form::radio('exam_result',"failed")}}{{Form::label('failed','Failed')}}</div>
  
    </div>
    </div>
  </div>
  </div>

<div class="row">
  <div class="form-group">
{{Form::label('dataflow_report','Dataflow report:',['class'=>'col-sm-1 control-label'])}}
<div id="radio1">
    <div class="col-sm-9">
   <div class="col-sm-4">{{Form::radio('dataflow_report',"completed","completed")}}{{Form::label('completed','Completed')}}</div>
  <div class="col-sm-4">{{Form::radio('dataflow_report',"pending")}}{{Form::label('pending','Pending')}}</div>
  <div class="col-sm-4">{{Form::radio('dataflow_report',"more docs req")}}{{Form::label('more_docs_req','More Docs Req')}}</div>
    </div>
    </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('dha_eligibility','DHA Eligibility:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;'])}}
<div id="radio1">
  {{Form::radio('DHA_eligibility',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('DHA_eligibility',"no")}}{{Form::label('no','No')}}
    
  </div>
    
 
  </div>
  </div>
  <div class="col-sm-5 ">
    <div class="form-group">
{{Form::label('updated_cv','Updated_CV:',['class'=>'col-sm-2 control-label','style'=>'padding-left:0px;margin-top:15px;'])}}
<div id="radio2">
  {{Form::radio('updated_cv',"yes","yes")}}{{Form::label('yes','Yes')}}
  {{Form::radio('updated_cv',"no")}}{{Form::label('no','No')}}
<!--    {{Form::checkbox('updated_cv',"yes")}}{{Form::label('yes','Yes')}}
  {{Form::checkbox('updated_cv',"no")}}{{Form::label('no','No')}} -->
  
  </div>
    
 
  </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
{{Form::label('remarks','Remarks:',['class'=>'col-sm-1 control-label','style'=>'margin-top:50px;'])}}
    <div class="col-sm-9">
     {{Form::textarea('remarks',null,['class'=>'form-control','style'=>'margin-top:30px;'])}}
    </div>
  </div>
  </div>
{{Form::submit('Create',['class'=>'btn btn-primary'])}}
{!!Form::close()!!}
<script type="text/javascript">
  $("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
</script>
  <script>
        $('input[type="checkbox"]').on('change', function() {
            $('input[name="' + this.name + '"]').not(this).prop('checked', false);
        });
    </script>