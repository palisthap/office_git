@extends('officio.main')
<!-- @section('title','add Dataflow Status') -->

@section('content')
{{----}}
<link rel="stylesheet" href="{{URL::to('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}">
<script src="{{URL::to('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script>
<script src="{{URL::to('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>
@include('officio.flash.message')
Name:{{$type[0]->dhafees->name}}&nbsp&nbsp&nbsp&nbsp&nbsp  Applied For:{{$type[0]->dhafees->applied_for}}<br></br>
<div class="container">
 
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">DHA Status</a></li>
    <li><a data-toggle="tab" href="#menu1">Dataflow Status</a></li>
    <li><a data-toggle="tab" href="#menu2">Exam Status</a></li>

</ul>

<section class="content">
    <div class="row">
        <div class="col-xs-12">

           

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                             
                              <h3>Add DHA Status</h3>
                              <div class="col-md-8">
                                {{ Form::open(['url'=>'admin/dhastatuss', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                                @include('officio.status.dhaform')

                                <div class="text-right border-top">
                                    <button type="submit" class="btn btn-warning">Create Profile</button>
                                </div>
                                {{ form::close() }}
                            </div>
                        </div>
                        
                        <div id="menu1" class="tab-pane fade">
                            <h3>Add Dataflow Status</h3>
                            <div class="col-md-8">
                                {{ Form::open(['url'=>'admin/dataflowform', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                                @include('officio.status.dataflowform')
                                <div class="text-right border-top">
                                    <button type="submit" class="btn btn-warning">ADD</button>
                                </div>
                                {{ form::close() }}
                            </div>
                        </div>
                        
                        <div id="menu2" class="tab-pane fade">
                           <h3>Add Exam Status</h3>
                           <div class="col-md-8">
                            {{ Form::open(['url'=>'admin/examstatusform', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                            <div class="box-body">

                                <input type="hidden" name="dha_id" value="{{ $dha_id }}" >
                                <div class="form-group" {{ $errors->has('preferred_date_from') ? ' has-error' : '' }}>
                                    <label for="preferred_date_from" class="col-sm-2 control-label">Preferred Date From:<span class=help-block"
                                        style="color: #b30000">&nbsp;* </span></label>

                                        <div class="col-sm-8">
                                          {!! Form::date('preferred_date_from', null , ['class'=> 'form-control', 'placeholder' => ' Preferred Date From', 'id'=>"preferred_date_from"]) !!}

                                          @if ($errors->has('preferred_date_from'))
                                          <span class="help-block">
                                            <strong> {{ $errors->first('preferred_date_from') }}</strong>
                                        </span>
                                        @endif

                                    </div>
                                </div>

                                <div class="form-group" {{ $errors->has('preferred_date_to') ? ' has-error' : '' }}>
                                    <label for="preferred_date_to" class="col-sm-2 control-label">TO:<span class=help-block"
                                        style="color: #b30000">&nbsp;* </span></label>

                                        <div class="col-sm-8">
                                            {!! Form::date('preferred_date_to', null , ['class'=> 'form-control', 'placeholder' => ' Preferred Date To', 'id'=>"preferred_date_to"]) !!}

                                            @if ($errors->has('preferred_date_to'))
                                            <span class="help-block">
                                                <strong> {{ $errors->first('preferred_date_to') }}</strong>
                                            </span>
                                            @endif

                                        </div>
                                    </div>

                                   <div class="form-group">
    {{Form::label('dhamcq','DHA/MCQ:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('dhamcq',"1","1")}}{{Form::label('1','Pass')}}
                    {{Form::radio('dhamcq',"0")}}{{Form::label('0','Fail')}}        

                    @if ($errors->has('dhamcq'))
                    <span class="help-block">
                        <strong> {{ $errors->first('dhamcq') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
                                    <div class="form-group" {{ $errors->has('exam_date_booked') ? ' has-error' : '' }}>
                                        <label for="exam_date_booked" class="col-sm-2 control-label">Exam Date Booked:<span class=help-block"
                                            style="color: #b30000">&nbsp;* </span></label>

                                            <div class="col-sm-8">
                                                {!! Form::date('exam_date_booked', null , ['class'=> 'form-control', 'placeholder' => ' Exam Date Booked', 'id'=>"exam_date_booked"]) !!}

                                                @if ($errors->has('exam_date_booked'))
                                                <span class="help-block">
                                                    <strong> {{ $errors->first('exam_date_booked') }}</strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="form-group" {{ $errors->has('dha_status') ? ' has-error' : '' }}>
                                            <label for="dha_status" class="col-sm-2 control-label">DHA Status :<span class=help-block"
                                                style="color: #b30000">&nbsp;* </span></label>

                                                <div class="col-sm-8">
                                                 
                                                   <input type="checkbox" onclick="ShowHideDiv(this)" id="value"  name="dha_status" value="1"  /> Pass &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                   <input type="checkbox"  id="checkbox1" class="dha_status" name="dha_status" value="0" /> Fail


                                                   @if ($errors->has('dha_status'))
                                                   <span class="help-block">
                                                    <strong> {{ $errors->first('dha_status') }}</strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="form-group" id="eligibility" {{ $errors->has('eligibility') ? ' has-error' : '' }} >
                                            <label for="eligibility" class="col-sm-2 control-label">Eligibility:<span class=help-block"
                                                style="color: #b30000">&nbsp;* </span></label>

                                                <div class="col-sm-8">
                                                
                                                   <input type="checkbox" id="eligibility" name="eligibility"  value="1"/> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                   <input type="checkbox" id="eligibility" name="eligibility" value="0"/> No
                                                   @if ($errors->has('eligibility'))
                                                   <span class="help-block">
                                                    <strong> {{ $errors->first('eligibility') }}</strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>
                                        @if(Request::segment(4) == 'edit')
                                        <div class="form-group">
                                            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

                                            <div class="col-sm-8">

                                                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                 @if($examstatus->status==1) checked @endif >Active</label>&nbsp;
                                                 <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                     @if($examstatus->status==0) checked @endif >Inactive</label>
                                                 </div>
                                             </div>
                                             @endif
                                         </div>

                                         <div class="text-right border-top">
                                            <button type="submit" class="btn btn-warning">ADD</button>
                                        </div>
                                        {{ form::close() }}
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div>
</section>
<script src="{{URL::to('js/jQuery.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::to('js/jquery.validate.min.js')}}"></script>


<script type="text/javascript">
    function ShowHideDiv(value) {
        var tableins = document.getElementById("eligibility");
        tableins.style.display = value.checked ? "block" : "none";
    }
</script>
<script>
    $('input[type="checkbox"]').on('change', function() {
        $('input[name="' + this.name + '"]').not(this).prop('checked', false);
    });
</script>

<script>
    $(document).ready(function(){

        $('#checkbox1').change(function(){
            if(this.checked)
                $('#eligibility').fadeOut();


            else
                $('#eligibility').fadeIn('slow');

        });
    });
    $(function(){
        $('ul#minitabs li a').click(function(){
            $('ul#minitabs li a').removeClass('current');
            $(this).addClass('current');
        })
    });

</script>

@stop