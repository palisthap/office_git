  @if(Request::segment(4)!='edit')   
  <input type="hidden" name="dha_id" value="{{$dha_id}}" >
  @endif
  
  
  <div class="box-body">


    <div class="form-group" {{ $errors->has('preferred_date_from') ? ' has-error' : '' }}>
        <label for="preferred_date_from" class="col-sm-2 control-label">Preferred Date From:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">
              {!! Form::date('preferred_date_from', null , ['class'=> 'form-control', 'placeholder' => ' preferred_date_from', 'id'=>"preferred_date_from"]) !!}

              @if ($errors->has('preferred_date_from'))
              <span class="help-block">
                <strong> {{ $errors->first('preferred_date_from') }}</strong>
            </span>
            @endif

        </div>
    </div>

    <div class="form-group" {{ $errors->has('preferred_date_to') ? ' has-error' : '' }}>
        <label for="preferred_date_to" class="col-sm-2 control-label">TO:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">
                {!! Form::date('preferred_date_to', null , ['class'=> 'form-control', 'placeholder' => ' preferred_date_to', 'id'=>"preferred_date_to"]) !!}

                @if ($errors->has('preferred_date_to'))
                <span class="help-block">
                    <strong> {{ $errors->first('preferred_date_to') }}</strong>
                </span>
                @endif

              </div>
        </div>



        <div class="form-group">
            {{Form::label('dhamcq','DHA/MCQ:',['class'=>'col-sm-2 control-label'])}}
            <div class="col-sm-8">
                {{Form::radio('dhamcq',"1","1")}}{{Form::label('1','Pass')}}
                {{Form::radio('dhamcq',"0")}}{{Form::label('0','Fail')}}        

                @if ($errors->has('dhamcq'))
                <span class="help-block">
                    <strong> {{ $errors->first('dhamcq') }}</strong>
                </span>
                @endif

            </div>
        </div>
        <div class="form-group" {{ $errors->has('exam_date_booked') ? ' has-error' : '' }}>
            <label for="exam_date_booked" class="col-sm-2 control-label">Exam Date Booked:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('exam_date_booked', null , ['class'=> 'form-control', 'placeholder' => ' Exam Date Booked', 'id'=>"exam_date_booked"]) !!}

                    @if ($errors->has('exam_date_booked'))
                    <span class="help-block">
                        <strong> {{ $errors->first('exam_date_booked') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

            <div class="form-group" {{ $errors->has('dha_status') ? ' has-error' : '' }}>
                <label for="dha_status" class="col-sm-2 control-label">DHA Status :<span class=help-block"
                    style="color: #b30000">&nbsp;* </span></label>

                    <div class="col-sm-8">

                     <input type="checkbox" onclick="ShowHideDiv(this)" id="value"  name="dha_status[]" value="1"   @if(old( 'dha_status', $examstatus->dha_status=='1') ) checked @endif/> Pass &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <input type="checkbox"  id="checkbox1" class="dha_status" name="dha_status" value="0" @if(old( 'dha_status', $examstatus->dha_status=='0') ) checked @endif/> Fail


                     @if ($errors->has('dha_status'))
                     <span class="help-block">
                        <strong> {{ $errors->first('dha_status') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

            <div class="form-group" id="eligibility" {{ $errors->has('eligibility') ? ' has-error' : '' }} >
                <label for="eligibility" class="col-sm-2 control-label">Eligibility:<span class=help-block"
                    style="color: #b30000">&nbsp;* </span></label>

                    <div class="col-sm-8">

                     <input type="checkbox" id="eligibility" name="eligibility"  value="1"/> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <input type="checkbox" id="eligibility" name="eligibility" value="0"/> No
                     @if ($errors->has('eligibility'))
                     <span class="help-block">
                        <strong> {{ $errors->first('eligibility') }}</strong>
                    </span>
                    @endif

                </div>
            </div>



           </div>