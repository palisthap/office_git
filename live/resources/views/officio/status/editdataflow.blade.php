@extends('officio.main')


@section('content')
{{----}}
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Data Flow Status
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($dataflow, [
        'route' => ['dataflow.update', $dataflow->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.status.dataflowform')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


