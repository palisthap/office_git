         @if(Request::segment(4)!='edit')   
          <input type="hidden" name="dha_id" value="{{$dha_id}}" >
          @endif
<div class="box-body">
  <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
    <label for="email" class="col-sm-2 control-label">Email:<span class=help-block"
      style="color: #b30000">&nbsp;* </span></label>

      <div class="col-sm-8">
        {!! Form::email('email', null , ['class'=> 'form-control', 'placeholder' => ' email', 'id'=>"email"]) !!}

        @if ($errors->has('email'))
        <span class="help-block">
          <strong> {{ $errors->first('email') }}</strong>
        </span>
        @endif


      </div>
    </div>
    
    <div class="form-group" {{ $errors->has('username') ? ' has-error' : '' }}>
      <label for="username" class="col-sm-2 control-label">Username:<span class=help-block"
        style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('username', null , ['class'=> 'form-control', 'placeholder' => ' username', 'id'=>"username"]) !!}

          @if ($errors->has('username'))
          <span class="help-block">
            <strong> {{ $errors->first('username') }}</strong>
          </span>
          @endif

        </div>
      </div>

      <div class="form-group" {{ $errors->has('password') ? ' has-error' : '' }}>
        <label for="password" class="col-sm-2 control-label">Password:<span class=help-block"
          style="color: #b30000">&nbsp;* </span></label>

          <div class="col-sm-8">
          <!--   {!! Form::text('password', null , ['class'=> 'form-control', 'placeholder' => ' password', 'id'=>"password"]) !!} -->
 {!! Form::input('password', 'password', null , ['class'=> 'form-control', 'placeholder' => 'Password', 'id'=>"password"]) !!}
            @if ($errors->has('password'))
            <span class="help-block">
              <strong> {{ $errors->first('password') }}</strong>
            </span>
            @endif

          </div>
        </div>






      <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
         </div>
