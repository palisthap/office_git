@extends('officio.main')
@section('title','Cash Out')
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       
         <span>
                <a href="{{ url('admin/status/create') }}">
                    <button type="button" class="btn btn-primary" id="viewbtn">
                        Create User Status
                    </button>
                </a>
        </span>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                             
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                $i=1;
                              


                            ?>
                            @foreach($status as $type)
                                <tr>
                                    <td>{{ $i++ }}</td>
                              <td>
                                          <?php
                                          $roleId = $type->dha_id;
                                         // $roleId = array_filter($value->dropdown_id,function($x){ return !empty($x); });
                                            $role = \App\DhaFees::where('id',$roleId)->first();

                                            if(empty($role)){
                                                echo 'N/A';
                                            }
                                            else{
                                                echo $role->name;
                                            }
                                            //dd($role);
                                        ?>

                                       
                                  </td>
                                   
                                
                                        
                                   
                                
                                  
                                    <td>
                                        @if($type->status == 1)
                                            Active
                                        @else
                                            InActive
                                        @endif
                                    </td>

                                    <td>

                                        <a type="button" type="button" class="btn btn-primary btn-sm"
                                                href="{{ route('status.edit', array($type->id)) }}">
                                            <i class="flaticon-edit"></i>
                                        </a>

                                        <form action="{{ route('status.destroy', array($type->id)) }}"
                                              method="DELETE" class="delete-user-form">
                                            {!! csrf_field() !!}

                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i class="flaticon-delete-button"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
</tbody>

                        </table>
                    </div>
                </div><!-- /.box-body
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>

@stop