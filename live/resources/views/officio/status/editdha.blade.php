@extends('officio.main')
@section('title','edit Cash Out')

@section('content')
{{----}}
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit DHA Profile
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($dhastatus, [
        'route' => ['status.update', $dhastatus->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.status.dhaform')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


