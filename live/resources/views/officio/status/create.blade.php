@extends('officio.main')
@section('title','add User Status')

@section('content')
{{----}}
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Create Dubai Health Authority(DHA) Profile
    </h1>
</section>
@include('officio.flash.message')


<section class="content">
   <div class="row">
    <div class="col-md-12">

        {{--@include('flash.message')--}}
        {{--@include('errors.list')--}}

        <!-- Horizontal Form -->
        <div class="box box-info">
            <!-- form start -->
            <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    {{ Form::open(['url'=>'admin/statuss', 'method'=>'GET', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}

                    @include('officio.status.form')
                    <div class="text-right border-top">
                        <button type="submit" class="btn btn-warning">Create Profile</button>
                    </div>
                    {{ form::close() }}
                </div>
            </div>
        </div>
    </div><!-- /.box -->


</div>
</section>
  
@stop
