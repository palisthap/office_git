@extends('officio.main')
<!-- @section('title','add Dataflow Status') -->

@section('content')
{{----}}
<link rel="stylesheet" href="{{URL::to('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}">
<script src="{{URL::to('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script>
<script src="{{URL::to('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>



<section class="content-header">

</section>
<div class="container">


  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">DHA Status</a></li>
    <li><a data-toggle="tab" href="#menu1">Data Flow Status</a></li>
    <li><a data-toggle="tab" href="#menu2">Exam Status</a></li>

  </ul>


  <!--  -->

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        @include('officio.flash.message')

        <div class="box">
          <div class="box-body">
            <div class="table-reponsive">

              <div class="tab-content">
                <div id="home" class="tab-pane fade in active">

                  <h3>DHA Status</h3>


                  <table id="example1" class="table table-bordered table-striped user-list">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Applied For</th>
                        <th>Email</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i=1;
                      ?>
                      @foreach($status as $type)
                      <?php
                      ?>
                      <tr>
                        <td>{{ $i++ }}</td>

                        <td>{{ $type->dhafees()->first()->name}}</td>
                        <td>{{ $type->dhafees()->first()->applied_for}}</td>
                        <td>{{ $type->email}}</td>
                        <td>{{ $type->username}}</td>
                        <td>{{ $type->password}}</td>
                        <td>

                          <a type="button" type="button" class="btn btn-primary btn-sm"
                          href="{{ route('status.edit', array($type->id)) }}">
                          <i class="flaticon-edit"></i>
                        </a>

                        <form action="{{ route('status.destroy', array($type->id)) }}"
                          method="DELETE" class="delete-user-form">
                          {!! csrf_field() !!}

                          <button type="submit" class="btn btn-sm btn-danger">
                            <i class="flaticon-delete-button"></i>
                          </button>

                        </form>
                      </td>


                    </tr>
                  </tbody>

                  @endforeach
                </table>

              </div>
              <div id="menu1" class="tab-pane fade">

               <h3>Data Flow Status</h3>


               <table id="example1" class="table table-bordered table-striped user-list">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Applied For</th>
                    <th>Passport No.</th>
                    <th>DHA Reference No.</th>
                    <th>Barcode No.</th>
                    <th>Remarks</th>

                    <th>Action</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  ?>
                  @foreach($dataflow as $type)
                  <tr>
                    <td>{{ $i++ }}</td>

                    <td>{{ $type->dhafees()->first()->name }}</td>
                    <td>{{ $type->dhafees()->first()->applied_for}}</td>
                    <td>{{ $type->passport_no}}</td>
                    <td>{{ $type->dha_ref_no}}</td>
                    <td>{{ $type->barcode_no}}</td>                                                        
                    <td>{{ $type->remarks}}</td>
                    <td>

                      <a type="button" type="button" class="btn btn-primary btn-sm"
                      href="{{ route('dataflow.edit', array($type->id)) }}">
                      <i class="flaticon-edit"></i>
                    </a>

                    <form action="{{ route('dataflow.destroy', array($type->id)) }}"
                      method="DELETE" class="delete-user-form">
                      {!! csrf_field() !!}

                      <button type="submit" class="btn btn-sm btn-danger">
                        <i class="flaticon-delete-button"></i>
                      </button>

                    </form>
                  </td>


                </tr>
              </tbody>

              @endforeach
            </table>


          </div>

          <div id="menu2" class="tab-pane fade">
            <h3>Add Exam Status</h3>
            <table id="example1" class="table table-bordered table-striped user-list">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Applied For</th>
                  <th style="width: 90px;">Prefered Date From</th>

                  <th style="width: 90px;">Prefered Date To</th>
                  <th>Booked Date</th>
                  <th>DHA/MCQ Status</th>
                  <th>DHA Status</th>
                  <th>Eligibility</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=1;
                ?>
                @foreach($examstatus as $type)
                <?php
                ?>
                <tr>
                  <td>{{ $i++ }}</td>

                  <td>{{ $type->dhafees()->first()->name}}</td>
                  <td>{{ $type->dhafees()->first()->applied_for}}</td>
                  <td>{{ $type->preferred_date_from}}</td>
                  <td>{{ $type->preferred_date_to}}</td>



                  <td>{{ $type->exam_date_booked}}</td>

                  <td>@if($type->dhamcq==1)
                    Pass
                    @else
                    Fail
                    @endif
                  </td>
                  

                  <td> @if($type->dha_status==1)
                    Pass
                    @else
                    Fail
                    @endif
                  </td>

                  <td> @if($type->eligibility==1)
                    Yes
                    @else
                    No
                    @endif
                  </td>
                  <td>

                    <a type="button" type="button" class="btn btn-primary btn-sm"
                    href="{{ route('examstatus.edit', array($type->id)) }}">
                    <i class="flaticon-edit"></i>
                  </a>

                  <form action="{{ route('examstatus.destroy', array($type->id)) }}"
                    method="DELETE" class="delete-user-form">
                    {!! csrf_field() !!}

                    <button type="submit" class="btn btn-sm btn-danger">
                      <i class="flaticon-delete-button"></i>
                    </button>

                  </form>
                </td>


              </tr>
            </tbody>

            @endforeach
          </table>

        </div>
      </div>

    </div>
  </div><!-- /.box-body -->
</div><!-- /.box -->
</div><!-- /.col -->

</div><!-- /.row -->

</section>

@stop

