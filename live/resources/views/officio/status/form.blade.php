<div class="box-body">
   
                        
                        <div class="form-group" {{ $errors->has('dha_id') ? ' has-error' : '' }}>
                            <label for="dha_id" class="col-sm-2 control-label">Name:<span class=help-block"
                                style="color: #b30000">&nbsp;* </span></label>

                                <div class="col-sm-8">
                                
     {{ Form::select('dha_id',$dhafees,null,['class'=>'form-control col-md-7 col-xs-12','placeholder'=>'--Select--']) }}
                                  @if ($errors->has('dha_id'))
                                  <span class="help-block">
                                    <strong> {{ $errors->first('dha_id') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
                        
                        

                        @if(Request::segment(4) == 'edit')
                        <div class="form-group">
                            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">

                                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                 @if($dhafees->status==1) checked @endif >Active</label>&nbsp;
                                 <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                     @if($dhafees->status==0) checked @endif >Inactive</label>
                                 </div>
                             </div>
                             @endif
                         </div>
