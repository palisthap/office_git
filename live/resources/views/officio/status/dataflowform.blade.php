  
         
 @if(Request::segment(4)!='edit')   
          <input type="hidden" name="dha_id" value="{{$dha_id}}" >
          @endif
<div class="box-body">
<div class="form-group" {{ $errors->has('passport_no') ? ' has-error' : ''  }}>    <label for="passport_no" class="col-sm-2 control-label">Passport Number:<span class=help-block"
	style="color: #b30000">&nbsp;* </span></label>

	<div class="col-sm-8">
		{!! Form::text('passport_no', null , ['class'=> 'form-control', 'placeholder' => ' Passport No', 'id'=>"passport_no"]) !!}

		@if ($errors->has('passport_no'))
		<span class="help-block">
			<strong> {{ $errors->first('passport_no') }}</strong>
		</span>
		@endif

	</div>
</div>      



<div class="form-group" {{ $errors->has('dha_ref_no') ? ' has-error' : ''}}>     <label for="dha_ref_no" class="col-sm-2 control-label">DHA Reference Number:<span class=help-block"
	style="color: #b30000">&nbsp;* </span></label>

	<div class="col-sm-8">
		{!! Form::text('dha_ref_no', null , ['class'=> 'form-control', 'placeholder' => ' DHA Reference No', 'id'=>"dha_ref_no"]) !!}

		@if ($errors->has('dha_ref_no'))
		<span class="help-block">
			<strong> {{ $errors->first('dha_ref_no') }}</strong>
		</span>
		@endif

	</div>
</div> 


<div class="form-group" {{ $errors->has('barcode_no') ? ' has-error' : ''}}>     <label for="barcode_no" class="col-sm-2 control-label">Dataflow Barcode Number:<span class=help-block"
	style="color: #b30000">&nbsp;* </span></label>

	<div class="col-sm-8">
		{!! Form::text('barcode_no', null , ['class'=> 'form-control', 'placeholder' => ' Barcode no', 'id'=>"barcode_no"]) !!}

		@if ($errors->has('barcode_no'))
		<span class="help-block">
			<strong> {{ $errors->first('barcode_no') }}</strong>
		</span>
		@endif

	</div>
</div> 





<div class="form-group" {{ $errors->has('remarks') ? ' has-error' : ''}}>      <label for="remarks" class="col-sm-2 control-label">Remarks:<span class=help-block"
	style="color: #b30000">&nbsp;* </span></label>

	<div class="col-sm-8">
		{!! Form::textarea('remarks', null , ['class'=> 'form-control', 'placeholder' => ' remarks', 'id'=>"remarks"]) !!}

		@if ($errors->has('remarks'))
		<span class="help-block">
			<strong> {{ $errors->first('remarks') }}</strong>
		</span>
		@endif

	</div>
</div>                

 <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
	</div>