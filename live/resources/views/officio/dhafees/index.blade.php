@extends('officio.main')
@section('title','DHA/Fees')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

       <span>
        <a href="{{ url('admin/DHA/create') }}">
            <button type="button" class="btn btn-success" id="viewbtn">
                Create DHA/Fees
            </button>
        </a>
    </span>
</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Qualification</th>
                                    <th>Applied for</th>
                                    <th>Total amount</th>


                                    <th>Status</th>
                                    <th>Actions</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=1;
                                ?>
                                @foreach($dhaFees as $type)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $type->name }}</td>
                                    <td>{{ $type->qualification }}</td>
                                    <td>{{ $type->applied_for}}</td>
                                    <td>{{ $type->first_payment }}</td>



                                    <td>
                                        @if($type->status == 1)
                                        Active
                                        @else
                                        InActive
                                        @endif
                                    </td>

                                    <td>

                                        <a type="button" type="button" class="btn btn-primary btn-sm"
                                        href="{{ route('dha.edit', array($type->id)) }}">
                                        <i class="flaticon-edit"></i>
                                    </a>

                                    <form action="{{ route('dha.destroy', array($type->id)) }}"
                                      method="DELETE" class="delete-user-form">
                                      {!! csrf_field() !!}

                                      <button type="submit" class="btn btn-sm btn-danger">
                                        <i class="flaticon-delete-button"></i>
                                    </button>
                                </form>
                                <a  href="{{ route('dha.show', array($type->id)) }}">View Profile
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        
                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>

@stop