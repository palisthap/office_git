@extends('officio.main')

@section('content')
{{----}}
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Create DHA User Profile
  </h1>
</section>
@include('officio.flash.message')


<section>
  <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
      <div class="col-lg-12">

        <ol class="breadcrumb">
          <li class="active">
            <i class="fa fa-user"></i> dhauserprofile                            </li>
          </ol>
        </div>
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-lg-12">
          <div class="col-md-5  toppad  pull-right col-md-offset-3 ">

          </div>
          <div class="  col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 toppad">


            <div class=" ">
             <div class=" ">
              <h3 class="panel-title uppercase blue ">@foreach($dhafees as $user)
                {{$user->name}}
                @endforeach
                <a href="{{ url('admin/edit/'.$user->id) }}" class="pull-right">Edit Profile</a></h3>
                <br>
              </div>
              <div class=" ">

                <div class="row">
                  <div class=" col-md-12 col-lg-12 col-sm-12 ">
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td>Name&nbsp; </td>
                          <td class="uppercase">{{$user->name}}</td>
                          <td>Qualification&nbsp; </td>
                          <td class="uppercase">{{$user->qualification}}</td>
                          <td>Applied For&nbsp; </td>
                          <td class="uppercase">{{$user->applied_for}}</td>
                          <td>Total</td>
                          <td class="uppercase">{{$user->first_payment}}</td>
                          <td>Paid Amount</td>
                          <?php
                          if(count($dhaFeeTotal[0]->installmentDhaTotal)>0){
                           $total_installment= $dhaFeeTotal[0]->installmentDhaTotal[0]['paid_amount'];
                          }
                          else{
                             $total_installment=0;
                          }
                          ?>
                          <td  class="uppercase">{{$total_installment}}</td>
                          <td>Due Amount</td>
                          <td class="uppercase">{{$user->first_payment-$total_installment }}</td>

                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <h3 class="breadcrumb"> Installment</h3>
    <div class="col-md-12 col-lg-12  col-sm-12">

     <a href="{{ url('admin/viewinvoice/'.$user->id) }}" target="_blank" class="pull-right btn btn-warning" style="width:130px;height:30px;">Generate Invoice</a>
     {{ Form::open(['url'=>'admin/installment/'.$user->id, 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}

     <div class="text-right border-top">
      <button type="submit" class=" btn-primary btn-sm ">Add Installment</button>
    </div>
    {{ form::close() }}
    <br><br>
  </div>
  <?php
  $sn_installment=0;
  ?>
  @if(!empty($user->installmentDha ))
  @foreach($user->installmentDha  as $installment_info)
  <div class="col-md-12 col-lg-12  col-sm-12 panel panel-info margin-left-5">
   <span class="panel-heading  uppercase">Installment {{++$sn_installment}}</span>
   <span class="panel-heading pad-icon pull-right">

 
                                        <form action="{{ route('installment.destroy', array($installment_info->id)) }}"
                                          method="DELETE" class="delete-user-form">
                                          {!! csrf_field() !!}

                                        <button type="submit" class="btn btn-sm btn-danger">
                                        <i class="flaticon-delete-button"></i>
                                    </button>
                                    </form>
   </span>
   <br><br><table class="table cop table-user-information">
   <tbody>

    <tr>
     <td>Date</td> <td class="uppercase">  {{$installment_info->nepali_date}}</td>
     <td>Payment Mode</td><td class="uppercase">{{$installment_info->payment_mode}}</td>
     <td>Received By</td>  <td class="uppercase">{{$installment_info->received_by}}</td>
     <td>Bank Deposited Date</td>  <td class="uppercase">{{$installment_info->deposited_date}}</td>
     <td>Verified By</td>  <td class="uppercase">{{$installment_info->verified_by}}</td>
   </tr>
   <tr><td>Amount</td> <td class="uppercase" style=" overflow-wrap: break-word;">{{$installment_info->amount}}</td></tr>
   <tr><td>


   </td>
 </tr>
</tbody>
</table>


</div>

@endforeach
@endif
<div class="col-sm-12 col-md-12 changed-color">
  <div class="alert-message alert-message-success">
    <h4>
      Todays Reminder By You</h4>  <br>
      <div class="update-nag">
        <div class="update-split update-success"><i class="glyphicon glyphicon-leaf"></i></div>

      </div>  

    </div>
  </div>
</section>

@stop

