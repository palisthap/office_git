@extends('officio.main')
@section('title','edit DHA/Fees')

@section('content')
{{----}}
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit DHA/Fees
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($dhaFee, [
        'route' => ['dha.update', $dhaFee->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.dhafees.form')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


