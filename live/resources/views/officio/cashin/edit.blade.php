@extends('officio.main')
@section('title','edit Cash In')

@section('content')
{{----}}
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Cash In
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($cashIn, [
        'route' => ['cash_in.update', $cashIn->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.cashin.form')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


