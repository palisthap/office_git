@extends('officio.main')
@section('title','Cash Out')
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Cash Out
         <span>
                <a href="{{ url('admin/cash_out/create') }}">
                    <button type="button" class="btn btn-primary" id="viewbtn">
                        Create Cash Out
                    </button>
                </a>
        </span>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Expenses Type</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Paid By</th>
                                <th>Recived By</th>
                                <th>Payment Mode</th>
                       
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                $i=1;
                              


                            ?>
                            @foreach($cashOuts as $type)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $type->expenses_type }}</td>
                                    <td>{{ $type->nepali_date }}</td>
                                    <td>{{ $type->amount }}</td>
                                    <td>{{ $type->paid_by }}</td>
                                    <td>{{ $type->received_by}}</td>
                                    <td>{{ $type->payment_mode }}</td>
                                
                                        
                                   
                                
                                  
                                    <td>
                                        @if($type->status == 1)
                                            Active
                                        @else
                                            InActive
                                        @endif
                                    </td>

                                    <td>

                                        <a type="button" type="button" class="btn btn-primary btn-sm"
                                                href="{{ route('cash_out.edit', array($type->id)) }}">
                                            <i class="flaticon-edit"></i>
                                        </a>

                                        <form action="{{ route('cash_out.destroy', array($type->id)) }}"
                                              method="DELETE" class="delete-user-form">
                                            {!! csrf_field() !!}

                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i class="flaticon-delete-button"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
</tbody>
<td>Total &nbsp &nbsp 
<?php echo $total=$data;  ?>
</td>
                        </table>
                    </div>
                </div><!-- /.box-body
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>

@stop