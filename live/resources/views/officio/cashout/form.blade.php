<div class="box-body">
    <div class="form-group" {{ $errors->has('expenses_type') ? ' has-error' : '' }}>
        <label for="expenses_type" class="col-sm-2 control-label">Expenses Type:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">


                     {{ Form::select('expenses_type', ['Rent' => 'Rent', 'Electricity & Garbage' => 'Electricity & Garbage', 'Internet' => 'Internet', 'Book Printing' => 'Book Printing', 'Advertising & Marketing' => 'Advertising & Marketing', 'Cancellation/Refund' => 'Cancellation/Refund', 'Telephone' => 'Telephone', 'Stationery' => 'Stationery', 'Lunch' => 'Lunch', 'Snacks & tea' => 'Snacks & tea', 'Staff Salary' => 'Staff Salary', 'Fuel & travel expenses' => 'Fuel & travel expenses', 'Repair & maintenance' => 'Repair & maintenance', 'Loan Payment' => 'Loan Payment','Basic Life Support' => 'Basic Life Support','Others' => 'Others'], null, ['class' => 'form-control']) }}
                @if ($errors->has('expenses_type'))
                <span class="help-block">
                    <strong> {{ $errors->first('expenses_type') }}</strong>
                </span>
                @endif

            </div>
        </div>

        <div class="form-group" {{ $errors->has('nepali_date') ? ' has-error' : '' }}>
            <label for="nepali_date" class="col-sm-2 control-label">Nepali Date:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('nepali_date', null , ['class'=> 'form-control', 'placeholder' => ' Nepali Date' ,'id'=>'nepaliDate5']) !!}
           
                    @if ($errors->has('nepali_date'))
                    <span class="help-block">
                        <strong> {{ $errors->first('nepali_date') }}</strong>
                    </span>
                   @endif
                </div>
            </div>
            <div class="form-group" {{ $errors->has('amount') ? ' has-error' : '' }}>
                <label for="amount" class="col-sm-2 control-label">Amount:<span class=help-block"
                    style="color: #b30000">&nbsp;* </span></label>

                    <div class="col-sm-8">
                        {!! Form::text('amount', null , ['class'=> 'form-control', 'placeholder' => ' Amount', 'id'=>"amount"]) !!}

                        @if ($errors->has('amount'))
                        <span class="help-block">
                            <strong> {{ $errors->first('amount') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>
                
                <div class="form-group" {{ $errors->has('paid_by') ? ' has-error' : '' }}>
                    <label for="paid_by" class="col-sm-2 control-label">Paid By:<span class=help-block"
                        style="color: #b30000">&nbsp;* </span></label>

                        <div class="col-sm-8">
                            {!! Form::text('paid_by', null , ['class'=> 'form-control', 'placeholder' => ' Paid By', 'id'=>"paid_by"]) !!}

                            @if ($errors->has('paid_by'))
                            <span class="help-block">
                                <strong> {{ $errors->first('paid_by') }}</strong>
                            </span>
                            @endif

                        </div>
                    </div>
                    
                    <div class="form-group" {{ $errors->has('received_by') ? ' has-error' : '' }}>
                        <label for="received_by" class="col-sm-2 control-label">Received By:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">
                                {!! Form::text('received_by', null , ['class'=> 'form-control', 'placeholder' => ' Received By', 'id'=>"received_by"]) !!}

                                @if ($errors->has('received_by'))
                                <span class="help-block">
                                    <strong> {{ $errors->first('received_by') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
                        
                        <div class="form-group" {{ $errors->has('payment_mode') ? ' has-error' : '' }}>
                            <label for="payment_mode" class="col-sm-2 control-label">Payment Mode:<span class=help-block"
                                style="color: #b30000">&nbsp;* </span></label>

                                <div class="col-sm-8">


                     {{ Form::select('payment_mode', ['cash' => 'cash', 'cheque' => 'cheque', 'bank' => 'bank'],null, ['class' => 'form-control']) }}
                                  @if ($errors->has('payment_mode'))
                                  <span class="help-block">
                                    <strong> {{ $errors->first('payment_mode') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
                        
                        

                          <div class="form-group">
    {{Form::label('status','Status:',['class'=>'col-sm-2 control-label'])}}
        <div class="col-sm-8">
                    {{Form::radio('status',"1","1")}}{{Form::label('1','Active')}}
                    {{Form::radio('status',"0")}}{{Form::label('0','Inactive')}}


        </div>
    </div>
</div>
