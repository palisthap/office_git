@extends('officio.main')
@section('title','Griffith Enquiry')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

       <span>
        <a href="{{ url('admin/enquirybythem/create') }}">
            <button type="button" class="btn btn-success" id="viewbtn">
                Create Griffith Enquiry
            </button>
        </a>
    </span>
</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Qualification</th>
                                    <th>Phone</th>
                                    <th>Remarks</th>


                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=1;
                                ?>
                                @foreach($enquirybythems as $type)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                 
                                    <td>{{ $type->date }}</td>
                                    <td>{{ $type->name }}</td>
                                    <td>{{ $type->email}}</td>
                                    <td>{{ $type->qualification }}</td>
                                    <td>{{ $type->phone}}</td>
                                    <td>{{ $type->remarks }}</td>



                                 

                               
                            </tbody>

                        </tr>
                        @endforeach

                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>

@stop