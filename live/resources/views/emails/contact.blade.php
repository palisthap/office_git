
<!-- You have successfully reserve for an Offer.
<br>
First Name : {{$data['first_name']}} <br>
Last Name  :{{$data['last_name']}} 
 -->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Activate Account</title>
</head>

<body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#000!important;font-family:Montserrat,sans-serif!important;font-size:16px;font-weight:400;line-height:1.3;margin:0!important;min-width:100%;padding:0!important;text-align:left;width:100%!important">
<style>
    @media only screen {
        html {
            min-height: 100%;
            background: #f3f3f3
        }
    }

    @media only screen and (max-width:596px) {
        .small-float-center {
            margin: 0 auto!important;
            float: none!important;
            text-align: center!important
        }
        .small-text-center {
            text-align: center!important
        }
        .small-text-left {
            text-align: left!important
        }
        .small-text-right {
            text-align: right!important
        }
    }

    @media only screen and (max-width:596px) {
        .hide-for-large {
            display: block!important;
            width: auto!important;
            overflow: visible!important;
            max-height: none!important;
            font-size: inherit!important;
            line-height: inherit!important
        }
    }

    @media only screen and (max-width:596px) {
        table.body table.container .hide-for-large,
        table.body table.container .row.hide-for-large {
            display: table!important;
            width: 100%!important
        }
    }

    @media only screen and (max-width:596px) {
        table.body table.container .callout-inner.hide-for-large {
            display: table-cell!important;
            width: 100%!important
        }
    }

    @media only screen and (max-width:596px) {
        table.body table.container .show-for-large {
            display: none!important;
            width: 0;
            mso-hide: all;
            overflow: hidden
        }
    }

    @media only screen and (max-width:596px) {
        table.body img {
            width: auto;
            height: auto
        }
        table.body center {
            min-width: 0!important
        }
        table.body .container {
            width: 95%!important
        }
        table.body .column,
        table.body .columns {
            height: auto!important;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            padding-left: 16px!important;
            padding-right: 16px!important
        }
        table.body .column .column,
        table.body .column .columns,
        table.body .columns .column,
        table.body .columns .columns {
            padding-left: 0!important;
            padding-right: 0!important
        }
        table.body .collapse .column,
        table.body .collapse .columns {
            padding-left: 0!important;
            padding-right: 0!important
        }
        td.small-1,
        th.small-1 {
            display: inline-block!important;
            width: 8.33333%!important
        }
        td.small-2,
        th.small-2 {
            display: inline-block!important;
            width: 16.66667%!important
        }
        td.small-3,
        th.small-3 {
            display: inline-block!important;
            width: 25%!important
        }
        td.small-4,
        th.small-4 {
            display: inline-block!important;
            width: 33.33333%!important
        }
        td.small-5,
        th.small-5 {
            display: inline-block!important;
            width: 41.66667%!important
        }
        td.small-6,
        th.small-6 {
            display: inline-block!important;
            width: 50%!important
        }
        td.small-7,
        th.small-7 {
            display: inline-block!important;
            width: 58.33333%!important
        }
        td.small-8,
        th.small-8 {
            display: inline-block!important;
            width: 66.66667%!important
        }
        td.small-9,
        th.small-9 {
            display: inline-block!important;
            width: 75%!important
        }
        td.small-10,
        th.small-10 {
            display: inline-block!important;
            width: 83.33333%!important
        }
        td.small-11,
        th.small-11 {
            display: inline-block!important;
            width: 91.66667%!important
        }
        td.small-12,
        th.small-12 {
            display: inline-block!important;
            width: 100%!important
        }
        .column td.small-12,
        .column th.small-12,
        .columns td.small-12,
        .columns th.small-12 {
            display: block!important;
            width: 100%!important
        }
        table.body td.small-offset-1,
        table.body th.small-offset-1 {
            margin-left: 8.33333%!important;
            Margin-left: 8.33333%!important
        }
        table.body td.small-offset-2,
        table.body th.small-offset-2 {
            margin-left: 16.66667%!important;
            Margin-left: 16.66667%!important
        }
        table.body td.small-offset-3,
        table.body th.small-offset-3 {
            margin-left: 25%!important;
            Margin-left: 25%!important
        }
        table.body td.small-offset-4,
        table.body th.small-offset-4 {
            margin-left: 33.33333%!important;
            Margin-left: 33.33333%!important
        }
        table.body td.small-offset-5,
        table.body th.small-offset-5 {
            margin-left: 41.66667%!important;
            Margin-left: 41.66667%!important
        }
        table.body td.small-offset-6,
        table.body th.small-offset-6 {
            margin-left: 50%!important;
            Margin-left: 50%!important
        }
        table.body td.small-offset-7,
        table.body th.small-offset-7 {
            margin-left: 58.33333%!important;
            Margin-left: 58.33333%!important
        }
        table.body td.small-offset-8,
        table.body th.small-offset-8 {
            margin-left: 66.66667%!important;
            Margin-left: 66.66667%!important
        }
        table.body td.small-offset-9,
        table.body th.small-offset-9 {
            margin-left: 75%!important;
            Margin-left: 75%!important
        }
        table.body td.small-offset-10,
        table.body th.small-offset-10 {
            margin-left: 83.33333%!important;
            Margin-left: 83.33333%!important
        }
        table.body td.small-offset-11,
        table.body th.small-offset-11 {
            margin-left: 91.66667%!important;
            Margin-left: 91.66667%!important
        }
        table.body table.columns td.expander,
        table.body table.columns th.expander {
            display: none!important
        }
        table.body .right-text-pad,
        table.body .text-pad-right {
            padding-left: 10px!important
        }
        table.body .left-text-pad,
        table.body .text-pad-left {
            padding-right: 10px!important
        }
        table.menu {
            width: 100%!important
        }
        table.menu td,
        table.menu th {
            width: auto!important;
            display: inline-block!important
        }
        table.menu.small-vertical td,
        table.menu.small-vertical th,
        table.menu.vertical td,
        table.menu.vertical th {
            display: block!important
        }
        table.menu[align=center] {
            width: auto!important
        }
        table.button.small-expand,
        table.button.small-expanded {
            width: 100%!important
        }
        table.button.small-expand table,
        table.button.small-expanded table {
            width: 100%
        }
        table.button.small-expand table a,
        table.button.small-expanded table a {
            text-align: center!important;
            width: 100%!important;
            padding-left: 0!important;
            padding-right: 0!important
        }
        table.button.small-expand center,
        table.button.small-expanded center {
            min-width: 0
        }
    }
    .CToWUd{margin-bottom: -25px !important;}

    @media (max-width:767px) {
        .columns+.columns {
            border-left: none
        }
    }
</style><span class="preheader" style="color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span>
<table class="body" style="Margin:0;background:#fff!important;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%">
    <tr style="padding:0;text-align:left;vertical-align:top">
        <td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
            <center data-parsed="" style="min-width:580px;width:100%">
                <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" align="center" class="float-center">
                <table align="center" class="row float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;position:relative;text-align:center;vertical-align:top;width:100%">
                    <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <table class="row table-head" style="background:rgba(51,44,43,.9) url(http://lugmety.go-demo.com/lugmety/images/bg-pattern.png) repeat 0 0;border-collapse:collapse;border-spacing:0;border-top:10px solid #eb593b!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                            <tbody>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%">
                                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                <center class="logo" data-parsed="" style="display:inline-block!important;margin-bottom:-40px!important;margin-top:-10px!important;max-width:100%!important;min-width:none!important;width:100%;margin-bottom: -25px;">
                                                    <img src="images/green.png" alt="logo" align="center" class="float-center" style="clear:both;display:block;float:none;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto;margin-bottom: -25px;">
                                                </center>
                                            </th>
                                            <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                        <table align="center" class="container wrapper" style="Margin:0 auto;background:#fff!important;border-collapse:collapse;border-spacing:0;margin:50px auto;padding:0;text-align:inherit;vertical-align:top;width:600px">
                            <tbody>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                    <table class="row mb30" style="border-collapse:collapse;border-spacing:0;display:table;margin-bottom:30px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                        <tbody>
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px">
                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                            <center data-parsed="" style="min-width:532px;width:100%">
                                                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:15px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                                                                 
                                                                      Dear{{$data['first_name']}} &nbsp&nbsp
{{$data['last_name']}} <br>
                                                                </p>
                                                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:15px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                                                                   You have successfully reserve for an Offer.&nbsp;
                                                                </p>
                                                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:15px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                                                                </p>
                                                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:15px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                                                            </center>
                                                        </th>
                                                        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                                    </tr>
                                                </table>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="row footer-table" style="border-collapse:collapse;border-spacing:0;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                            <tbody>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px">
                                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                <table class="row table-head" style="background:rgba(51,44,43,.9) url(http://lugmety.go-demo.com/lugmety/images/bg-pattern.png) repeat 0 0;border-bottom:10px solid #eb593b!important;border-collapse:collapse;border-spacing:0;border-top:none!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%">
                                                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                        <p class="footer-text" style="Margin:0;Margin-bottom:10px;color:#fff!important;font-family:Montserrat,sans-serif!important;font-size:16px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:20px!important;text-align:center!important">Thank You Again!</p>
                                                                    </th>
                                                                    <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </tr>
                    </tbody>
                </table>
            </center>
        </td>
    </tr>
</table>
<!-- prevent Gmail on iOS font size manipulation -->
<div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>

</html>